<?php

$serviceee =array();
foreach($service as $value){  
$serviceee[]   = $value->services_id;

}

?>
    <style>
  .model-mobile-height {
    height: 150%;
  }


</style>
    <div class="ps-page--single">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="<?= base_url(); ?>">Home</a></li>
                    <li><a href="dashboard">Vendor Dashboard</a></li>
                    <li><a href="dashboard"><?php print_r($this->session->userdata('data')['data']->name); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="ps-vendor-dashboard">
        <div class="container">
            <div class="ps-section__content">
                <ul class="ps-section__links">
                    <li class="active"><a href="<?= base_url(); ?>user/dashboard"><button class="btn-success">Dashboard</button></a></li>
                 
                   
                    <li><a href="#">My Store</a></li>
                     <li><a href="#"><span data-toggle="modal" data-target="#myModalChangePassword"> Change Password</span></a></li>
                    <li><a href="<?= $cdn; ?>user/logout">Logout</a></li>
                </ul>
                <div class="ps-block--vendor-dashboard">
                    <div class="ps-block__header">
                        <h3>Recent Service Request</h3>
                    </div>
                    <div class="ps-block__content">
                        <form class="ps-form--vendor-datetimepicker" action="#" method="POST">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text" id="time-from">From</span></div>
                                        <input class="form-control ps-datepicker" aria-label="Username" aria-describedby="time-from">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text" id="time-form">To</span></div>
                                        <input class="form-control ps-datepicker" aria-label="Username" aria-describedby="time-to">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                                    <button class="ps-btn"><i class="icon-sync2"></i> Search</button>
                                    <a href="<?= base_url(); ?>user/dashboard"> <button class="ps-btn"><i class="icon-sync2"></i> Reset</button></a>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table ps-table ps-table--vendor">
                                <thead>
                                    <tr>
                                        <th>date</th>
                                        <th>service</th>
                                        <th>customer name</th>
                                        <th>contact no</th>
                                        <th>address</th>
                                        <th>status</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="7"> <center>No Record Found</center></td>
                                        
                                    </tr>
                                  
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="ps-block--vendor-dashboard">
                    <div class="ps-block__header">
                        <h3>Our Services</h3> <button class="modeButton btn btn-success" data-toggle="modal" data-target="#myModalAddService"> Add New Service</button>
                    </div>
                    <div class="ps-block__content">
                       
                        <div class="table-responsive">
                            <table class="table ps-table ps-table--vendor">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Service Name</th>
                                        <th>Service Price</th>
                                        <th>Status</th>
                                        
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                               //  print_r($service);
                                 
                                 // print_r($serviceee);
                                 $counter = 0;
                                        foreach($service as $value){  
                                            $counter++;
                                            ?>
                                    <tr>
                                        <td><?= $counter ?></td>
                                        <td><?= $value->services ?></td>
                                        <td><?= $value->price ?> - <?= $value->price_max ?></td>
                                        <td><?php if($value->status == 1){ echo "<span style='color:white; background-color:green; padding:5px;'>Active</span>"; } else { echo "<span style='color:white; background-color:red; padding:5px;'>Deactivate</span>"; } ?></td>
                                        <td><button class="btn btn-info modeButton<?= $value->id ?>" data-toggle="modal" data-target="#myModal<?= $value->id ?>"> Edit</button></td>
  
                                       
                                    </tr>

                                            <div class="modal" id="myModal<?= $value->id ?>" style="margin-top:60px;">
                                                <div class="modal-dialog model-mobile-height" style="max-width: 666px!important; background-color: #fff!important;">
                                                <div class="modal-content">
                                                
                                                    <!-- Modal Header -->
                                                    <div class="modal-header">
                                                    <h4 class="modal-title">Edit <?= $value->services ?> Service</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <!-- Modal body -->
                                                    <div class="modal-body">
                                                

                                                    <div class="container">
                <form action="<?= base_url(); ?>User/updateService" method="POST">
                   
                    <div class="ps-tabs col-md-12" style="width:100%">
                        <div class="ps-tab active" id="sign-in">
                            <div class="ps-form__content">
                                <input type="hidden" name="table_id" value="<?= $value->id ?>" />
                                <input type="hidden" name="service_id" value="<?= $value->services_id ?>" />
                                
                                <h5>Service Name</h5>
                                <div class="form-group col-md-6">
                                    <select class="form-control" name="service_id">
                                            <option value="">Please Select Service</option>
                                            <?php 
                                            if(!empty($service_list)){
                                            foreach($service_list as $valuee){ ?>
                                                <option <?php if(in_array($valuee->id, $serviceee)){ echo "disabled"; } ?> <?php if($valuee->id == $value->services_id){ echo "selected"; } ?> value="<?= $valuee->id ?>"><?= $valuee->services ?></option>
                                            <?php } } ?>
                                    </select>
                                </div>

                                <h5>Min Price</h5>
                                <div class="form-group col-md-6 form-forgot">
                                    <input class="form-control"  value="<?= $value->price ?>" type="text"  name="price" placeholder="Min Price">
                                </div>

                                <h5>Max Price</h5>
                                <div class="form-group col-md-6 form-forgot">
                                    <input class="form-control"  value="<?= $value->price_max ?>" type="text"  name="price_max" placeholder="Max Price">
                                </div>

                                <h5>About Service</h5>
                                <div class="form-group col-md-6 form-forgot">
                                    <textarea class="form-control" name="description"  rows="3"><?= $value->description ?></textarea>
                                </div>

                                <h5>Status</h5>
                                <div class="form-group col-md-6">
                                    <select class="form-control" name="status">
                                            <option value="" >Please Select Status</option>
                                            <option value="1" <?php if($value->status==1){ echo "selected"; } ?>>Active</option>
                                            <option value="0" <?php if($value->status==0){ echo "selected"; } ?>>Deactive</option>
                                    </select>
                                </div>
                                
                                <div class="form-group col-md-6 form-forgot">
                                <button type="submit" class="btn btn-info">Save Details</button>
                                                    <button type="button" data-dismiss="modal" class="btn btn-info">Close</button>
                                                    <br><br><br><br>
                                                    </div>
                               
                               
                            </div>
                          
                        </div>
                    
                    </div>            </div>

                                                
                                                    </div>
                                                    
                                                    <!-- Modal footer -->
                                                    
                                                    </form>
                                                    
                                                </div>
                                                </div>
                                            </div>
    


                                    <?php
                                        }
                                        
                                                    
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>





    <div class="modal" id="myModalAddService" style="margin-top:60px; ">
                                                <div class="modal-dialog model-mobile-height" style="max-width: 666px!important; background-color: #fff!important;">
                                                <div class="modal-content">
                                                
                                                    <!-- Modal Header -->
                                                    <div class="modal-header">
                                                    <h4 class="modal-title">Add Service</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <!-- Modal body -->
                                                    <div class="modal-body">
                                                

                                                    <div class="container">
                <form action="<?= base_url(); ?>User/addService" method="POST">
                   
                    <div class="ps-tabs col-md-12" style="width:100%">
                        <div class="ps-tab active" id="sign-in">
                            <div class="ps-form__content">
                                <input type="hidden" name="shop_id" value="<?= $shop_id_parlor ?>" />
                                <h5>Choose Service Type</h5>
                                <div class="form-group col-md-6">
                                    <select class="form-control service_type" name="service_type" id="service_type">
                                            <option value="0">OLD Service</option>
                                            <option value="1">NEW Service</option>
                                           
                                    </select>
                                </div>
                                <div class="old_service" style="display:;">
                                <h5>Service Name</h5>
                                <div class="form-group col-md-6">
                                    <select class="form-control" name="service_id">
                                            <option value="">Please Select Service</option>
                                            <?php 
                                            if(!empty($service_list)){
                                            foreach($service_list as $valuee){ ?>
                                                <option <?php if(in_array($valuee->id, $serviceee)){ echo "disabled"; } ?>  value="<?= $valuee->id ?>"><?= $valuee->services ?></option>
                                            <?php } } ?>
                                    </select>
                                </div>
                                </div>
                                <div class="new_service" style="display:none;">
                                    <h5>Service Name </h5>
                                    <div class="form-group col-md-6 form-forgot">
                                        <input class="form-control"  value="" type="text"  name="service_name" placeholder="Service Name">
                                    </div>

                                  
                                    <div class="form-group col-md-6 form-forgot" style="display:none;">
                                        <input class="form-control service_image"  value="" type="file"  name="service_image" placeholder="Service Name">
                                    </div>
                                </div>
                                        
                        <script type="text/javascript">
                        $(document).ready(function(){
                           // $('.service_image').on('change', function(){
                               // var v = $('.service_image').val();
                            //    $.ajax({
                             //       type: 'post',
                              //      url: '<?php echo $this->config->base_url() ?>Upload/uploadServiceImage,
                               //     success: function (data) {
                               //         console.log(data);
                                      

                               //     }
                                  //  });

                         //   });
                            $('.service_type').on('change', function(){
                                //alert(service_type);
                               var service_type = $('.service_type').val();
                               if(service_type==0){
                                    $('.new_service').hide();
                                    $('.old_service').show();
                               } else {
                                    $('.new_service').show();
                                    $('.old_service').hide();
                               }
                            });
                        });
                                    </script>
                                <h5>Min Price</h5>
                                <div class="form-group col-md-6 form-forgot">
                                    <input class="form-control"  value="" type="text"  name="price" placeholder="Min Price">
                                </div>

                                <h5>Max Price</h5>
                                <div class="form-group col-md-6 form-forgot">
                                    <input class="form-control"  value="" type="text"  name="price_max" placeholder="Max Price">
                                </div>

                                <h5>About Service</h5>
                                <div class="form-group col-md-6 form-forgot">
                                    <textarea class="form-control" name="description"  rows="3"></textarea>
                                </div>

                                <h5>Status</h5>
                                <div class="form-group col-md-6">
                                    <select class="form-control" name="status">
                                            <option value="" >Please Select Status</option>
                                            <option value="1" <?php if($value->status==1){ echo "selected"; } ?>>Active</option>
                                            <option value="0" <?php if($value->status==0){ echo "selected"; } ?>>Deactive</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 form-forgot">
                                <button type="submit" class="btn btn-info">Save Details</button>
                                                    <button type="button" data-dismiss="modal" class="btn btn-info">Close</button>
                                                    <br><br><br><br>
                                                    </div>
                               
                               
                            </div>
                          
                        </div>
                    
                    </div>            </div>

                                                
                                                    </div>
                                                    
                                                    <!-- Modal footer -->
                                                    
                                                    </form>
                                                    
                                                </div>
                                                </div>
                                            </div>
    
  <script type="text/javascript">
$(document).ready(function(){
    // $(".modeButton").trigger('click'); 
});
</script>

<div class="modal" id="myModalChangePassword" style="margin-top:60px; ">
                                                <div class="modal-dialog" style="max-width: 666px!important;">
                                                <div class="modal-content">
                                                <div class="alert alert-success success_pass" style="display:none;">
                                          <strong>Success!</strong> Password Changed Successfully.....
                                          </div>
                                                    <!-- Modal Header -->
                                                    <div class="modal-header">
                                                    <h4 class="modal-title">Change Password</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <!-- Modal body -->
                                                    <div class="modal-body">
                                                

                                                    <div class="container">
                <form action="#" method="POST">
                   
                    <div class="ps-tabs col-md-12" style="width:100%">
                        <div class="ps-tab active" id="sign-in">
                            <div class="ps-form__content">
                               
                              
                                <h5>Old Password</h5>
                                <div class="form-group col-md-6 form-forgot">
                                    <input class="form-control old_pass"  value="" type="password"  name="old_password" placeholder="Old Password">
                                    <span style="color:red" class="error_password"></span>
                                </div>

                                <h5>New Password</h5>
                                <div class="form-group col-md-6 form-forgot">
                                    <input class="form-control newPass"  value="" type="password"  name="new_password" placeholder="New Password">
                                </div>

                             
                                <div class="form-group col-md-6 form-forgot">
                                <button type="button" class="btn btn-info changePassword">Change Password</button>
                                                    <button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
                                                    <br><br><br><br>
                                                    </div>
                               
                               <script>
                               $('.changePassword').click(function(){
                                   var status = 0;
                                    var old_pass =  $('.old_pass').val();
                                    var newPass =  $('.newPass').val();
                                    if(old_pass==''){
                                            $('.old_pass').css('border-color', 'red');
                                             status = 1;
                                             $('.error_password').text('');
                                        
                                    } else {
                                            $('.old_pass').css('border-color', '');
                                            $('.error_password').text('');
                                            
                                            
                                    }
                                    
                                    if(newPass==''){
                                            $('.newPass').css('border-color', 'red');
                                            $('.error_password').text('');
                                            status = 1;
                                        
                                    } else {
                                            $('.newPass').css('border-color', '');
                                            $('.error_password').text('');
                                    }
                                    // alert('Testing');
                                   // alert();
                                      if(status == 0){
                                        //  alert();
                                             $.ajax({
                                                type: 'post',
                                                url: '<?php echo $this->config->base_url() ?>user/changePassword/'+old_pass+'/'+newPass,
                                                success: function (data) {
                                                    console.log(data);
                                                    if(data==0){
                                                        $('.error_password').text('Please Entery Old Password Valid....');
                                                            
                                                    } else {
                                                        
                                                        $('.success_pass').show();
                                                         var delay = 2000; 
                                                    var url = 'http://makingyouchange.com/user/dashboard'
                                                    setTimeout(function(){ window.location = url; }, delay);
        
        
        
                                                           // window.location.href = "http://makingyouchange.com/user/dashboard";
                                                    }
                                                    
                                                
                                                }
                                            });
                                      }
                              
                               });
                               
                                   </script>
                               </script>
                            </div>
                          
                        </div>
                    
                    </div>            </div>

                                                
                                                    </div>
                                                    
                                                    <!-- Modal footer -->
                                                    
                                                    </form>
                                                    
                                                </div>
                                                </div>
                                            </div>
