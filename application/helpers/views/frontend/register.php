<div id="breadcrumbs">
  <div class="container">
    <ul>
      <li><a href="#">Home</a></li>
      <li>Register</li>
    </ul>
  </div>
  <!-- / container -->
</div>
<!-- / body -->

<div id="body">
  <div class="container">
    <div id="content" class="full">


      <div class="total-count" style="padding:12px;">
        <h3 style="line-height:1.2"><?=$message?></h3>

        <form action="<?=base_url()?>user/registration" method="post">
          <input type="text" placeholder="Your Name" required name="name" class="in-register"/><br>
          <input type="text" placeholder="Company Name" required name="company" class="in-register"/><br>

          <input type="text" placeholder="Email Address" required name="email" class="in-register"/><br>
          <input type="text" placeholder="Telephone No" required name="mobile" class="in-register"/><br>
          <input type="text" placeholder="Tax Id No" required name="tax" class="in-register"/><br>

          <input type="text" placeholder="Password" required name="password" class="in-register"/><br>

          <!-- <select name="gender">
            <option value="1">Male</option><option value="2">Female</option>
          </select><br> -->

          <input type="text" name="address" required placeholder="Address" class="in-register"/><br>

          <button type="submit" class="btn-grey">REGISTER</button>
        </form>


      </div>

    </div>
    <!-- / content -->
  </div>
  <!-- / container -->
</div>
<!-- / body -->
