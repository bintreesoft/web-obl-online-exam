<style>
@media only screen and (max-width: 600px) {
  .slider-width-mobile {
    height: 180px;
  }
}
@media only screen and (max-width: 1000px) {
  .product-width-mobile {
    height: 180px!important;
  }
}
</style>
<div id="homepage-4">
       
        <div class="ps-deal-of-day">
            <div class="container">
                <div class="ps-section__header">
                    <div class="ps-block--countdown-deal">
                   
                        <div class="ps-block__left">
                        <br>
                            <h3>Our Latest Products</h3>
                        </div>
                       
                    </div><a href="#">View all</a>
                </div>
                <div class="ps-section__content">
                    <div class="ps-carousel--nav owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="30" data-owl-nav="true" data-owl-dots="true" data-owl-item="5" data-owl-item-xs="2" data-owl-item-sm="3" data-owl-item-md="4" data-owl-item-lg="4" data-owl-item-xl="5" data-owl-duration="1000" data-owl-mousedrag="on">
                        <div class="ps-product ps-product--inner">
                            <div class="ps-product__thumbnail"><a href="#"><img src="<?=base_url()?>img/products/1.jpg" alt="" class="product-width-mobile" height="314px"></a>
                                <div class="ps-product__badge">Coming Soon</div>
                                <ul class="ps-product__actions">
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Read More"><i class="icon-bag2"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="icon-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to Whishlist"><i class="icon-heart"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><i class="icon-chart-bars"></i></a></li>
                                </ul>
                            </div>
                            <div class="ps-product__container">
                                <p class="ps-product__price sale">RS 999 <del>1100.00 </del><small>18% off</small></p>
                                <div class="ps-product__content"><a class="ps-product__title" href="#">Indo Era</a>
                                    <p>Sold by:<a href="#"> Make You  Change</a></p>
                                    <div class="ps-product__rating">
                                        <select class="ps-rating" data-read-only="true">
                                            <option value="1">1</option>
                                            <option value="1">2</option>
                                            <option value="1">3</option>
                                            <option value="1">4</option>
                                            <option value="2">5</option>
                                        </select><span>01</span>
                                    </div>
                                    <div class="ps-product__progress-bar ps-progress" data-value="37">
                                        <div class="ps-progress__value"><span></span></div>
                                        <p>Sold:95</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ps-product ps-product--inner">
                            <div class="ps-product__thumbnail"><a href="#"><img src="<?=base_url()?>img/products/2.jpg" alt="" class="product-width-mobile" height="314px"></a>
                                <div class="ps-product__badge out-stock">Coming Soon</div>
                                <ul class="ps-product__actions">
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Read More"><i class="icon-bag2"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="icon-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to Whishlist"><i class="icon-heart"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><i class="icon-chart-bars"></i></a></li>
                                </ul>
                            </div>
                            <div class="ps-product__container">
                                <p class="ps-product__price">Rs 101.99<small>18% off</small></p>
                                <div class="ps-product__content"><a class="ps-product__title" href="#">Libas</a>
                                    <p>Sold by:<a href="#">Make You  Change</a></p>
                                    <div class="ps-product__rating">
                                        <select class="ps-rating" data-read-only="true">
                                            <option value="1">1</option>
                                            <option value="1">2</option>
                                            <option value="1">3</option>
                                            <option value="1">4</option>
                                            <option value="2">5</option>
                                        </select><span>01</span>
                                    </div>
                                    <div class="ps-product__progress-bar ps-progress" data-value="55">
                                        <div class="ps-progress__value"><span></span></div>
                                        <p>Sold:9</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ps-product ps-product--inner">
                            <div class="ps-product__thumbnail"><a href="#"><img src="<?=base_url()?>img/products/3.jpg" alt="" class="product-width-mobile" height="314px"></a>
                                <div class="ps-product__badge">Coming Soon</div>
                                <ul class="ps-product__actions">
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Read More"><i class="icon-bag2"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="icon-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to Whishlist"><i class="icon-heart"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><i class="icon-chart-bars"></i></a></li>
                                </ul>
                            </div>
                            <div class="ps-product__container">
                                <p class="ps-product__price sale">Rs 42.00 <del>$60.00 </del><small>18% off</small></p>
                                <div class="ps-product__content"><a class="ps-product__title" href="#">VLCC Gold Facial Kit, 60g</a>
                                    <p>Sold by:<a href="#">Make You  Change</a></p>
                                    <div class="ps-product__rating">
                                        <select class="ps-rating" data-read-only="true">
                                            <option value="1">1</option>
                                            <option value="1">2</option>
                                            <option value="1">3</option>
                                            <option value="1">4</option>
                                            <option value="2">5</option>
                                        </select><span>02</span>
                                    </div>
                                    <div class="ps-product__progress-bar ps-progress" data-value="39">
                                        <div class="ps-progress__value"><span></span></div>
                                        <p>Sold:50</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ps-product ps-product--inner">
                            <div class="ps-product__thumbnail"><a href="#"><img src="http://www.o3plus.com/skin/frontend/default/grayscale/images/hbox/box3.jpg" alt="" class="product-width-mobile" height="314px"></a>
                                <div class="ps-product__badge out-stock">Coming Soon</div>
                                <ul class="ps-product__actions">
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Read More"><i class="icon-bag2"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="icon-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to Whishlist"><i class="icon-heart"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><i class="icon-chart-bars"></i></a></li>
                                </ul>
                            </div>
                            <div class="ps-product__container">
                                <p class="ps-product__price">Rs 320.00<small>18% off</small></p>
                                <div class="ps-product__content"><a class="ps-product__title" href="#">Pro Artist</a>
                                    <p>Sold by:<a href="#"> Make You  Change</a></p>
                                    <div class="ps-product__rating">
                                        <select class="ps-rating" data-read-only="true">
                                            <option value="1">1</option>
                                            <option value="1">2</option>
                                            <option value="1">3</option>
                                            <option value="1">4</option>
                                            <option value="2">5</option>
                                        </select><span>01</span>
                                    </div>
                                    <div class="ps-product__progress-bar ps-progress" data-value="38">
                                        <div class="ps-progress__value"><span></span></div>
                                        <p>Sold:41</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ps-product ps-product--inner">
                            <div class="ps-product__thumbnail"><a href="#"><img src="<?=base_url()?>img/products/6.jpg" alt="" class="product-width-mobile" height="314px"></a>
                                <div class="ps-product__badge out-stock">Coming Soon</div>
                                <ul class="ps-product__actions">
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Read More"><i class="icon-bag2"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="icon-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to Whishlist"><i class="icon-heart"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><i class="icon-chart-bars"></i></a></li>
                                </ul>
                            </div>
                            <div class="ps-product__container">
                                <p class="ps-product__price">Rs 85.00<small>18% off</small></p>
                                <div class="ps-product__content"><a class="ps-product__title" href="#">VEU Reborn Essential</a>
                                    <p>Sold by:<a href="#">Make You  Change</a></p>
                                    <div class="ps-product__rating">
                                        <select class="ps-rating" data-read-only="true">
                                            <option value="1">1</option>
                                            <option value="1">2</option>
                                            <option value="1">3</option>
                                            <option value="1">4</option>
                                            <option value="2">5</option>
                                        </select><span>01</span>
                                    </div>
                                    <div class="ps-product__progress-bar ps-progress" data-value="16">
                                        <div class="ps-progress__value"><span></span></div>
                                        <p>Sold:26</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ps-product ps-product--inner">
                            <div class="ps-product__thumbnail"><a href="#"><img src="https://media-asia-cdn.oriflame.com/-/media/Images/Catalog/Products/_global/3/33/337/33709.ashx?u=0101010000&w=234&imageFormat=Jpeg&ib=%23F5F5F5&q=70" alt="" class="product-width-mobile" height="314px"></a>
                                <div class="ps-product__badge out-stock">Coming Soon</div>
                                <ul class="ps-product__actions">
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Read More"><i class="icon-bag2"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="icon-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to Whishlist"><i class="icon-heart"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><i class="icon-chart-bars"></i></a></li>
                                </ul>
                            </div>
                            <div class="ps-product__container">
                                <p class="ps-product__price">Rs 92.00<small>18% off</small></p>
                                <div class="ps-product__content"><a class="ps-product__title" href="#">Pore Minimising Primer</a>
                                    <p>Sold by:<a href="#"> Make You  Change</a></p>
                                    <div class="ps-product__rating">
                                        <select class="ps-rating" data-read-only="true">
                                            <option value="1">1</option>
                                            <option value="1">2</option>
                                            <option value="1">3</option>
                                            <option value="1">4</option>
                                            <option value="2">5</option>
                                        </select><span>01</span>
                                    </div>
                                    <div class="ps-product__progress-bar ps-progress" data-value="77">
                                        <div class="ps-progress__value"><span></span></div>
                                        <p>Sold:14</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
       
        
        
    </div>