<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<div class="ps-page--single ps-page--vendor">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="<?=base_url();?>">Home</a></li>
                    <li>Store List</li>
                </ul>
            </div>
        </div>
        <section class="ps-store-list">
            <div class="container">
                
                <div class="ps-section__wrapper">
                    <div class="ps-section__left">
                        <aside class="widget widget--vendor">
                            <h3 class="widget-title">Search</h3>
                            <input class="form-control" type="text" placeholder="Search...">
                        </aside>
                        <aside class="widget widget--vendor">
                            <h3 class="widget-title">Filter by Service</h3>
                            <div class="form-group">
                                <select class="ps-select">
                                    <option>Lighting</option>
                                    <option>Exterior</option>
                                    <option>Custom Grilles</option>
                                    <option>Wheels & Tires</option>
                                    <option>Performance</option>
                                </select>
                            </div>
                        </aside>
                        <aside class="widget widget--vendor">
                            <h3 class="widget-title">Filter by Location</h3>
                            <div class="form-group">
                                <select class="ps-select">
                                    <option>Chooose Location</option>
                                   
                                </select>
                            </div>
                           
                            <div class="auto-widget">
    <p>Your Skills: <input type="text" id="skill_input"/></p>
</div>
                        </aside>
                        <script>
$(function() {
    $("#skill_input").autocomplete({
        alert();
        source: "<?php echo base_url('skills/autocompleteData'); ?>",
    });
});

$(function() {
    $("#skill_input").autocomplete({
        source: "<?php echo base_url('skills/autocompleteData'); ?>",
        select: function( event, ui ) {
            event.preventDefault();
            $("#skill_input").val(ui.item.id);
        }
    });
});

$( ".selector" ).autocomplete({
    source: [ "PHP", "Python", "Ruby", "JavaScript", "MySQL", "Oracle" ]
});


$( ".selector" ).autocomplete({
    source: "http://codexworld.com"
});



$( ".selector" ).autocomplete({
    minLength: 5
});


$( ".selector" ).autocomplete({
    select: function( event, ui ) {}
});
</script>
                    </div>
                    <div class="ps-section__right">
                        <section class="ps-store-box">
                            <div class="ps-section__header">
                                <p>Showing 1 -8 of 22 results</p>
                                <select class="ps-select">
                                    <option value="1">Sort by Newest: old to news</option>
                                    <option value="2">Sort by Newest: New to old</option>
                                    <option value="3">Sort by average rating: low to hight</option>
                                    <option value="3">Sort by average rating: hight to low</option>
                                </select>
                            </div>
                            <div class="ps-section__content">
                                <div class="row">
                                <?php if(!empty($pralour)){ 
                                    foreach($pralour as $val){ 
                                    ?>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                        <article class="ps-block--store">
                                            <div class="ps-block__thumbnail bg--cover" data-background="<?php if($val->background_image!=''){ echo base_url().'myc-admin/'.$val->background_image; } else { echo base_url().'img/p_banners.jpg'; }?>"></div>
                                            <div class="ps-block__content">
                                                <div class="ps-block__author"><a class="ps-block__user" href="<?= base_url(); ?>Home/shopDetails/<?= $val->shop_idd ?>"><img src="<?php if($val->user_image!=''){ echo base_url().'myc-admin/'.$val->user_image; } else { echo base_url()."img/vendor/store/user/4.jpg"; }?>" alt="" height="100px;"></a><a class="ps-btn" href="<?= base_url(); ?>Home/shopDetails/<?= $val->shop_idd ?>">Visit Store</a></div>
                                                <h4><?= $val->name?></h4>
                                                <select class="ps-rating" data-read-only="true">
                                                    <option value="1">1</option>
                                                    <option value="1">2</option>
                                                    <option value="1">3</option>
                                                    <option value="1">4</option>
                                                    <option value="2">5</option>
                                                </select>
                                                <p><?=$val->address?></p>
                                                <ul class="ps-block__contact">
                                                    <li><i class="icon-user"></i><?php if(!empty($val->owner_name)){ echo $val->owner_name; } else { echo "Not Found"; } ?></li>
                                                    <li><i class="icon-telephone"></i><?php if(!empty($val->contact)){ echo $val->contact; } else { echo "Not Found"; } ?></li>
                                                </ul>
                                                <div class="ps-block__inquiry"><a href="<?= base_url(); ?>Home/shopDetails/<?= $val->shop_idd ?>"><i class="icon-question-circle"></i> inquiry</a></div>
                                            </div>
                                        </article>
                                    </div>
                                <?php } } ?>
                                </div>
                                <!--div class="ps-pagination">
                                    <ul class="pagination">
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">Next Page<i class="icon-chevron-right"></i></a></li>
                                    </ul>
                                </div-->
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="ps-newsletter">
        <div class="container">
            <form class="ps-form--newsletter" action="http://nouthemes.net/html/martfury/do_action" method="post">
                <div class="row">
                    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__left">
                            <h3>Newsletter</h3>
                            <p>Subcribe to get information about products and coupons</p>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__right">
                            <div class="form-group--nest">
                                <input class="form-control" type="email" placeholder="Email address">
                                <button class="ps-btn">Subscribe</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>