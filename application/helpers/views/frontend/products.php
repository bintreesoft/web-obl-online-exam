


<div id="breadcrumbs">
  <div class="container">
    <ul>
      <li><a href="#">Home</a></li>
      <li>Product results</li>
    </ul>
  </div>
  <!-- / container -->
</div>
<!-- / body -->

<div id="body">
  <div class="container">
    <div><?=$catData->name?> <?php if ($subcatId!=null): ?>
          <span>      =>  <?=$subcatData->name?><span>
    <?php endif; ?> (<?=sizeof($product)?>) </div>
    <div class="products-wrap">
      <aside id="sidebar">
        <!-- <div class="widget">
          <h3>Products per page:</h3>
          <fieldset>
            <input checked type="checkbox">
            <label>8</label>
            <input type="checkbox">
            <label>16</label>
            <input type="checkbox">
            <label>32</label>
          </fieldset>
        </div> -->


        <?php foreach ($catData->subcat as $key => $value): ?>
          <div class="widget" style="border-bottom:none;padding:0px">

             <a href="<?=base_url().'Home/products/'.$catData->cat_id.'/'.$value->subcat_id?>">   <h3 <?php if ($subcatId == $value->subcat_id): ?>
               style="color:yellow;margin-top:30px";
             <?php else: ?>
               style="margin-top:30px";
             <?php endif; ?>><?=$value->name?></h3></a>

            <!-- <fieldset style="margin-bottom:14px"> -->

              <?php foreach ($value->lowcat as $key => $lowcat): ?>
              <!-- <input type="checkbox" class="lowcat <?=$lowcat->lowcat_id?>"> -->
                <!-- <label><?php echo $lowcat->name;?></label> -->
               <a href="<?=base_url().'Home/products/'.$catData->cat_id.'/'.$value->subcat_id.'/'.$lowcat->lowcat_id?>">    <p style="color:<?php if ($lowcat->lowcat_id == $lowcatId): ?>
                  yellow
                <?php else: ?>
                  white
                <?php endif; ?>;margin-left:24px"><?php echo $lowcat->name;?></p></a>

              <?php endforeach; ?>


            <!-- </fieldset> -->
          </div>
        <?php endforeach; ?>
        <!-- <div class="widget">
          <h3>Sort by:</h3>
          <fieldset>
            <input checked type="checkbox">
            <label>Popularity</label>
            <input type="checkbox">
            <label>Date</label>
            <input type="checkbox">
            <label>Price</label>
          </fieldset>
        </div>
        <div class="widget">
          <h3>Condition:</h3>
          <fieldset>
            <input checked type="checkbox">
            <label>New</label>
            <input type="checkbox">
            <label>Used</label>
          </fieldset>
        </div>
        <div class="widget">
          <h3>Price range:</h3>
          <fieldset>
            <div id="price-range"></div>
          </fieldset>
        </div> -->
      </aside>
      <div id="content">
        <section class="row" style="margin-left:0px !important">







            <?php if ($subcatId!=null): ?>


              <?php foreach ($product as $key => $value): ?>
                <div class="col-md-4">
                <div class="products" >
                <article style="margin-right:10px !important;margin-bottom:10px;margin-left:0px">
                  <div style="height:155px;">
                    <?php if ($value->url!=null): ?>
                      <a><img width="155" src="<?=$cdn?><?=$value->url?>" alt=""></a>

                    <?php endif; ?>
                </div>
                  <h3 style="height:150px;padding:8px"><a><?=$value->name?></a></h3>
                  <!-- <h4><a href="product.html">$990.00</a></h4> -->
                  <a href="<?=base_url()?>Home/product/<?=$value->product_id?>" class="btn-add">VIEW MORE</a>
                </article>
              </div>
            </div>
              <?php endforeach; ?>

            <?php else: ?>


              <?php if ($catId==84): ?>
                <div  class="d-flex justify-content-center md-12">  <h2 class="p-2 bd-highlight">Please select a category</h2></div>

              <?php else: ?>

                <?php foreach ($product as $key => $value): ?>
                  <div class="col-md-4">
                  <div class="products" >
                  <article style="margin-right:10px !important;margin-bottom:10px;margin-left:0px">
                    <div style="height:155px;">
                      <?php if ($value->url!=null): ?>
                        <a><img width="155" src="<?=$cdn?><?=$value->url?>" alt=""></a>

                      <?php endif; ?>
                  </div>
                    <h3 style="height:150px;padding:8px"><a><?=$value->name?></a></h3>
                    <!-- <h4><a href="product.html">$990.00</a></h4> -->
                    <a href="<?=base_url()?>Home/product/<?=$value->product_id?>" class="btn-add">VIEW MORE</a>
                  </article>
                </div>
              </div>
                <?php endforeach; ?>

              <?php endif; ?>


            <?php endif; ?>






          <!-- <article>
            <a href="product.html"><img src="images/12.jpg" alt=""></a>
            <h3><a href="product.html">cupidatat non proident</a></h3>
            <h4><a href="product.html">$1 200.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/13.jpg" alt=""></a>
            <h3><a href="product.html">Duis aute irure</a></h3>
            <h4><a href="product.html">$2 650.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/14.jpg" alt=""></a>
            <h3><a href="product.html">magna aliqua</a></h3>
            <h4><a href="product.html">$3 500.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/15.jpg" alt=""></a>
            <h3><a href="product.html">Lorem ipsum dolor</a></h3>
            <h4><a href="product.html">$1 500.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/1.jpg" alt=""></a>
            <h3><a href="product.html">cupidatat non proident</a></h3>
            <h4><a href="product.html">$3 200.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/16.jpg" alt=""></a>
            <h3><a href="product.html">Duis aute irure</a></h3>
            <h4><a href="product.html">$2 650.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/17.jpg" alt=""></a>
            <h3><a href="product.html">magna aliqua</a></h3>
            <h4><a href="product.html">$3 500.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article> -->
        </section>
      </div>
      <!-- / content -->
    </div>
    <!-- <div class="pagination">
      <ul>
        <li><a href="#"><span class="ico-prev"></span></a></li>
        <li><a href="#">1</a></li>
        <li class="active"><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#"><span class="ico-next"></span></a></li>
      </ul>
    </div> -->
  </div>
  <!-- / container -->
</div>
<!-- / body -->
