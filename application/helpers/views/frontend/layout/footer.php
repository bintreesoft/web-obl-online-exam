
<footer class="ps-footer">

        <div class="container">
            <div class="ps-footer__widgets">
                <aside class="widget widget_footer widget_contact-us"  style="width:33%;">
                    <h4 class="widget-title">Contact us</h4>
                    <div class="widget_content">
                        <p>Call us 24/7</p>
                        <h3><?= $footer['4']->value ?></h3>
                        <p><?= $footer['3']->value ?></p>
                        <ul class="ps-list--social">
                            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a class="instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Quick links</h4>
                    <ul class="ps-list--link">
                        <li><a href="#">Policy</a></li>
                        <li><a href="#">Term & Condition</a></li>
                        <li><a href="#">Shipping</a></li>
                        <li><a href="#">Return</a></li>
                        <li><a href="#">FAQs</a></li>
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Company</h4>
                    <ul class="ps-list--link">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Affilate</a></li>
                        <li><a href="#">Career</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Bussiness</h4>
                    <ul class="ps-list--link">
                        <li><a href="#">Our Press</a></li>
                        <li><a href="#">Checkout</a></li>
                        <li><a href="#">My account</a></li>
                        <li><a href="#">Shop</a></li>
                    </ul>
                </aside>
            </div>
            
            <div class="ps-footer__copyright">
                <p>© 2018 <?= $footer['0']->value ?>. All Rights Reserved</p>
                <p><span>We Using Safe Payment For:</span><a href="#"><img src="img/payment-method/1.jpg" alt=""></a><a href="#"><img src="img/payment-method/2.jpg" alt=""></a><a href="#"><img src="img/payment-method/3.jpg" alt=""></a><a href="#"><img src="img/payment-method/4.jpg" alt=""></a><a href="#"><img src="img/payment-method/5.jpg" alt=""></a></p>
            </div>
        </div>
    </footer>
    <style>
    @media only screen and (max-width: 600px) {
    .ps-form__content {
        float:left; 
        height:200px!important;
    }
    .mobile-popup {
        margin-top: 150px!important;
    }
    .ps-popup__content{
        height: 204px!important;
    }
    }
  
    </style>
   <?php if(isset($model)){ ?>
    <div class="ps-popup" id="subscribe" data-time="500">
        <div class="ps-popup__content bg--cover" data-background="<?= base_url(); ?>img/image-popup.jpg"><a class="ps-popup__close" href="#"><i class="icon-cross"></i></a>
        <div class="ps-popup__content bg--cover" data-background="<?= base_url(); ?>img/image-popup.jpg"><a class="ps-popup__close" href="#"><i class="icon-cross"></i></a>
                <div class="ps-form__content" style="float:left; height:300px;">
                        <div class="form-group mobile-popup" style="    margin-top: 224px;">
                        <a href="<?= base_url(); ?>home/products" ><button class="ps-btn">Products</button></a>
                        </div>
                        
                </div>

                <div class="ps-form__content" style="float:right; height:300px;">
                         <div class="form-group mobile-popup"  style="    margin-top: 224px;">
                            <a href="<?= base_url(); ?>home/parlour" ><button class="ps-btn">Parlour</button></a>
                        </div>
                      
                </div>
        </div>
        </div>
    </div>
    <?php } ?>
    <div id="back2top"><i class="pe-7s-angle-up"></i></div>
    <div class="ps-site-overlay"></div>
    <div id="loader-wrapper">
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="ps-search" id="site-search"><a class="ps-btn--close" href="#"></a>
        <div class="ps-search__content">
            <form class="ps-form--primary-search" action="http://nouthemes.net/html/martfury/do_action" method="post">
                <input class="form-control" type="text" placeholder="Search for...">
                <button><i class="aroma-magnifying-glass"></i></button>
            </form>
        </div>
    </div>
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    
    <script src="<?= $cdn; ?>plugins/popper.min.js"></script>
    <script src="<?= $cdn; ?>plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?= $cdn; ?>plugins/bootstrap4/js/bootstrap.min.js"></script>
    <script src="<?= $cdn; ?>plugins/imagesloaded.pkgd.min.js"></script>
    <script src="<?= $cdn; ?>plugins/masonry.pkgd.min.js"></script>
    <script src="<?= $cdn; ?>plugins/isotope.pkgd.min.js"></script>
    <script src="<?= $cdn; ?>plugins/jquery.matchHeight-min.js"></script>
    <script src="<?= $cdn; ?>plugins/slick/slick/slick.min.js"></script>
    <script src="<?= $cdn; ?>plugins/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?= $cdn; ?>plugins/slick-animation.min.js"></script>
    <script src="<?= $cdn; ?>plugins/lightGallery-master/dist/js/lightgallery-all.min.js"></script>
    <script src="<?= $cdn; ?>plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?= $cdn; ?>plugins/sticky-sidebar/dist/sticky-sidebar.min.js"></script>
    <script src="<?= $cdn; ?>plugins/jquery.slimscroll.min.js"></script>
    <script src="<?= $cdn; ?>plugins/select2/dist/js/select2.full.min.js"></script>
    <script src="<?= $cdn; ?>plugins/gmap3.min.js"></script>
    <!-- custom scripts-->
    <script src="<?= $cdn; ?>js/main.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxflHHc5FlDVI-J71pO7hM1QJNW1dRp4U&amp;region=GB"></script>
</body>


<!-- Mirrored from nouthemes.net/html/martfury/homepage-7.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Dec 2019 09:36:17 GMT -->
</html>