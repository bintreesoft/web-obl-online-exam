<?php // print_r($service); ?>
<div class="ps-page--blog">
        <div class="container">
            <div class="ps-page__header">
                <h1>Our Service</h1>
                <div class="ps-breadcrumb--2">
                    <ul class="breadcrumb">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Our Service</li>
                    </ul>
                </div>
            </div>
            <div class="ps-blog">
               
                <div class="ps-blog__content">
                    <div class="row">
                        <?php if(!empty($service)){ 
                            foreach($service as $values){ ?>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 " >
                            <div class="ps-post">
                                <div class="ps-post__thumbnail"  ><a class="ps-post__overlay" href="#"></a><img src="<?= $values->service_image!='' ?  base_url().'myc-admin/'.$values->service_image : base_url().'img/no.jpg' ?>" alt="" style="min-height:250px!important; height:250px!important; ">
                                    <div class="ps-post__badge"><i class="icon-volume-high"></i></div>
                                </div>
                                <div class="ps-post__content">
                                    <div class="ps-post__meta">
                                    </div><a class="ps-post__title" href="#"><?= $values->services ?></a>
                                </div>
                            </div>
                        </div>
                        <?php } 
                        } ?>
                        
                    </div>
                    <!--div class="ps-pagination">
                        <ul class="pagination">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">Next Page<i class="icon-chevron-right"></i></a></li>
                        </ul>
                    </div-->
                </div>
            </div>
        </div>
    </div>
    <div class="ps-newsletter">
        <div class="ps-container">
            <form class="ps-form--newsletter" action="http://nouthemes.net/html/martfury/do_action" method="post">
                <div class="row">
                    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__left">
                            <h3>Newsletter</h3>
                            <p>Subcribe to get information about products and coupons</p>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__right">
                            <div class="form-group--nest">
                                <input class="form-control" type="email" placeholder="Email address">
                                <button class="ps-btn">Subscribe</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>