


<div id="breadcrumbs">
  <div class="container">
    <ul>
      <li><a href="#">Home</a></li>
      <li>Product results</li>
    </ul>
  </div>
  <!-- / container -->
</div>
<!-- / body -->

<div id="body">
  <div class="container">
    <div style="padding:12px;font-size:24px;margin-left:12px">You Searched For " <?=$title?> " total results ( <?=sizeof($product)?> )</div>
    <div class="products-wrap">

      <div id="content">
        <section class="products">



          <?php foreach ($product as $key => $value): ?>
            <article style="margin:24px">
              <div style="height:155px;">
                <?php if ($value->url!=null): ?>
                  <a><img width="155" src="<?=$cdn?><?=$value->url?>" alt=""></a>

                <?php endif; ?>
            </div>
              <h3 style="height:150px;padding:8px"><a><?=$value->name?></a></h3>
              <!-- <h4><a href="product.html">$990.00</a></h4> -->
              <a href="<?=base_url()?>Home/product/<?=$value->product_id?>" class="btn-add">VIEW MORE</a>
            </article>
          <?php endforeach; ?>
          <!-- <article>
            <a href="product.html"><img src="images/12.jpg" alt=""></a>
            <h3><a href="product.html">cupidatat non proident</a></h3>
            <h4><a href="product.html">$1 200.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/13.jpg" alt=""></a>
            <h3><a href="product.html">Duis aute irure</a></h3>
            <h4><a href="product.html">$2 650.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/14.jpg" alt=""></a>
            <h3><a href="product.html">magna aliqua</a></h3>
            <h4><a href="product.html">$3 500.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/15.jpg" alt=""></a>
            <h3><a href="product.html">Lorem ipsum dolor</a></h3>
            <h4><a href="product.html">$1 500.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/1.jpg" alt=""></a>
            <h3><a href="product.html">cupidatat non proident</a></h3>
            <h4><a href="product.html">$3 200.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/16.jpg" alt=""></a>
            <h3><a href="product.html">Duis aute irure</a></h3>
            <h4><a href="product.html">$2 650.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article>
          <article>
            <a href="product.html"><img src="images/17.jpg" alt=""></a>
            <h3><a href="product.html">magna aliqua</a></h3>
            <h4><a href="product.html">$3 500.00</a></h4>
            <a href="cart.html" class="btn-add">Add to cart</a>
          </article> -->
        </section>
      </div>
      <!-- / content -->
    </div>
    <!-- <div class="pagination">
      <ul>
        <li><a href="#"><span class="ico-prev"></span></a></li>
        <li><a href="#">1</a></li>
        <li class="active"><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#"><span class="ico-next"></span></a></li>
      </ul>
    </div> -->
  </div>
  <!-- / container -->
</div>
<!-- / body -->
