<style>
@media only screen and (max-width: 600px) {
  .slider-width-mobile {
    height: 220px;
  }
}
@media only screen and (max-width: 1000px) {
  .product-width-mobile {
    height: 220px!important;
  }
}
</style>
    <div class="navigation--list">
        <div class="navigation__content"><a class="navigation__item ps-toggle--sidebar" href="#menu-mobile"><i class="icon-menu"></i><span> Menu</span></a><a class="navigation__item ps-toggle--sidebar" href="#navigation-mobile"><i class="icon-list4"></i><span> Categories</span></a><a class="navigation__item ps-toggle--sidebar" href="#search-sidebar"><i class="icon-magnifier"></i><span> Search</span></a><a class="navigation__item ps-toggle--sidebar" href="#cart-mobile"><i class="icon-bag2"></i><span> Cart</span></a></div>
    </div>
    <div class="ps-panel--sidebar" id="search-sidebar">
        <div class="ps-panel__header">
            <form class="ps-form--search-mobile" action="http://nouthemes.net/html/martfury/#" method="get">
                <div class="form-group--nest">
                    <input class="form-control" type="text" placeholder="Search something...">
                    <button><i class="icon-magnifier"></i></button>
                </div>
            </form>
        </div>
        <div class="navigation__content"></div>
    </div>
    
    <div class="ps-breadcrumb">
        <div class="ps-container">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><?= $shopDetails->name; ?></li>
            </ul>
        </div>
    </div>
            <?php if(!empty($shopImagesSlider)){ ?>
        <div id="homepage-1">
        <div class="ps-home-banner ps-home-banner--1">
            <div class="ps-container">
                <div class="ps-section__left">
                    <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on">
                    <?php
              // print_r($shopImages);die;
               foreach($shopImagesSlider as $value){ ?>
                <div class="ps-banner"><a href="#"><img src="<?=base_url().'myc-admin/'.$value->image?>" alt="" class="slider-width-mobile" ></a></div>               
                <?php }  ?>
                    </div>
                </div>
                <div class="ps-section__right">
                
                <a class="ps-collection" href="#"><img src="<?= base_url()?>img/ads/img-pro.jpg" style="max-height:150px;" alt=""></a>
                <a class="ps-collection" href="#"><img src="<?= base_url()?>img/ads/img-pro.jpg" style="max-height:150px;" alt=""></a>
                </div>
            </div>
        </div>
        </div> 
         <?php } ?>
    <div class="ps-page--shop">
        <div class="ps-container">
            <div class="ps-shop-banner">
                <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on">
                <?php
              // print_r($shopImages);die;
               /* foreach($shopImages as $value){ ?>
                <a href="#"><img src="<?= $cdn.'myc-admin/'.$value->image; ?>" alt=""></a>
                <?php }  */?>
                
                </div>
            </div>
            <div class="ps-vendor-store">
            <div class="container">
                <div class="ps-section__container">
                    <div class="ps-section__left">
                        <div class="ps-block--vendor">
                            <div class="ps-block__thumbnail">
                            
                            <?php if(isset($shopImages->image)!=''){ ?> <img src="<?php echo  $cdn.'myc-admin/'.$shopImages->image; ?>" alt=""> 
                            
                        <?php } else { ?>
                            <img src="<?php echo  base_url().'myc-admin/img/no.jpg'; ?>" alt=""> 
                        <?php } ?>
                            
                            </div>
                            <div class="ps-block__container">
                                <div class="ps-block__header">
                                    <h4><?= $shopDetails->name; ?></h4>
                                    <select class="ps-rating" data-read-only="true">
                                        <option value="1">1</option>
                                        <option value="1">2</option>
                                        <option value="1">3</option>
                                        <option value="1">4</option>
                                        <option value="2">5</option>
                                    </select>
                                    <p><strong>85% Positive</strong> (562 rating)</p>
                                </div><span class="ps-block__divider"></span>
                                <div class="ps-block__content">
                                    <p><strong>About US</strong>, <?= $shopDetails->description_about; ?></p>
                                    <figure>
                                        <figcaption>Follow us on social</figcaption>
                                        <ul class="ps-list--social-color">
                                            <li><a class="facebook" href="https://www.facebook.com/Making-You-Change-106952160816970/?modal=admin_todo_tour>"><i class="fa fa-facebook"></i></a></li>
                                            <li><a class="twitter" href="<?= $shopDetails->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                                            <li><a class="youtube" style="background-color: #ef2100;" href="https://www.youtube.com/channel/UCFa_gIjc_F5ShJKGZikCTyA"><i class="fa fa-youtube"></i></a></li>
                                            <li><a class="google" style="background-color: #0550da;" href="<?= $shopDetails->google; ?>"><i class="fa fa-google"></i></a></li>
                                        </ul>
                                    </figure>
                                </div>
                                <div class="ps-block__footer">
                                    <p>Owner Name: <strong><?= $shopDetails->owner_name; ?></strong></p>
                                    <p>Call us directly<strong><?= $shopDetails->contact; ?></strong></p>
                                    <p>Address : <?= $shopDetails->address; ?></p>
                                    <p>or Or if you have any question</p><a class="ps-btn ps-btn--fullwidth" href="#">Contact Seller</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ps-section__right">
                        
                        
                        <div class="ps-shopping ps-tab-root">
                            <div class="ps-shopping__header">
                                <p><strong> <?= count($service); ?></strong> Services</p>
                                
                                <div class="ps-shopping__actions">
                                   
                                    <div class="ps-shopping__view">
                                        <p>View </p>
                                        <ul class="ps-tab-list">
                                            <li class="active"><a href="#tab-1"><i class="icon-grid"></i></a></li>
                                            <li><a href="#tab-2"><i class="icon-list4"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="ps-tabs">
                                <div class="ps-tab active" id="tab-1">
                                    <div class="row">

                                    <?php 
                                    if(!empty($service)){ 
                                        foreach($service as $value){ ?>
                                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-6 ">
                                            <div class="ps-product">
                                                <div class="ps-product__thumbnail"><a href="#"><img src="<?= base_url(); ?>myc-admin/<?= $value->service_image ?>" alt=""></a>
                                                    <!--div class="ps-product__badge">11%</div-->
                                                  
                                                </div>
                                                <div class="ps-product__container"><a class="ps-product__vendor" href="#"></a>
                                                    <div class="ps-product__content"><a class="ps-product__title" href="#"><?= $value->services ?></a>
                                                        <div class="ps-product__rating">
                                                            <select class="ps-rating" data-read-only="true">
                                                                <option value="1">1</option>
                                                                <option value="1">2</option>
                                                                <option value="1">3</option>
                                                                <option value="1">4</option>
                                                                <option value="1">5</option>
                                                            </select><span>05</span>
                                                        </div>
                                                        <p class="ps-product__price sale">Rs. <?= $value->price ?> -  <?= $value->price_max ?></p>
                                                    </div>
                                                    <div class="ps-product__content hover"><a class="ps-product__title" href="#"><?= $value->services ?></a>
                                                        <p class="ps-product__price sale">Rs. <?= $value->price ?>   <?php if(!empty($value->price_max)){ echo  "- ".$value->price_max; } ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }  }?>
                                        
                                        
                                        
                                       
                                        
                                    </div>
                                    <!--div class="ps-pagination">
                                        <ul class="pagination">
                                            <li class="active"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">Next Page<i class="icon-chevron-right"></i></a></li>
                                        </ul>
                                    </div-->
                                </div>
                                <div class="ps-tab" id="tab-2">
                                <?php 
                                if(!empty($service)){ 
                                    foreach($service as $value){ ?>
                                    <div class="ps-product ps-product--wide">
                                        <div class="ps-product__thumbnail"><a href="#"><img src="<?= base_url(); ?>myc-admin/<?= $value->service_image ?>" alt=""></a>
                                            <!--div class="ps-product__badge">17%</div-->
                                        </div>
                                        <div class="ps-product__container">
                                            <div class="ps-product__content"><a class="ps-product__title" href="#"><?= $value->services ?></a>
                                                <div class="ps-product__rating">
                                                    <select class="ps-rating" data-read-only="true">
                                                        <option value="1">1</option>
                                                        <option value="1">2</option>
                                                        <option value="1">3</option>
                                                        <option value="1">4</option>
                                                        <option value="2">5</option>
                                                    </select><span>01</span>
                                                </div>
                                                <p class="ps-product__vendor">Sold by:<a href="#"><?= $shopDetails->name; ?></a></p>
                                                <p style="width: 450px;"><?= $value->description ?></p>
                                               
                                            </div>
                                            <div class="ps-product__shopping">
                                                <p class="ps-product__price sale">Rs. <?= $value->price ?> -  <?= $value->price_max ?></p><a class="ps-btn" href="#">Add to cart</a>
                                                <!--ul class="ps-product__actions">
                                                    <li><a href="#"><i class="icon-heart"></i> Wishlist</a></li>
                                                    <li><a href="#"><i class="icon-chart-bars"></i> Compare</a></li>
                                                </ul-->
                                            </div>
                                        </div>
                                    </div>

                                    <?php }  }?>
                                    <!--div class="ps-pagination">
                                        <ul class="pagination">
                                            <li class="active"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">Next Page<i class="icon-chevron-right"></i></a></li>
                                        </ul>
                                    </div-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <!--div class="ps-shop-brand"><a href="#"><img src="<?= $cdn; ?>img/brand/1.jpg" alt=""></a><a href="#"><img src="<?= $cdn; ?>img/brand/2.jpg" alt=""></a><a href="#"><img src="<?= $cdn; ?>img/brand/3.jpg" alt=""></a><a href="#"><img src="<?= $cdn; ?>img/brand/4.jpg" alt=""></a><a href="#"><img src="<?= $cdn; ?>img/brand/5.jpg" alt=""></a><a href="#"><img src="<?= $cdn; ?>img/brand/6.jpg" alt=""></a><a href="#"><img src="<?= $cdn; ?>img/brand/7.jpg" alt=""></a><a href="#"><img src="<?= $cdn; ?>img/brand/8.jpg" alt=""></a></div>
            <div class="ps-shop-categories">
                <div class="row align-content-lg-stretch">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                        <div class="ps-block--category-2" data-mh="categories">
                            <div class="ps-block__thumbnail"><img src="<?= $cdn; ?>img/categories/shop/1.jpg" alt=""></div>
                            <div class="ps-block__content">
                                <h4>Clothing &amp; Apparel</h4>
                                <ul>
                                    <li><a href="#">Accessories</a></li>
                                    <li><a href="#">Bags</a></li>
                                    <li><a href="#">Kid's Fashion</a></li>
                                    <li><a href="#">Mens</a></li>
                                    <li><a href="#">Shoes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                        <div class="ps-block--category-2" data-mh="categories">
                            <div class="ps-block__thumbnail"><img src="<?= $cdn; ?>img/categories/shop/2.jpg" alt=""></div>
                            <div class="ps-block__content">
                                <h4>Garden &amp; Kitchen</h4>
                                <ul>
                                    <li><a href="#">Cookware</a></li>
                                    <li><a href="#">Decoration</a></li>
                                    <li><a href="#">Furniture</a></li>
                                    <li><a href="#">Garden Tools</a></li>
                                    <li><a href="#">Home Improvement</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                        <div class="ps-block--category-2" data-mh="categories">
                            <div class="ps-block__thumbnail"><img src="<?= $cdn; ?>img/categories/shop/3.jpg" alt=""></div>
                            <div class="ps-block__content">
                                <h4>Consumer Electrics</h4>
                                <ul>
                                    <li><a href="#">Air Conditioners</a></li>
                                    <li><a href="#">Audios &amp; Theaters</a></li>
                                    <li><a href="#">Car Electronics</a></li>
                                    <li><a href="#">Office Electronics</a></li>
                                    <li><a href="#">Refrigerations</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                        <div class="ps-block--category-2" data-mh="categories">
                            <div class="ps-block__thumbnail"><img src="<?= $cdn; ?>img/categories/shop/4.jpg" alt=""></div>
                            <div class="ps-block__content">
                                <h4>Health &amp; Beauty</h4>
                                <ul>
                                    <li><a href="#">Equipments</a></li>
                                    <li><a href="#">Hair Care</a></li>
                                    <li><a href="#">Perfumer</a></li>
                                    <li><a href="#">Skin Care</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                        <div class="ps-block--category-2" data-mh="categories">
                            <div class="ps-block__thumbnail"><img src="<?= $cdn; ?>img/categories/shop/5.jpg" alt=""></div>
                            <div class="ps-block__content">
                                <h4>Computers &amp; Technologies</h4>
                                <ul>
                                    <li><a href="#">Desktop PC</a></li>
                                    <li><a href="#">Laptop</a></li>
                                    <li><a href="#">Smartphones</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                        <div class="ps-block--category-2" data-mh="categories">
                            <div class="ps-block__thumbnail"><img src="<?= $cdn; ?>img/categories/shop/6.jpg" alt=""></div>
                            <div class="ps-block__content">
                                <h4>Jewelry &amp; Watches</h4>
                                <ul>
                                    <li><a href="#">Gemstones Jewelry</a></li>
                                    <li><a href="#">Men's Watches</a></li>
                                    <li><a href="#">Women's Watches</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                        <div class="ps-block--category-2" data-mh="categories">
                            <div class="ps-block__thumbnail"><img src="<?= $cdn; ?>img/categories/shop/7.jpg" alt=""></div>
                            <div class="ps-block__content">
                                <h4>Phone &amp; Accessories</h4>
                                <ul>
                                    <li><a href="#">Iphone 8</a></li>
                                    <li><a href="#">Iphone X</a></li>
                                    <li><a href="#">Samsung Note 8</a></li>
                                    <li><a href="#">Samsung S8</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                        <div class="ps-block--category-2" data-mh="categories">
                            <div class="ps-block__thumbnail"><img src="<?= $cdn; ?>img/categories/shop/8.jpg" alt=""></div>
                            <div class="ps-block__content">
                                <h4>Sport &amp; Outdoor</h4>
                                <ul>
                                    <li><a href="#">Freezer Burn</a></li>
                                    <li><a href="#">Frigde Cooler</a></li>
                                    <li><a href="#">Wine Cabinets</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div-->
            
        </div>
    </div>
    <div class="ps-newsletter">
        <div class="container">
            <form class="ps-form--newsletter" action="http://nouthemes.net/html/martfury/do_action" method="post">
                <div class="row">
                    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__left">
                            <h3>Newsletter</h3>
                            <p>Subcribe to get information about products and coupons</p>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__right">
                            <div class="form-group--nest">
                                <input class="form-control" type="email" placeholder="Email address">
                                <button class="ps-btn">Subscribe</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
