<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class ProductModel extends CI_Model {



    function get_products(){

    }


    function get_product_on_category($catId){


      return $this->db->query("select product.* , pi.url , pi.is_main from product
      left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
      join product_to_category as ptc on ptc.product_id = product.product_id and ptc.cat_id = $catId")->result();



    }



    function get_product_on_category_count($catId){


      return $this->db->query("select  count(product.product_id) as count from product
      join product_to_category as ptc on ptc.product_id = product.product_id and ptc.cat_id = $catId")->row()->count;



    }





    function get_product_on_subcat($subcatId){


      return $this->db->query("select product.* , pi.url , pi.is_main from product
      left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
      join product_to_subcat as pts on pts.product_id = product.product_id and pts.subcat_id = $subcatId")->result();



    }


    function get_product_on_lowcat($subcatId){


      return $this->db->query("select product.* , pi.url , pi.is_main from product
      left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
      join product_to_lowcat as pts on pts.product_id = product.product_id and pts.lowcat_id = $subcatId")->result();



    }



    function get_product($productId){

      $data['detail'] = $this->db->query("select * from product where product_id = $productId")->row();
      $data['img'] = $this->db->query("select * from product_img where product_id = $productId order by is_main desc")->result();

      return $data;

    }


    function get_home_product(){


      $data = $this->db->query("select product.* , pi.url , pi.is_main from product
      left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
      join product_to_category as ptc on ptc.product_id = product.product_id
      order by product.sort_order LIMIT 10")->result();

      return $data;



    }


    function get_home_latest_product(){


      $data = $this->db->query("select product.* , pi.url , pi.is_main from product
      left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
      join product_to_category as ptc on ptc.product_id = product.product_id
      where product.display_home = 1
      order by product.sort_order LIMIT 10")->result();


      $proCount =   sizeof($data);

      if($proCount<10){
        $proCount = 10-$proCount;


        $moredata = $this->db->query("select product.* , pi.url , pi.is_main from product
        left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
        join product_to_category as ptc on ptc.product_id = product.product_id
        where product.display_home != 1
        order by  RAND() LIMIT $proCount")->result();


      }else{
        $moredata = array();
      }


    $data =   array_merge($data,$moredata);

      return $data;



    }

    function get_home_liked_product(){


      $data = $this->db->query("select product.* , pi.url , pi.is_main from product
      left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
      join product_to_category as ptc on ptc.product_id = product.product_id
      where product.display_home = 2
      order by product.sort_order LIMIT 10")->result();


      $proCount = sizeof($data);


      if($proCount<10){
        $proCount = 10-$proCount;


        $moredata = $this->db->query("select product.* , pi.url , pi.is_main from product
        left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
        join product_to_category as ptc on ptc.product_id = product.product_id
        where product.display_home != 2
        order by  RAND() LIMIT $proCount")->result();


      }else{
        $moredata = array();
      }



      $data =  array_merge($data,$moredata);

      return $data;



    }







}
