<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class HelperModel extends CI_Model {




  function forgotpass($userId){

    $email = $this->db->get_where('user' , array('user_id'=>$userId))->row()->email;



    $this->load->library('email');
    $this->email->set_header('MIME-Version', '1.0; charset=utf-8');


    $this->email->set_header('Content-type', 'text/html');
    $this->email->from('info@tresorjewelryinc.com', 'Tresorjewelryinc');
    $this->email->to($email);
    // $this->email->cc('another@another-example.com');
    // $this->email->bcc('them@their-example.com');

    $this->email->subject('Reset Password');


    $data['company'] = $this->db->get_where('settings' , array('key'=>'system_name'))->row()->value;
    $data['user'] = $this->db->get_where('user' , array('user_id'=>$userId))->row()->name;
    $data['verify'] =base_url()."user/updatepass/".$this->generatePassHash($userId);
    $message = $this->load->view('template/passwordreset', $data, true);


    $this->email->message($message);

    // $this->email->send();
    //
    // echo $this->email->print_debugger();

    if($this->email->send()){
//Success email Sent
       echo $this->email->print_debugger();
       // echo "true";
    }else{
       //Email Failed To Send
       echo $this->email->print_debugger();
       // echo 'false';
    }

  }



  function register($userId){



            $email = $this->db->get_where('user' , array('user_id'=>$userId))->row()->email;



            $this->load->library('email');
            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');


            $this->email->set_header('Content-type', 'text/html');
            $this->email->from('info@tresorjewelryinc.com', 'Tresorjewelryinc');
            $this->email->to($email);
            // $this->email->cc('another@another-example.com');
            // $this->email->bcc('them@their-example.com');

            $this->email->subject('Registration Complete');


            $data['company'] = $this->db->get_where('settings' , array('key'=>'system_name'))->row()->value;
            $data['user'] = $this->db->get_where('user' , array('user_id'=>$userId))->row()->name;
            $data['verify'] =base_url()."user/verify/".$this->generateHash($userId);
            $message = $this->load->view('template/confirmation', $data, true);


            $this->email->message($message);

            // $this->email->send();
            //
            // echo $this->email->print_debugger();

            if($this->email->send()){
       //Success email Sent
               echo $this->email->print_debugger();
               // echo "true";
            }else{
               //Email Failed To Send
               echo $this->email->print_debugger();
               // echo 'false';
            }



            //send mail to admin


            $userData = $this->db->get_where('user' , array('user_id'=>$userId))->row();



            $this->load->library('email');
            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');


            $this->email->set_header('Content-type', 'text/html');
            $this->email->from('info@tresorjewelryinc.com', 'Tresorjewelryinc');
            $this->email->to("tresorjewelryinc@gmail.com");
            // $this->email->cc('another@another-example.com');
            // $this->email->bcc('them@their-example.com');

            $this->email->subject('New User Registered');


            // $data['company'] = $this->db->get_where('settings' , array('key'=>'system_name'))->row()->value;
            // $data['user'] = $this->db->get_where('user' , array('user_id'=>$userId))->row()->name;
            // $data['verify'] =base_url()."user/verify/".$this->generateHash($userId);
            // $message = $this->load->view('template/confirmation', $data, true);


            $message = "New User registered on tresorjewelryinc portal<br>";
            $message.= "Username : ".$userData->name."<br>";
            $message.= "Email : ".$userData->email."<br>";
            $message.= "Address : ".$userData->address."<br>";

            $message.= "Please goto to portal to activate the new user.";




            $this->email->message($message);

            // $this->email->send();
            //
            // echo $this->email->print_debugger();

            if($this->email->send()){
       //Success email Sent
               echo $this->email->print_debugger();
               // echo "true";
            }else{
               //Email Failed To Send
               echo $this->email->print_debugger();
               // echo 'false';
            }



  }

  function generateHash($userId){





    $bytes = openssl_random_pseudo_bytes(32);
    $checkHash = base64_encode($bytes);

    $isUnique = $this->checkHash($checkHash);

    while($isUnique){


      $bytes = openssl_random_pseudo_bytes(32);
      $checkHash = base64_encode($bytes);

          $isUnique = $this->checkHash($checkHash);


    }

    $ip['user_id'] = $userId;
    $ip['status'] = 0;
    $ip['hash'] = $checkHash = str_replace("/" , "" , $checkHash);

    $this->db->insert('user_verify' , $ip);


    return $checkHash;

  }





  function checkHash($checkHash){





            $hashData = $this->db->get_where('user_verify' , array('hash'=>$checkHash , 'status'=>1))->row();

            if($hashData!=null){
              return true;

            }else{
              return false;
            }


  }


  function generatePassHash($userId){





    $bytes = openssl_random_pseudo_bytes(32);
    $checkHash = base64_encode($bytes);

    $isUnique = $this->checkPassHash($checkHash);

    while($isUnique){


      $bytes = openssl_random_pseudo_bytes(32);
      $checkHash = base64_encode($bytes);

          $isUnique = $this->checPasskHash($checkHash);


    }

    $ip['user_id'] = $userId;
    $ip['status'] = 0;
    $ip['hash'] = $checkHash = str_replace("/" , "" , $checkHash);

    $this->db->insert('password_reset' , $ip);


    return $checkHash;

  }




    function checkPassHash($checkHash){





              $hashData = $this->db->get_where('password_reset' , array('hash'=>$checkHash , 'status'=>1))->row();

              if($hashData!=null){
                return true;

              }else{
                return false;
              }


    }







}
