<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class UiModel extends CI_Model {




  function base64url_encode($data) {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
  }
  
  function base64url_decode($data) {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
  }



  function get_category_with_details(){


  return  $this->db->query("select * from category
                      left join subcat on category.cat_id = subcat.cat_id
                      order by category.cat_id")->result();


                    }


    function get_category(){

      return  $this->db->query("select * from category
                          order by category.cat_id")->result();
                          }


    function get_head_category(){

      return  $this->db->query("select * from category where display = 1
                          order by category.cat_id")->result();
                          }



    function get_category_on_id($catId){

      return  $this->db->query("select * from category
                          where cat_id = $catId")->row();
                          }


    function get_subcat($catId){

      return  $this->db->query("select * from subcat
                           where cat_id = $catId")->result();
                      }




      function get_lowcat($subId){

        return  $this->db->query("select * from lowcat
                             where subcat_id = $subId")->result();
                        }


                        function get_lowcat_on_id($subcatId){

                          return  $this->db->query("select * from lowcat
                                               where lowcat_id = $subcatId")->row();
                                          }
    function get_subcat_on_id($subcatId){

      return  $this->db->query("select * from subcat
                           where subcat_id = $subcatId")->row();
                      }



    function getCatLoop($catId){

      $category = $this->get_category_on_id($catId);





        $category->subcat = $this->get_subcat($catId);


        foreach ($category->subcat as $key => $value) {

          $category->subcat[$key]->lowcat = $this->get_lowcat($value->subcat_id);

        }



      return $category;
    }

    function getHeaderData(){

      $category = $this->get_head_category();



      //
      foreach ($category as $key => $value) {

        $category[$key]->subcat = $this->get_subcat($value->cat_id);

      }

      $head['category'] = $category;


      $head['shows'] = $this->getActiveShows();

      return $head;
    }


    function getActiveShows(){

      date_default_timezone_set('Asia/Kolkata');

      $date = date('Y-m-d');

      return  $this->db->query("select *, if(date>='$date' , 'active' , 'expired') as status from shows having status = 'active'")->result();

    }

    function renderView($path , $data = array()){

      // print_r($this->session->userdata());
      $session =   $this->session->userdata('data');
     $data['system_name'] =  $this->db->query("SELECT *  FROM settings where setting_id=1")->row();
     $data['system_address'] =  $this->db->query("SELECT *  FROM settings where setting_id=4")->row();
     $data['system_phone'] =  $this->db->query("SELECT *  FROM settings where setting_id=5")->row();
     $data['system_email'] =  $this->db->query("SELECT *  FROM settings where setting_id=6")->row();
     $data['logo'] =  $this->db->query("SELECT *  FROM settings where setting_id=15")->row();

      if(isset($this->session->userdata('data')['data']->user_id)){ $loginId = $this->session->userdata('data')['data']->user_id; } else { $loginId = ""; }
          $headData = $this->getHeaderData();
          $data['loginId'] = $loginId;
        
          $data['footer'] =  $this->db->query("SELECT *  FROM settings ")->result();
          $data['cdn'] = "http://localhost/quiz/";
      		$this->load->view('frontend/layout/header' , $data);
          $this->load->view($path,$data);
          

          $data['facebook'] =  $this->db->query("SELECT *  FROM settings where setting_id=11")->row();
          $data['twitter'] =  $this->db->query("SELECT *  FROM settings where setting_id=12")->row();
          $data['instagram'] =  $this->db->query("SELECT *  FROM settings where setting_id=13")->row();
          $data['youtube'] =  $this->db->query("SELECT *  FROM settings where setting_id=14")->row();

    	$this->load->view('frontend/layout/footer',$data);
    }



    function renderViewExam($path , $data = array()){

      // print_r($this->session->userdata());
      $session =   $this->session->userdata('data');
     $data['system_name'] =  $this->db->query("SELECT *  FROM settings where setting_id=1")->row();
     $data['system_address'] =  $this->db->query("SELECT *  FROM settings where setting_id=4")->row();
     $data['system_phone'] =  $this->db->query("SELECT *  FROM settings where setting_id=5")->row();
     $data['system_email'] =  $this->db->query("SELECT *  FROM settings where setting_id=6")->row();
     $data['logo'] =  $this->db->query("SELECT *  FROM settings where setting_id=15")->row();

      if(isset($this->session->userdata('data')['data']->user_id)){ $loginId = $this->session->userdata('data')['data']->user_id; } else { $loginId = ""; }
          $headData = $this->getHeaderData();
          $data['loginId'] = $loginId;
        
          $data['footer'] =  $this->db->query("SELECT *  FROM settings ")->result();
          $data['cdn'] = "http://localhost/quiz/";
      	//	$this->load->view('frontend/layout/header' , $data);
      		$this->load->view($path,$data);
    // 	$this->load->view('frontend/layout/footer',$data);
    }

    function get_Ques_done($exam_id,$user_id){

      return $this->db->query("select * from `exam_start`,`question`
       where `question`.`question_id`=`exam_start`.`question_id` and  `exam_start`.`exam_id`='$exam_id' and `exam_start`.`user_id`='$user_id' and `exam_start`.`is_deleted`=0 and `exam_start`.`answer`!='' and `exam_start`.`answer`=`question`.`answer`")->num_rows();
    }

    function get_solve_done($exam_id,$user_id){

      return $this->db->query("select * from `exam_start` WHERE  `exam_id`='$exam_id' and `user_id`='$user_id'  and `answer`!='' ")->num_rows();
   }

   function get_Ques_wrong($exam_id,$user_id){

    return $this->db->query("select * from `exam_start`,`question`
    where `question`.`question_id`=`exam_start`.`question_id` and  `exam_start`.`exam_id`='$exam_id' and `exam_start`.`user_id`='$user_id' and `exam_start`.`is_deleted`=0 and `exam_start`.`answer`!='' and `exam_start`.`answer`!=`question`.`answer`")->num_rows();
 }


 function getCheckExamRecod($exam_id,$user_id){

  return $this->db->query("select * from `exam_record` WHERE `is_deleted`=0 and `exam_id`='$exam_id' and `user_id`='$user_id'")->num_rows();
}

function exam_start($examRecord,$examRuning){
 // print_r($examRecord);
  $userId = $examRecord['user_id'];
  $exam_id = $examRecord['exam_id'];
  foreach($examRuning as $val){
    $ans =  $val['answer'];

$question_id = $val['question_id'];
    $this->db->where(array('exam_id'=>$exam_id,'user_id'=>$userId,'question_id'=>$question_id));
    $this->db->update('exam_start' , array('answer'=>$ans));
  }

}

        function renderApi($data = array()){

      // print_r($this->session->userdata());

    //   $session =   $this->session->userdata('data');

    //   if($path != 'frontend/home' && $path !='frontend/login' && $path != 'frontend/register' && $path != 'frontend/forgotpass' && $path != 'frontend/updatepassword'){

    //     if(!$session['status'])
    //       redirect('/home/login' , 'auto');

    //   }


        $headData = $this->getHeaderData();

          $data['category'] = $headData['category'];
          $data['shows'] = $headData['shows'];


          $data['cdn'] = "http://tresorjewelryinc.com/tresor-admin/";

      	echo json_encode($data);
    }



    function getBanner(){

      return $this->db->query("select *  from slider ")->result();
    }

    function getParlour(){
      return $this->db->query("select * from shop where trash='null'")->result();
    }

    function getPost(){

      return $this->db->query("select * , concat('http://tresorjewelryinc.com/tresor-admin/' , url) as url  from posts ")->result();
    }

}
