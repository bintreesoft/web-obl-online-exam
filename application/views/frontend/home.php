
<content>


<div class="New_homePage">


<div class="testSeies_head" style="background-color:#5959ff !important;">

<div class="container">

<h3 class="tst_banner_text">Save your study time upto 40%</h3>

<div class="row tst_explore_text" >
<div class="col-xs-4 col-sm-4 col-md-4 tst_banner_text_seprator">
<p>Perform</p>
</div>
<div class="col-xs-4 col-sm-4 col-md-4 tst_banner_text_seprator">
<p>Analyse</p>
</div>
<div class="col-xs-4 col-sm-4 col-md-4 tst_banner_text_seprator1">
<p>Improve</p>
</div>
</div>
</div>
</div>


<div class="Nhm_TestCounter">
<div class="container">
<div class="col-xs-3 col-sm-3 col-md-3">
<div class="Nhm_countNob Nhm_rightLine">
<span class="countIcon nobs_icon-1"></span>
<h3  class="ng-binding"><?= $total_users->value?></h3>
<p>Users</p>
</div>
</div>
<div class="col-xs-3 col-sm-3 col-md-3">
<div class="Nhm_countNob Nhm_rightLine">
<span class="countIcon nobs_icon-2"></span>
<h3  class="ng-binding"><?= $total_questions->value?></h3>
<p>Questions Attempted</p>
</div>
</div>
<div class="col-xs-3 col-sm-3 col-md-3">
<div class="Nhm_countNob Nhm_rightLine">
<span class="countIcon nobs_icon-3"></span>
<h3  class="ng-binding"><?= $total_exams->value?></h3>
<p>Exams</p>
</div>
</div>
<div class="col-xs-3 col-sm-3 col-md-3">
<div class="Nhm_countNob">
<span class="countIcon nobs_icon-4"></span>
<h3 class="ng-binding"><?= $total_test->value?></h3>
<p>Tests</p>
</div>
</div>
</div>
</div>

<div class="Nhm_popularExam ng-scope" >
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<h2 class="Nhm_heading-1 Nhm_heading-2">Popular Exams</h2>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<!-- ngRepeat: popularExam in popularExamsData -->
<?php foreach($exams as $value){
    
   // print_r($value);?>
<div class="Nhm-popularDiv ng-scope">
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>exams/subExam/<?php echo $value->cat_id; ?>">
<span class="Nw_ExmPoprIcon">
<img class="img-responsive" alt="exam icon" src="<?=base_url()?>admin/<?=$value->url?>">
</span>
<p  class="ng-binding"><?php echo $value->name; ?></p>
</a>
</div><!-- end ngRepeat: popularExam in popularExamsData -->
<?php } ?>
</div>
</div>
</div>
</div>


<div class="Nhm_popularExam">
<div class="container">
<div class="row">

<div class="col-xs-12 col-sm-12 col-md-12">
<h2 class="Nhm_heading-1 Nhm_heading-2">With Collection of Expected Questions</h2>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="item ts_QualitySlide">
<ul class="NwBanner-Slider">
<?php
foreach($slider  as $val){ ?>
<li style="background-color: #fff !important;">

<div class="row NwH_Quality">
<div class="col-xs-12 col-sm-6 col-md-6">
<h4>Get Free Tests!</h4>
<p><?=$val->description?></p>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
<img class="img-responsive" src="<?=base_url();?>admin/<?=$val->url?>" alt="Get Free Tests! image">
</div>
</div>
</li>
<?php } ?>


</ul>
</div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
<h2 class="Nhm_heading-1 Nhm_heading-2">PERSONALIZED PERFORMANCE ANALYSIS</h2>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="row NwH_ExmExplore NwH_Ts-about">
<div class="NwH_tsDetails">
<div class="col-md-7">
<p><?= $about->value?></p>
</div>
<div class="col-md-5">
<img class="NwH_tsDetails_img" src="<?=base_url();?>admin/<?= $about->url?>" alt="EduGorilla single feature" />
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="Nhm_Media Hm_comment">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<h2 class="Nhm_heading-1 Nhm_heading-2">Students Love Us</h2>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="item Nwh_showPartnr">
<ul id="NwH_partner_slider" class="Nhm_commentSlider">

<?php
foreach($posts  as $val){ ?>


<li style="background-color: #fff !important;">

<div class="NwH_studentComnt">
<img class="img-responsive" src="<?=base_url();?>admin/<?=$val->url ?>" alt="student"/>
<h4><?=$val->title ?></h4>
<p class="testimonialReadMore"><?=$val->subtitle ?></p>
</div>
</li>
<?php } ?>
</ul>
</div>
</div>
</div>
</div>
</div>


</div>
<script src="<?=base_url();?>static/js/multisliderc934.js?v=4ae0bbca19886e8c435b66b4b915df2b"></script>
<script src="<?=base_url();?>static/node_modules/lightslider/js/lightslidere666.js?v=6d048bed813ba18f9e6785a9abf45889"></script>
<script type="text/javascript">
$(document).ready(function() {
$("#NwBanner_offer").lightSlider({
loop:true,
keyPress:true,
item:4,
auto:true,
speed:500,
pauseOnHover: true,
pager: false
});
$(".NwBanner-Slider").lightSlider({
loop:true,
keyPress:true,
item:1,
auto:true,
speed:500,
pauseOnHover: true
});
// exam list Slide
$('#right-button').click(function() {
event.preventDefault();
$('#Nhm_content').animate({
scrollLeft: "+=300px"
}, "fast");
});
$('#left-button').click(function() {
event.preventDefault();
$('#Nhm_content').animate({
scrollLeft: "-=300px"
}, "fast");
});
//partner slide
$(".NwPartnerSlider").lightSlider({
loop:true,
keyPress:true,
item:7,
auto:true,
speed:500,
pauseOnHover: true
});
//comment slide
$("#NwH_partner_slider").lightSlider({
loop:true,
keyPress:true,
item:3,
auto:true,
speed:500,
pauseOnHover: true,
responsive : [{
breakpoint:480,
settings: {
item:1,
slideMove:1
}
}]
});
/* Readmore Testimonials */
var showChar = 200;
var ellipsestext = "";
var moretext = "more";
var lesstext = "less";
$('.testimonialReadMore').each(function() {
var content = $(this).html();
if(content.length > showChar) {
var c = content.substr(0, showChar);
var h = content.substr(showChar, content.length - showChar);
var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span style="display: none;">' + h + '</span>&nbsp;&nbsp;<a class="morelink">' + moretext + '</a>';
$(this).html(html);
$(this).parent().addClass('lessTestimonial');
}
});
$('.morelink').on('click', function(e){
if($(this).html() == 'less'){
$(this).prev().css('display', 'none');
$(this).parent().parent().addClass('lessTestimonial');
$(this).html(moretext);
} else{
$(this).prev().css('display', '');
$(this).parent().parent().removeClass('lessTestimonial');
$(this).parent().parent().parent().parent().css('height', '');
$(this).html(lesstext);
}
});
// exam sub list
$(".Explore_subExam").hide();
$(".parentmenu").show();
$('.parentmenu').click(function(){
$( this ).find(".Explore_subExam").slideToggle();
$(this).find('.NwH_arrow > i').toggleClass('fa fa-caret-up fa fa-caret-down');
});
const slider = document.querySelector('#Nhm_content');
let isDown = false, startX, scrollLeft;

});
</script>


</content>
