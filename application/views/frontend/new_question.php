
        <div id="main">
            <div class="NwQs_ExamTopic">
                <div class="NwQs_ExamNameTab">
                    <span class="NwQs_sectionText">Sections | </span>
                    <div class="NwQs_ExamName_scroll">
                        <button class="NwQs_Exname active" title="CBT" style="">
                            <span>CBT</span>
                        </button>
                    </div>

                </div>
                <!-- ngIf: totalLang.length > 1 -->
                
            </div>
            <div class="NwQs_ExamTopic NwQs_ExTiming" style="padding:1px;" ng-show="!insideInstruction">
                <div class="NwQs_ExamNameTab">
                    <ul class="ques_paper_breadcrumb" style="padding:0px 23px;">
                        <li>
                            <h4><b>Question <span ><?= $question_no ?></span>:</b></h4></li>
                    </ul>
                </div>
                <div class="NwQs_langS">
                    <p class="NwQs_marking">Marks: <span class="NwQs_posMark " >+1</span> <span class="NwQs_negMark " >-0.333</span></p>
                </div>
            </div>
          
            <div class="container-fluid" ng-show="!insideInstruction">
                <div class="row">
                    <div class="col-sm-12 col-md-12 NwQs_show_Ques">
                        <div id="General_Physics" class="NwQs_Content_Open">
                            <div>
                            <p class="ques_heading_text"><?= $question ?></p>
                            <?php if(isset($image_question)) {?>
                                <img  src="<?= base_url().'admin/'.$image_question ?>" alt="" />
                            <?php } ?>

                                <input type="hidden" value="<?= $question_no ?>" class="question_no" ?>
                                <input type="hidden" value="<?= $exam_id ?>" class="exam_id" ?>

                                <ul class="quest_ans_click " style="">
                                    <li class="">
                                        <label style="padding-top: 10px; padding-bottom: 9px;">
                                            <input type="radio" name="rdoAnswers" <?php if($answer=="a"){ echo "checked";} ?> value="a"  id="ans_c83b1804d45f41ed8a9814abdec98b290">
                                            <label for="ans_c83b1804d45f41ed8a9814abdec98b290"><?= $a ?></label>

                                            <?php if(isset($image_a)) {?>
                                                <img  src="<?= base_url().'admin/'.$image_a ?>" alt="" />
                                            <?php } ?>
                                        </label>
                                    </li>
                                    <li class="">
                                        <label style="padding-top: 10px; padding-bottom: 9px;">
                                            <input type="radio" name="rdoAnswers" <?php if($answer=="b"){ echo "checked";} ?> value="b" id="ans_c83b1804d45f41ed8a9814abdec98b291">
                                            <label for="ans_c83b1804d45f41ed8a9814abdec98b291"><?= $b ?></label>
                                            <?php if(isset($image_b)) {?>
                                                <img  src="<?= base_url().'admin/'.$image_b ?>" alt="" />
                                            <?php } ?>

                                        </label>
                                    </li>
                                    <li class="">
                                        <label style="padding-top: 10px; padding-bottom: 9px;">
                                            <input type="radio" name="rdoAnswers" <?php if($answer=="c"){ echo "checked";} ?> value="c"  id="ans_c83b1804d45f41ed8a9814abdec98b292">
                                            <label for="ans_c83b1804d45f41ed8a9814abdec98b292"><?= $c ?></label>
                                            <?php if(isset($image_c)) {?>
                                                <img  src="<?= base_url().'admin/'.$image_c ?>" alt="" />
                                            <?php } ?>

                                        </label>
                                    </li>
                                    <li class="">
                                        <label style="padding-top: 10px; padding-bottom: 9px;">
                                            <input type="radio" name="rdoAnswers" <?php if($answer=="d"){ echo "checked";} ?> value="d"  id="ans_c83b1804d45f41ed8a9814abdec98b293">
                                            <label for="ans_c83b1804d45f41ed8a9814abdec98b293"><?= $d ?></label>
                                            <?php if(isset($image_d)) {?>
                                                <img  src="<?= base_url().'admin/'.$image_d ?>" alt="" />
                                            <?php } ?>

                                        </label>
                                    </li>
                                    <?php if($e!='' || $image_e!=''){ ?>
                                    <li class="">
                                        <label style="padding-top: 10px; padding-bottom: 9px;">
                                            <input type="radio" name="rdoAnswers" <?php if($answer=="e"){ echo "checked";} ?> value="e"  id="ans_c83b1804dd45f41ed8a9814abdec98b293">
                                            <label for="ans_c83b1804dd45f41ed8a9814abdec98b293"><?= $e ?></label>
                                            <?php if(isset($image_e)) {?>
                                                <img  src="<?= base_url().'admin/'.$image_e ?>" alt="" />
                                            <?php } ?>

                                        </label>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="mobQS_closebtn" onclick="closeNav()"></a>
            <div class="quest_profile_ans">
                <div class="row user_detail">
                    <div class="ques_user_image">
                        <img class="img-responsive" alt="userimage" src="https://image.shutterstock.com/image-vector/profile-placeholder-image-gray-silhouette-600w-1190386324.jpg">
                        <p class="NwQs_marking NwQs_userName " ng-bind="user_data.name">mukesh</p>
                    </div>
                    <ul class="ques_ans_details">
                    <?php 
                    $Attempted=0;
                    $NotAttempted=0;
                    $NotVisit=0;
                    foreach($examRuning as $key=> $val){ 


                        if($val['status']==0){
                            $NotVisit++;
                        } else if($val['status']==1){
                            $NotAttempted++;
                        }else{
                            $Attempted++;
                        }
                      

                     } 
                     ?>
                        <li><span class="green " ><?=$Attempted?></span>Attempted</li>
                        <li><span ><?=$NotVisit?></span>Not Visited</li>
                        <li><span class="red " ><?=$NotAttempted?></span>Not Answered</li>
                    </ul>
                </div>
                <div class="section_name_class">
                    <b>Section: </b> <span >CBT</span>
                </div>
                <div class="quest_nobs_details">
                    <ul class="question_nob_list">
                    <?php 
                       
                       foreach($examRuning as $key=> $val){ 
                           if($val['status']==0){
                               echo '<li onclick="return openQuestion('.$val['counter'].','.$val['exam_id'].')"; style="background: white; border: 1px solid rgba(0, 0, 0, 0.51); color: rgb(0, 0, 0);">'.$val['question_no'].'</li>';
                           } else if($val['status']==1){
                               echo '<li onclick="return openQuestion('.$val['counter'].','.$val['exam_id'].')"; style="background: rgb(218, 84, 80); color: rgb(255, 255, 255); border-radius: 15px;">'.$val['question_no'].'</li>';

                           }else{
                               echo '<li onclick="return openQuestion('.$val['counter'].','.$val['exam_id'].')"; style="background: #3c763d; border: 1px solid rgba(0, 0, 0, 0.51); color: rgb(0, 0, 0);">'.$val['question_no'].'</li>';

                           }
                         

                        } ?>
                    </ul>
                    <div class="NwQs_report_submit">

                    <a href="<?=base_url();?>exam/result" style="display: block"> <button type="button" >SUBMIT TEST</button></a>
                    </div>
                </div>
            </div>
        </div>
</div>