<div id="breadcrumbs">
  <div class="container">
    <ul>
      <li><a href="#">Home</a></li>
      <li>Login</li>
    </ul>
  </div>
  <!-- / container -->
</div>
<!-- / body -->

<div id="body">
  <div class="container">
    <div id="content" class="full">


      <div class="total-count" style="padding:12px;">

        <?php
         if ($message!=''): ?>
          <h3 style="line-height:1.2;"><?=$message?></h3>

        <?php else: ?>
          <h3 style="line-height:1.2;">Generate Password Reset Link</h3>

        <?php endif; ?>

        <form action="<?=base_url()?>user/forgotpass/gen" method="post">
          <input type="text" placeholder="Email Address" name="email" class="in-register"/><br>

          <button type="submit" class="btn-grey">Generate Link</button>
        </form>


      </div>

    </div>
    <!-- / content -->
  </div>
  <!-- / container -->
</div>
<!-- / body -->
