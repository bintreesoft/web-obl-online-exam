<div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="mobQS_closebtn" onclick="closeNav()"></a>
            <div class="quest_profile_ans">
                <div class="row user_detail">
                    <div class="ques_user_image">
                        <img class="img-responsive" alt="userimage" src="https://image.shutterstock.com/image-vector/profile-placeholder-image-gray-silhouette-600w-1190386324.jpg">
                        <p class="NwQs_marking NwQs_userName " ng-bind="user_data.name">mukesh</p>
                    </div>
                    <ul class="ques_ans_details">
                    <?php 
                    $Attempted=0;
                    $NotAttempted=0;
                    $NotVisit=0;
                    foreach($examRuning as $key=> $val){ 


                        if($val['status']==0){
                            $NotVisit++;
                        } else if($val['status']==1){
                            $NotAttempted++;
                        }else{
                            $Attempted++;
                        }
                      

                     } ?>
                        <li><span class="green " ><?=$Attempted?></span>Attempted</li>
                        <li><span ><?=$NotVisit?></span>Not Visited</li>
                        <li><span class="red " ><?=$NotAttempted?></span>Not Answered</li>
                    </ul>
                </div>
                <div class="section_name_class">
                    <b>Section: </b> <span >CBT</span>
                </div>
                <div class="quest_nobs_details">
                    <ul class="question_nob_list">
                        <?php 
                        foreach($examRuning as $key=> $val){ 

                            if($val['status']==0){
                                echo '<li onclick="return openQuestion('.$val['counter'].','.$val['exam_id'].')"; style="background: white; border: 1px solid rgba(0, 0, 0, 0.51); color: rgb(0, 0, 0);">'.$val['question_no'].'</li>';
                            } else if($val['status']==1){
                                echo '<li onclick="return openQuestion('.$val['counter'].','.$val['exam_id'].')"; style="background: rgb(218, 84, 80); color: rgb(255, 255, 255); border-radius: 15px;">'.$val['question_no'].'</li>';

                            }else{
                                echo '<li onclick="return openQuestion('.$val['counter'].','.$val['exam_id'].')"; style="background: #3c763d; border: 1px solid rgba(0, 0, 0, 0.51); color: rgb(0, 0, 0);">'.$val['question_no'].'</li>';

                            }
                          

                         } ?>
                       
                    </ul>
                    <div class="NwQs_report_submit">

                    <a href="<?=base_url();?>exam/result" style="display: block"> <button type="button" >SUBMIT TEST</button></a>
                    </div>
                </div>
            </div>
        </div>
        <div id="main">
           
           
          
            <div class="container-fluid" ng-show="!insideInstruction">
                <div class="row">
                    <div class="col-sm-12 col-md-12 NwQs_show_Ques">
                        <div id="General_Physics" class="NwQs_Content_Open">
                            <div>
                            <div class="col-md-12">
                                    <a href="<?=base_url();?>exam/result" style="display: block"> <img src="<?=base_url();?>static/thankyoumsg.svg" alt="thankyou" class="img-svg">
                                    </a>
                                    
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>