
<style type="text/css">
	ol.qanswer li {
    line-height: 30px;
    padding: 10px 0;
    font-size: 15px;
}

ol.qanswer {
    padding-left: 0;
    margin-top: 10px;
}

.course_imf {
    width: 100%;
    float: left;
    box-shadow: 3px 4px 5px 1px #ccc;
    border: 4px solid rgb(102, 100, 212);
}

.papers {
    margin-top: 30px;
    margin-bottom: 30px;
}
/*-------------------*/

.cousr_video {
    margin-top: 10px;
}
ul.cousr_video li {
    font-size: 20px;
    display: block;
    margin: 15px 0;
}

ul.cousr_video {
    padding-left: 0;
}

ul.cousr_video li a{ color: #333; text-transform: capitalize; }

ul.cousr_video li a img {
    width: 30px;
    display: inline-block;
    margin-left: 10px;
}

ul.cousr_video li span {
    margin-right: 15px;
}


.lefts {
    text-align: left;
    position: relative;
    margin-bottom: 20px !important;
    display: block;
    padding-bottom: 20px;
}

.lefts:before {
    content: "";
    display: block;
    position: absolute;
    left: 0 !important;
    width: 170px;
    height: 4px;
    background: #6664d4;
    bottom: 0;
}

div#main-navigation {
    padding: 0;
}
.course_imf img {
    margin: 0 auto;
}

img.text-right.img-responsive {
    /* text-align: right; */
    float: right;
    margin-right: 10%;
}

.time_left {
    color: white;
    font-weight: 600;
    background-color: black;
    padding: 4px;
    font-size: 14px;
}
i.fa.fa-inr {
    padding: 0;
}

#df {
    color: green;
    font-size: 40px;
}
a#line_break {
    border-top: 2px solid white;
}

.exam_pattern_paper {
	background:#ecf0f5;
	padding:15px;
	border-radius:8px;
	box-shadow:7px 7px 10px -3px rgba(36, 37, 36, .1803921568627451);
	margin-bottom:15px;
 
}
div#google_translate_element {
	text-align:right;
	margin:0 auto;
	display:table;
	float:right;
	width:70px!important;
	position:relative;
	top:6px
}
ul.address_link {
	padding-left:0
}
.col-md-12.marquee_prize {
	color:red;
	font-weight:600;
	font-size:16px
}
.tot_prz {
	color:#fff;
	font-weight:600;
	background-color:#000;
	padding:4px;
	font-size:14px
}
.tab-content {
	-webkit-user-select:none;
	-khtml-user-select:none;
	-moz-user-select:none;
	-ms-user-select:none;
	-o-user-select:none;
	user-select:none
}
ul.socials_link li {
	display:inline-block;
	margin:0 3px;
	font-size:18px;
	text-align:center!important;
	background:#ddf0ff;
	width:30px;
	height:auto
}
ul.socials_link {
	padding-left:0;
	height:auto;
	text-align:left
}
span.light_line, span.light_line2 {
	width:100%;
	height:1px;
	float:none
}
ul.socials_link li a {
	color:#333;
	text-align:center!important;
	padding:6px 3px;
	display:block
}
span.light_line {
	background:#088ef9;
	display:block;
	margin:15px auto 0
}
.dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
	color:#262626;
	text-decoration:none;
	background-color:transparent!important
}
a.exam_emo span {
	text-align:center;
	width:100%;
	padding:0 10px;
	font-size:18px;
	display:table;
	margin:0 auto
}
.panel-title, .semi-disppp {
	font-size:14px
}
.online-feature:first-child {
	background:#6664d4
}
span.light_line2 {
	background:#088ef9;
	display:block;
	margin:0 auto
}
ul.quciklink {
	padding-left:0;
	margin-top:20px
}
ul.quciklink li {
	display:block;
	line-height:25px;
	font-size:14px;
	padding-left:20px;
	z-index:999999;
	width:50%;
	float:left
}
.footer-2.clearfix {
	background:#6664d4;
	padding:20px 0;
	color:#fff
}
ul.quciklink li a {
	color:#eee
}
.quickbar p {
	margin-top:23px!important;
	line-height:23px;
	text-align:justify
}
ul.quciklink li:before {
	content:"\f118";
	font-family:fontawesome;
	position:absolute;
	left:0;
	top:0
}
.payment {
	margin-top:20px
}
.header-alt {
	background:#111309;
	position:fixed;
	top:0;
	min-height:50px;
	left:0;
	right:0;
	z-index:99;
transition:all .8s
}
.select_exam.exma_ple {
	padding:40px 0
}
.semi-disppp {
	padding-top:10px;
	font-weight:500!important;
	line-height:30px;
	column-count:1;
	text-align:justify
}
.form-horizontal .control-label {
	padding-top:7px;
	margin-bottom:0;
	text-align:left!important
}
.input-group-addon {
	min-width:50px
}
.change_pass {
	background:0 0
}

ul.dropdown-menu.multi-level {
	left:9px!important
}
li.loginpanel a {
	background:#333
}
li.loginpanel {
	padding-left:0!important
}
li.regpanel {
	padding-left:5px!important
}
li.loginpanel a:hover {
	background:#010101!important;
	color:#fff!important
}
li.regpanel a {
	background:#333
}
li.regpanel a:hover {
	background:#010101!important;
	color:#fff!important
}
ul.quesr {
	padding-left:0
}
ul.quesr li {
    padding: 10px 0;
    border-bottom: 1px solid #357f83;
    margin-bottom: 5px;
    display: block;
    width: 100%;
    float: left;
}
.Exam-date h4, .corse_name {
	font-weight:700;
	border-bottom:1px solid #7878;
	padding-bottom:10px;
	text-align:center;
}
.Exam-date, .pricfe {
	margin-bottom:10px
}
.corse_name {
	font-size:22px;
	text-transform:uppercase
}
.pricfe {
	font-size:20px;
	font-weight:700;
	margin-top:10px
}
a.btn.btn-pay {
	background:#6664d4;
	border-radius:0;
	font-size:18px;
	color:#fff
}



.course_imf .exam_pattern_paper {
    margin-bottom: 0;
}


.corse_name {
    font-size: 25px;
    text-transform: capitalize;
    color: #6664d4;
	
}
.pricfe {
	font-size:20px;
	font-weight:700;
	margin-top:10px
}
a.btn.btn-pay {
	background:#6664d4;
	border-radius:0;
	font-size:18px;
	color:#fff;
	display: block;
}
a.full-url-single {
    color: #1d1b1b;
    font-weight: normal !important;
    font-size: 14px;
}

ul.quesr li strong {
    font-weight: 600;
	
}
ul.quesr {
    display: block;
    clear: both;
	    margin-bottom: 5px;
    margin-top: 5px;
    float: left;
}

</style>

<?php
$catId = $exams->category;
$langg = $exams->language;

 $caegory = $this->db->query("SELECT * FROM `category` WHERE is_deleted=0 and cat_id = $catId")->row();
 $langage = $this->db->query("SELECT * FROM `language` WHERE is_deleted=0 and language_id = $langg")->row();

?>

<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="mypapers">
					<h2 class=" lefts"><?= $exams->name; ?></h2>

					<p><?= $exams->description; ?> </p>
 
            <hr>

        
            <div class="clearfix"></div>

              <hr>


            <div class="row">

            <div class="col-md-12">
              <ul class="cousr_video">
						              
               
                
                
                
              </ul>

    
            </div>

            </div>
					

				</div>
				

			</div>

			
			<div class="col-md-4">
				<div class="course_imf">
					 <!--img src="<?= base_url(); ?>admin/<?= $category->url; ?>" class="img-responsive" alt="<?= $exams->name; ?>"-->
					<div class="exam_pattern_paper"> 
              <div class="corse_name"><?= $exams->name; ?></div>
              <ul class="quesr">
                <li><span class="pull-left"><strong>Exam Category :</strong></span><span class="pull-right"><?= $categoryy->name; ?></span></li>
                <li><span class="pull-left"><strong>Total Questions :</strong></span><span class="pull-right"><?= $exams->question; ?></span></li>
                <li><span class="pull-left"><strong>Exam Duration :</strong></span><span class="pull-right"> <?= $exams->minutes; ?> minutes</span></li>
                
<li><span class="pull-left"><strong>Cut off :</strong></span><span class="pull-right"><?= $exams->cut_off; ?> % </span></li>
                    <li><span class="pull-left"><strong>Negative marks :</strong></span><span class="pull-right"><?= $exams->negative; ?> % /Que</span></li>
                <li><span class="pull-left"><strong>Paper language :</strong></span><span class="pull-right"> <?= $langage->name; ?></span></li>

              </ul>
              
              <div class="clearfix"></div>


		          

			<a href="<?=base_url();?>exam/exam_start_user/<?= $exams->exam_id; ?>" class="btn btn-pay ready_exam clearfix" style="margin-bottom: 5px;">Exam Start</a>
				






            </div>
				</div>

			</div>



		</div>


	</div>