<footer>

<div class="school_footer">
<div class="container">
<div class="row menu_row">
<div class="col-xs-12 col-sm-12 col-md-4">
<div class="addrss">
<p><a target="_self" itemprop="url" href="<?=base_url();?>" rel="home"><img src="<?=base_url();?><?=$logo->value?>" alt="EduGorilla" class="logo1"></a></p>
<div class="eg-footer-social">
<ul class="social_link">

<li><a itemprop="sameAs" class="facebook_hover" href="<?=$facebook->value?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>


<li><a itemprop="sameAs" class="twitter_hover" href="<?=$twitter->value?>" target="_blank"><i class="fab fa-twitter"></i></a></li>


<li><a itemprop="sameAs" class="youtube_hover" href="<?=$youtube->value?>" target="_blank"><i class="fab fa-youtube"></i></a></li>


<li><a itemprop="sameAs" class="instagram_hover" href="<?=$instagram->value?>" target="_blank"><i class="fab fa-instagram"></i></a></li>

</ul>
</div>


<div id="google_translate_element"></div>
</div>
</div>

<div class="col-xs-12 col-sm-3 col-md-2">
<p class="footer_heading">ABOUT</p>
<ul class="sub_menu">
<li><a target="_blank" itemprop="url" href="#">About Us</a></li>
<li><a target="_blank" itemprop="url" href="#">Impact Stories</a></li>
<li><a target="_blank" itemprop="url" href="#">Media</a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-3 col-md-2">
<p class="footer_heading">HELP</p>
<ul class="sub_menu">
<li><a target="_blank" itemprop="url" href="#">Contact</a></li>
<li><a target="_blank" itemprop="url" href="#">Knowledge Base</a></li>
<li><a target="_blank" itemprop="url" href="#">Career With Us</a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-3 col-md-2">
<p class="footer_heading">STUDENT</p>
<ul class="sub_menu">
<li><a target="_blank" itemprop="url" href="#">List of Coachings</a></li>
<li><a target="_blank" itemprop="url" href="#">List of Schools</a></li>
<li><a target="_blank" itemprop="url" href="#">Forum</a></li>

</div>
<div class="col-xs-12 col-sm-3 col-md-2">
<p class="footer_heading">BUSINESS</p>
<ul class="sub_menu">
<li><a target="_blank" itemprop="url" href="#">Register Your Institute</a></li>
<li><a target="_blank" itemprop="url" href="#">Write For Us</a></li>
<li><a target="_blank" itemprop="url" href="#">Register Your Institute</a></li>
</ul>

</div>

</div>
<div class="row copyright_row">
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 eg_copyright_text">
<p>Copyright © <span class="p_year"></span> Bintree Softwares Pvt. Ltd</p>
</div>
<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 eg_copyright_link">
<ul class="copyright_menu">


</ul>
</div>

</div>
</div>
</div>

</footer>

<script>
jQuery(document).ready(function($) {
$("head").append(`<link rel="canonical" href="${location.origin+location.pathname}">`);
$(".TS_Search_Modal_head").show();
$(".testS_examSearch_list").show();
$("#admin_li").css('display', 'block');
$(".alert_container").css('display', 'table-cell');
$(".TsH_examSearch_list").show();
$(".TsH_text_seprator").show();
/* if(location.pathname != '/') {
$(".TsH_searchBar").css('visibility', 'visible');
} */
$(window).bind('scroll', function(e) {
var navHeight = $( window ).height() - 70;
if ($(window).scrollTop() > 100) {
$('#Tst_Fixed_navbar').addClass('fixed-header');
/* if($(window).scrollTop() > 600 && location.pathname == '/') {
$(".TsH_searchBar").css('visibility', 'visible');
}
else if(location.pathname == '/') {
$(".TsH_searchBar").css('visibility', 'hidden');
} */
}
else {
$('#Tst_Fixed_navbar').removeClass('fixed-header');
}
});
});
</script>

<script>
function getUrlArgument(arg) {
var vars = {};
var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
vars[key] = value;
});
if (arg in vars) {
return decodeURIComponent(vars[arg]);
}
return "";
}
var google_onetap = {
supportedAuthMethods: [
"https://accounts.google.com"
],
supportedIdTokenProviders: [{
uri: "https://accounts.google.com",
clientId: "765614789489-8q7pljsejr2mdjvv8hb87d6vn330pvn2.apps.googleusercontent.com"
}]
};
window.onGoogleYoloLoad = (googleyolo) => {
if(!getCookie('eg_user')) {
const hintPromise = googleyolo.hint(google_onetap);
hintPromise.then((credential) => {
if (credential.idToken) {
useGoogleIdTokenForAuth(credential);
googleyolo.disableAutoSignIn();
}
},
(error) => {
return;
});
}
};
var useGoogleIdTokenForAuth = function (credential) {
document.getElementById("ajaxLoadingSignUpBase").style.display = 'block';
var data = "from=google&id="+(Math.floor(Math.random() * 1000000000000000000000)).toString()+"&name="+credential.displayName+"&imgURL="+credential.profilePicture+"&email="+credential.id+"&idToken="+credential.idToken;
var url = "/api/v1/auth/loginsignup/fb/go/st1";
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
if (this.readyState == 4 && this.status == 200) {
var data = JSON.parse(xhttp.responseText);
document.getElementById("ajaxLoadingSignUpBase").style.display = 'none';
if(data.status) {
if(data.result.data.type == 'signup'){
document.getElementById("fb_go_signup_base").style.display = "block";
document.getElementById("temp").value = data.result.data.token;
}
else if (data.result.data.type == 'login') {
next = getUrlArgument("next");
if (next) {
window.location.href = getUrlArgument("next");
}
else {
location.reload();
}
}
}
}
};
xhttp.open("POST.html", url, true);
xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
xhttp.send(data);
}
function getCookie(key) {
var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
return keyValue ? keyValue[2] : null;
}
</script>

</body>

<!-- Mirrored from testseries.edugorilla.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Mar 2020 06:45:29 GMT -->
</html>