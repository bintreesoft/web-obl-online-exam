<!DOCTYPE html>
<html lang="en-US">
<?php   ?>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<base >
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?=base_url();?><?=$logo->value?>" />

<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
<link rel="stylesheet" href="<?=base_url();?>online/maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=base_url();?>online/use.fontawesome.com/releases/v5.12.1/css/all.css">
<script src="<?=base_url();?>online/ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?=base_url();?>online/maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-animate.min.js"></script>
<link rel="stylesheet" href="<?=base_url();?>static/css/test_series_common8735.css?v=1426ee27994456eaecb5822fd4b88484">
<link rel="stylesheet" href="<?=base_url();?>static/css/menu_desktop3011.css?v=38201e70ce0cf28436792148587d51f7">
<link rel="stylesheet" href="<?=base_url();?>static/css/footer_desktop11f6.css?v=216b472360ee7f681e82b18c96239f6e">
<link rel="stylesheet" href="<?=base_url();?>static/css/test_series_menu6d9e.css?v=d33c2b47ed01d8f6ee108ba0f9d5396a">
<script src="<?=base_url();?>static/js/menu_desktop0731.js?v=586a32f066a7ea41ca8cf015254ec332"></script>


<title>Free Exams nTest Series for Bank, SSC, JEE, CAT, GATE</title>

<link rel="canonical" href="index.html" />


<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
<link rel="stylesheet" href="<?=base_url();?>static/css/select2.min7dec.css?v=d44571114a90b9226cd654d3c7d9442c">
<link rel="stylesheet" href="<?=base_url();?>static/css/signup7dd1.css?v=b0309dfcbe1d9ffae5e6cb1bca79e423">
<script src="<?=base_url();?>static/js/signupeffb.js?v=f65d42df574305efd06943ec166a7e4e"></script>

<!-- Global site tag (gtag.js) - Google Analytics (edugorilla) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-77283231-2"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto%20Slab" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=PT+Sans&amp;display=swap" rel="stylesheet">
<script src="<?=base_url();?>static/js/homepage_new0e80.js?v=9aea95a4295bf7c405378b6899c2eea9"></script>
<link rel="stylesheet" href="<?=base_url();?>static/css/homepage_newcbe6.css?v=d6c6fa4b16c53ebc52ee6db5d2d6f159">
<link rel="stylesheet" href="<?=base_url();?>static/node_modules/lightslider/css/lightslider1fcd.css?v=9bd6e6060e07c0471bf1e233ca337029"/>
<script src="<?=base_url();?>static/js/signupeffb.js?v=f65d42df574305efd06943ec166a7e4e"></script>
<link rel="stylesheet" href="<?=base_url();?>static/css/ecatalog_L0Singled8cb.css?v=d7f4612ac9f6947eb26afc9bed3eb5f8">
<script src="<?=base_url();?>static/js/ecatalog_L0Singlea6f7.js?v=0ebde42745640047021127e6892dad39"></script>

<link rel="stylesheet" href="<?=base_url();?>static/css/ecatalog_L2Single.css?v=cea7deff851b72b7b76ab5bb415b5437">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-77283231-2"></script>

<style>
.ts_QualitySlide {
    padding: 32px 32px 32px 32px;
    background: #fff;
    border-radius: 4px;
    box-shadow: 0px 1px 6px 0px rgba(100, 100, 100, 0.12);
}
.lslide {
    background-color: #fff !important;

}
.Nhm_popularExam {
    background-color: #fff;

}
</style>

<style>
 
@media only screen and (max-width: 600px) {
    .TsH_rightMenu_nav {
        margin-right: 0px;
  }
 .desktop {
	  display:none;
  }
  .mobile {
	  display:block!important;
  }
  .lg_logPopup_Logo{
	
   background-position:left!important;
   margin-top:10px;
    
}
.row {
    width: 100%;
}
.ts_lg_rightPart {
    height: 100%;
    width: 106%!important;
    position: relative;
    overflow: unset;
    top: 0;
    display: inline-block;

}
.Ts_loginPage {
    width: 100%;
    margin: 0 auto;
    /* padding: 30px 40px; */
    font-family: Roboto;
}
  
}
@media (max-width: 767px) and (min-width: 300px){
    .Nhm-popularDiv {
    width: 131px;
}
}
@media (max-width: 767px) and (min-width: 280px){
    .slct_exam_series_exam {
    width: 42%;
}
.tests_div {
    width: 100%;
    min-height: 0px; 
}
}
@media (max-width: 767px) and (min-width: 280px){
    .exam_course_test_paper_start {
    width: 100%;
}
}

</style>
</head>
<body ng-app="TestSeries">
<header style="background-color:red !important;">

<div >
<div class="" id="promo_code_back"></div>
<div class="Ts_headerFull_W">
<nav class="navbar navbar-default TsH_Navigation" id="Tst_Fixed_navbar" style="background-color:#5959ff !important;">
<div class="TsH_toolbar" >
<div class="TsH_logo_image desktop">
<a target="_self" href="<?=base_url();?>"><span class="lg_logPopup_Logo" style="display: block;background: url(<?=base_url();?><?=$logo->value?>) no-repeat;height: 50px;background-size: contain;background-position: center;"></span></a>
</div>

<div class="TsH_logo_image mobile"  style="display:none;" >
<a target="_self" href="<?=base_url();?>"><span class="lg_logPopup_Logo" style="display: block;background: url(<?=base_url();?><?=$logo->value?>) no-repeat;height: 50px;background-size: contain;background-position: center;"></span></a>
</div>
<div class="TsH_searchBar">
<div class="TsH_searchList">
<div class="input-group TsH_searchView" style="display:none">
<input type="hidden" ng-model="examSearchTextHead" ng-keyup="examAjaxSearchHead(examSearchTextHead, $event)" class="form-control TsH_examCourseBox" placeholder="e.g. BITSAT, Bank, MBA" ng-value="currentSearchTitle" />
<div class="input-group-btn">
<button class="btn btn-default" type="button" ng-click="examAjaxSearchHead(examSearchTextHead, $event)">
<i class="glyphicon glyphicon-search"></i>
</button>
</div>
</div>
<div class="TsH_examSearch_list" ng-show="examSearchStatusHead">
<ul class="TsH_Exam_nameList" ng-if="searchExamResultHead.length == 0">
<li class="TsH_Exam_name_li"><a>No Result Found</a></li>
</ul>
<ul class="TsH_Exam_nameList list_full" ng-if="searchExamResultHead.length > 0">
<li ng-repeat="x in searchExamResultHead" title="{{x.name}}" class="TsH_Exam_name_li" data-value="{{x.name}}"><a target="_self" ng-href="{{x.url}}" ng-bind="x.name"></a></li>
</ul>
</div>
</div>
</div>

<div class="TsH_rightMenu_nav mobile" style="display:none; float:right; margin-left:; margin-top:-53px" >

<ul class="nav navbar-nav navbar-right TsH_rgtMenu_link" >

<?php if(isset($loginId) && $loginId>0){ ?>

<li class=" TsH_text_seprator" ><a target="_self" href="<?=base_url();?>user/dashboard" ><span class="TsH_signUp">Dashboard </span></a>

<a target="_self" href="<?=base_url();?>User/logout" ><span class="TsH_signUp">Logout </span></a> 

<?php } else { ?>
<li class=" TsH_text_seprator" ><a target="_self" href="<?=base_url();?>login" ><span class="TsH_signUp">Login </span></a>

<a target="_self" href="<?=base_url();?>signup" ><span class="TsH_signUp">Signup </span></a>

<?php } ?>
</ul>
</div>


<div class="TsH_rightMenu_nav desktop">
<ul class="nav navbar-nav navbar-right TsH_rgtMenu_link">


<li class=" TsH_text_seprator"><a target="_self" href="<?=base_url();?>exams/mainExam" ><span class="TsH_signUp">Exams </span></a>


<?php if(isset($loginId) && $loginId>0){ ?><li class="TsH_moreEx"><a><?php echo $this->session->userdata('data')['data']->name; ?> <i class="fa fa-angle-down"></i></a>
<ul class="TsH_More_examMmenu">

<li><a target="_self" href="<?=base_url();?>user/dashboard">User Account</a></li>

<li><a target="_self" href="<?=base_url();?>User/logout">Logout</a></li>

</ul>
</li>
<?php } else { ?>


<li class=" TsH_text_seprator"><a target="_self" href="<?=base_url();?>login" ><span class="TsH_signUp">Login </span></a>
 <a target="_self" href="<?=base_url();?>signup"> <span class="TsH_signUp">Signup</span></a></li>
<?php } ?>

<li class="tS_Phone_modal"><a id="phoneMenu" href="tel:+91-6393216806"><i class="fa fa-phone" aria-hidden="true"></i> +91
 <?php echo $system_phone->value; ?></a></li>

</ul>
</div>
</div>
</nav>
</div>



<div id="fb_go_signup_base" class="test_series_modal">
<div class="test_series_modal-content">
<div class="test_series_modal-header">
</div>

</div>
</div>

</div>

</header>
