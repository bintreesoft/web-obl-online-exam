
<content>


<div class="New_homePage">







<div class="Nhm_popularExam ng-scope" >
<div class="container">
<div class="row">

<div class="col-xs-12 col-sm-12 col-md-12">

<div class="Nhm-popularDiv ng-scope ">
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>">
<span class="countIcon nobs_icon-1"></span>
<p  class="ng-binding">Home</p>
</a>
</div>


<div class="Nhm-popularDiv ng-scope " >
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/dashboard">
<span class="countIcon nobs_icon-4"></span>
<p  class="ng-binding">All Exams</p>
</a>
</div>


<div class="Nhm-popularDiv ng-scope " >
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/myExams">
<span class="countIcon nobs_icon-2"></span>
<p  class="ng-binding">My Exams</p>
</a>
</div>




<div class="Nhm-popularDiv ng-scope "  style="border:solid 1px green;">
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/profile" >
<span class="countIcon nobs_icon-3"></span>
<p  class="ng-binding">My Profile</p>
</a>
</div>




<div class="Nhm-popularDiv ng-scope">
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/logout">
<span class="countIcon nobs_icon-1"></span>
<p  class="ng-binding">Logout</p>
</a>
</div>




<style>
th.text-center.update-result {
	font-size: 25px;
	color: #0c7cd5;
}
.result_history table, td, th {
    border: 1px solid #c1c1c1;
    font-weight: 500;
    text-align: justify;
}

.result_history table {
    border-collapse: collapse;
}
.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
}
table {
    background-color: transparent;
}
table {
    border-spacing: 0;
    border-collapse: collapse;
}
* {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
table {
    display: table;
    border-collapse: separate;
    white-space: normal;
    line-height: normal;
    font-weight: normal;
    font-size: medium;
    font-style: normal;
    color: -internal-quirk-inherit;
    text-align: start;
    border-spacing: 2px;
    border-color: grey;
    font-variant: normal;
}
body {
    font-family: 'Source Sans Pro', sans-serif;
    font-weight: normal;
    color: rgba(0,0,0,0.6);
    font-size: 16px;
}
</style>
<script>
function validation(){
	var password = $('#password').val();
		var confirm_password = $('#confirm_password').val();
		if(password!=confirm_password){
			//alert('P');
			$('#pError').show();
			return false;
		} else {
			$('#pError').hide();
		}

}
</script>
<div class="col-xs-12 col-sm-12 col-md-12">
<h3 class="Nhm_heading-1 Nhm_heading-2">My Exams </h3>
</div>
<span class="ts_lg_text" style="color:red;"><?php

 //print_r($this->session->userdata('data')['msg']);

echo $msg;

//$this->session->unset_userdata('data')['msg'];
 ?></span>
<div class="result_history col-md-12">
        <div class="table-responsive">
        <form class="ts_lgForm" name="sign_up_user" onSubmit="return validation()" action="<?=base_url();?>User/updateProfile" method="POST">
<div class="form-group has-error col-md-6">
<label for="male">Name</label>
<input type="text" pattern="[A-Za-z. ]{1,80}" name="full_name"  value="<?= $userDetails->name ?>" autocomplete="full_name" class="form-control " placeholder="Full Name*" required>

</div>
<div class="form-group has-error col-md-6">
<label for="male">Email</label>

<input type="email" name="email"  autocomplete="email"  value="<?= $userDetails->email ?>" class="form-control" placeholder="Email address*" >

</div>
<div class="form-group has-error col-md-6">
<label for="male">Phone</label>

<input type="text" pattern="[0-9]{4,13}" name="phone_no"  value="<?= $userDetails->mobile ?>" autocomplete="phone_no" class="form-control" placeholder="Phone Number*" required>

</div>

<div class="form-group has-error  col-md-12">
<label for="male">Please Fill Password If you want to change.</label>
<span style="color:red; display:none;" id="pError">Your Password And Confirm Password not Match</span>

</div>
<div class="form-group has-error  col-md-6">
<label for="male">Password</label>

<input type="password" pattern=".{6,}" value="" name="password" autocomplete="password" id="password" class="form-control" placeholder="Password*" >

<!-- <span><i class="fa fa-eye"></i></span> -->
</div>
<div class="form-group has-error  col-md-6">
<label for="male">Confirm Password</label>

<input type="password" pattern=".{6,}" name="confirm_password" id="confirm_password" autocomplete="confirm_password" class="form-control" placeholder="Confirm Password*" >
</div>
<button type="submit" class="btn ts_log_btn">Update Profile</button>

</form>
        </div>
      </div>



</div>
</div>



</div>
</div>



</div>
</content>
