
<content>


<div class="New_homePage" >







<div class="Nhm_popularExam ng-scope" >
<div class="container">
<div class="row">

<div class="col-xs-12 col-sm-12 col-md-12">

<div class="Nhm-popularDiv ng-scope ">
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>">
<span class="countIcon nobs_icon-1"></span>
<p  class="ng-binding">Home</p>
</a>
</div>


<div class="Nhm-popularDiv ng-scope " >
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/dashboard">
<span class="countIcon nobs_icon-4"></span>
<p  class="ng-binding">All Exams</p>
</a>
</div>


<div class="Nhm-popularDiv ng-scope " style="border:solid 1px green;" >
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/myExams">
<span class="countIcon nobs_icon-2"></span>
<p  class="ng-binding">My Exams</p>
</a>
</div>




<div class="Nhm-popularDiv ng-scope">
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/profile">
<span class="countIcon nobs_icon-3"></span>
<p  class="ng-binding">My Profile</p>
</a>
</div>




<div class="Nhm-popularDiv ng-scope">
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/logout">
<span class="countIcon nobs_icon-1"></span>
<p  class="ng-binding">Logout</p>
</a>
</div>




<style>
th.text-center.update-result {
	font-size: 25px;
	color: #0c7cd5;
}
.result_history table, td, th {
    border: 1px solid #c1c1c1;
    font-weight: 500;
    text-align: justify;
}

.result_history table {
    border-collapse: collapse;
}
.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
}
table {
    background-color: transparent;
}
table {
    border-spacing: 0;
    border-collapse: collapse;
}
* {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
table {
    display: table;
    border-collapse: separate;
    white-space: normal;
    line-height: normal;
    font-weight: normal;
    font-size: medium;
    font-style: normal;
    color: -internal-quirk-inherit;
    text-align: start;
    border-spacing: 2px;
    border-color: grey;
    font-variant: normal;
}
body {
    font-family: 'Source Sans Pro', sans-serif;
    font-weight: normal;
    color: rgba(0,0,0,0.6);
    font-size: 16px;
}
</style>

<div class="col-xs-12 col-sm-12 col-md-12">
<h3 class="Nhm_heading-1 Nhm_heading-2">My Exams </h3>
</div>
<div class="result_history col-md-12">
        <div class="table-responsive">
          <table class="table">
            <thead>
             
              
                 <br>
                  <div class="clearfix"></div>
              
              <tr>
              <th>Sr.</th>
                <th>Exam</th>
                <th>Negative Marking</th>
                <th>Total Questions</th>
                <th>Cut Off</th>
                <th>Attempted</th>
                <th>Right Answers</th>
                <th>Wrong Answers</th>
                <th>Marks obtained</th>
                <!--   <th>Wrong Ans.</th>-->
                
                <th>Result</th>
                <th>Status</th>
                <th>See History</th>
               
              </tr>
            </thead>
            <tbody>
            <?php if(count($exams)>0){ 
               $counter=0;
              // print_r($exams);
               foreach($exams as $val){ 
                  $counter++;
               ?>
               <tr>
                  <td><?= $counter ?></td>
                  <td><?= $val->name ?></td>
                  <td><?= $val->negative ?>  % / Question </td>
                  <td><?= $val->question ?></td>
                  <td><?= $val->cut_off ?></td>
                  
                  <td><?= $val->attempted ?></td>
                  <td><?= $val->right_ans ?></td>
                  <td><?= $val->wrong_ans ?></td>
                  <td><?= $val->mark_obtained ?></td>
                  <td><?= $val->result ?></td>
                  <td><?php if($val->result>=$val->cut_off){echo "Pass";}else{echo "Fail";}?></td>
                  <td><a href="<?=base_url();?>user/history/<?= $val->exam_id ?>"><i class="fa fa-book"></i></a></td>
                 
                
               </tr>
               <?php } } else { ?>
                  <tr>
                  <td rowspan=2> <center>No Record Found</center></td>
                  
                
               </tr>
                  <?php } ?>
            
            </tbody>
          </table>
        </div>
      </div>



</div>
</div>



</div>
</div>



</div>
</content>
