<html ng-app="">

<head>
<meta content="width=device-width, initial-scale=1" name="viewport" />

    <link rel="icon" href="<?=base_url();?>static/media/wl_client_images/381acc4073d8485ebd52962572d8cff6.png">
    <link rel="stylesheet" href="<?=base_url();?>static/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>static/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="<?=base_url();?>static/test_application.css?v=6fcf035db1b5140b822bbecc52aa4546">
    <link rel="stylesheet" href="<?=base_url();?>static/calculator.css?v=7f725a3e50cd44d571ec27c8aaa3b3d4">
    <link rel="stylesheet" href="<?=base_url();?>/static/css/select2.min.css?v=d44571114a90b9226cd654d3c7d9442c">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <script type="text/javascript" async="" src="<?=base_url();?>/static/analytics.js"></script>
    <script src="<?=base_url();?>/static/jquery.min.js"></script>
    <script src="<?=base_url();?>/static/bootstrap.min.js"></script>
    <script src="<?=base_url();?>/static/js/shuffle.js?v=8c75ff457514c0852d39e1391c8756d3"></script>
    <script src="<?=base_url();?>/static/js/test_application.js?v=cf91c70b9981e593e9391577a6084730"></script>
    <script src="<?=base_url();?>/static/js/calculator.js?v=b095204e92180705f20973e358899b4a"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<style>
@media only screen and (max-width: 600px) {
    .question_nob_list {
    padding-left: 0;
    list-style-type: none;
    overflow-y: scroll;
    position: initial;
    height: calc(100vh - 450px);
}

#main {
    transition: margin-left .5s;
    margin-right: 0px;
    display: block;
}

.NwQs_stickyDiv {
    position: initial;
    bottom: 0;
    width: -webkit-calc(100% - 300px);
    width: -moz-calc(100% - 300px);
    width: 100%;
    padding: 15px 30px;
    border-top: 1px solid #bec4dc;
}

.sidenav {
    height: calc(100% - 82px);
    position: initial;
    z-index: 9999;
    right: 0;
    background-color: #eff2fb;
    overflow-x: initial;
    transition: .5s;
    bottom: 0;
    width: 100%;
}

.question_nob_list {
    padding-left: 0;
    list-style-type: none;
    overflow-y: scroll;
    position: initial;
    height: calc(100vh - 450px);
}

.NwQs_report_submit {
    padding: 2px 5px 5px;
    position: initial;
    text-align: center;
    bottom: 10px;
    width: 100%;
    box-shadow: 2px -4px 5px #b5dcef85;
    margin: 30px -8px 0 -11px;
}
.NwQs_show_Ques {
    padding: 43px 6px 44px 16px;
    position: relative;
    z-index: 999;
    overflow-y: auto;
    height: -moz-calc(100vh - (82px + 194px));
    height: -webkit-calc(100vh - (82px + 194px));
}
}
</style>
</head>

<body >
  
    <div class="NwQs_ExamTopic NwQs_ExTiming NwQs_exam_hedaer" style="background-color:#5959ff !important;">
        <div class="NwQs_ExamNameTab" style="width:40%;">
            <a class="NwQs_logo_image" href="<?=base_url()?>"> <img src="<?=base_url();?><?=$logo->value?>" class="img-responsive" alt="logo"></a>
            <span class="NwQs_QuesPaperName NwQs_marking "><?= $resultExam->name ?></span>
        </div>

        
 <script type="text/javascript" src="<?=base_url();?>static/jquery-timer.js"></script>
  <script type="text/javascript" src="<?=base_url();?>static/jquery.countdownTimer.js"></script>
                            <script>
                                $(function(){
                                    $('#given_date').countdowntimer({
                                        startDate : "<?php echo $cur_time; ?>",
                                        dateAndTime : "<?php echo $end_date; ?>",
                                        size : "lg",
										regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
										regexpReplaceWith: " $2:$3:$4",

                                       
                                    });
                                });
                            </script>
        <p class="NwQs_marking_time " style="">Time Left: <span id="given_date"></span></p>
       
    </div>
    <div class="SectionFirst"  style="">
        
        <div id="main">
            <div class="NwQs_ExamTopic">
                <div class="NwQs_ExamNameTab">
                    <span class="NwQs_sectionText">Sections | </span>
                    <div class="NwQs_ExamName_scroll">
                        <button class="NwQs_Exname active" title="CBT" style="">
                            <span>CBT</span>
                        </button>
                    </div>

                </div>
                <!-- ngIf: totalLang.length > 1 -->
               
            </div>
            <div class="NwQs_ExamTopic NwQs_ExTiming" style="padding:1px;" ng-show="!insideInstruction">
                <div class="NwQs_ExamNameTab">
                    <ul class="ques_paper_breadcrumb" style="padding:0px 23px;">
                        <li>
                            <h4><b>Question <span ><?= $question_no ?></span>:</b></h4></li>
                    </ul>
                </div>
                <div class="NwQs_langS">
                    <p class="NwQs_marking">Marks: <span class="NwQs_posMark " >+1</span> <span class="NwQs_negMark " >-0.333</span></p>
                </div>
            </div>
         
            <div class="container-fluid" ng-show="!insideInstruction">
                <div class="row">
                    <div class="col-sm-12 col-md-12 NwQs_show_Ques">
                        <div id="General_Physics" class="NwQs_Content_Open">
                            <div>
                                <p class="ques_heading_text"><?= $question ?></p>
                                <?php if(isset($image_question)) {?>
                                <img  src="<?= base_url().'admin/'.$image_question ?>" alt="" />
                            <?php } ?>
                                <input type="hidden" value="<?= $question_no ?>" class="question_no" ?>
                                <input type="hidden" value="<?= $exam_id ?>" class="exam_id" ?>

                                <ul class="quest_ans_click " style="">
                                    <li class="">
                                        <label style="padding-top: 10px; padding-bottom: 9px;">
                                            <input type="radio" name="rdoAnswers" <?php if($answer=="a"){ echo "checked";} ?> value="a"  id="ans_c83b1804d45f41ed8a9814abdec98b290">
                                            <label for="ans_c83b1804d45f41ed8a9814abdec98b290"><?= $a ?></label>
                                            <?php if(isset($image_a)) {?>
                                                <img  src="<?= base_url().'admin/'.$image_a ?>" alt="" />
                                            <?php } ?>
                                        </label>
                                    </li>
                                    <li class="">
                                        <label style="padding-top: 10px; padding-bottom: 9px;">
                                            <input type="radio" name="rdoAnswers" <?php if($answer=="b"){ echo "checked";} ?> value="b" id="ans_c83b1804d45f41ed8a9814abdec98b291">
                                            <label for="ans_c83b1804d45f41ed8a9814abdec98b291"><?= $b ?></label>
                                            <?php if(isset($image_b)) {?>
                                                <img  src="<?= base_url().'admin/'.$image_b ?>" alt="" />
                                            <?php } ?>
                                        </label>
                                    </li>
                                    <li class="">
                                        <label style="padding-top: 10px; padding-bottom: 9px;">
                                            <input type="radio" name="rdoAnswers" <?php if($answer=="c"){ echo "checked";} ?> value="c"  id="ans_c83b1804d45f41ed8a9814abdec98b292">
                                            <label for="ans_c83b1804d45f41ed8a9814abdec98b292"><?= $c ?></label>
                                            <?php if(isset($image_c)) {?>
                                                <img  src="<?= base_url().'admin/'.$image_c ?>" alt="" />
                                            <?php } ?>
                                        </label>
                                    </li>
                                    <li class="">
                                        <label style="padding-top: 10px; padding-bottom: 9px;">
                                            <input type="radio" name="rdoAnswers" <?php if($answer=="d"){ echo "checked";} ?> value="d"  id="ans_c83b1804d45f41ed8a9814abdec98b293">
                                            <label for="ans_c83b1804d45f41ed8a9814abdec98b293"><?= $d ?></label>
                                            <?php if(isset($image_d)) {?>
                                                <img  src="<?= base_url().'admin/'.$image_d ?>" alt="" />
                                            <?php } ?>
                                        </label>
                                    </li>
                                    <?php if($e!='' || $image_e!=''){ ?>
                                    <li class="">
                                        <label style="padding-top: 10px; padding-bottom: 9px;">
                                            <input type="radio" name="rdoAnswers" <?php if($answer=="e"){ echo "checked";} ?> value="e"  id="ans_c83b1804dd45f41ed8a9814abdec98b293">
                                            <label for="ans_c83b1804dd45f41ed8a9814abdec98b293"><?= $e ?></label>
                                            <?php if(isset($image_e)) {?>
                                                <img  src="<?= base_url().'admin/'.$image_e ?>" alt="" />
                                            <?php } ?>
                                        </label>
                                    </li>
                                    <?php } ?>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
<div class="NwQs_stickyDiv" ng-show="!insideInstruction">
            <div class="row">
                <div class="stickyDiv_left">
                    <button type="button" class="btn NwQs_ResponseBtn ResponseBtn" ng-click="clear_response(show_single_question.qtmpl)">Clear Response</button>
                </div>
                <div class="stickyDiv_right">
                    <button type="button" class="btn NwQs_ResponseBtn save_next_ques" onclick="return clossdaeNav();"> Save &amp; Next</button>
                </div>
            </div>
        </div>
       
<div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="mobQS_closebtn" onclick="closeNav()"></a>
            <div class="quest_profile_ans">
                <div class="row user_detail">
                    <div class="ques_user_image">
                        <img class="img-responsive" alt="userimage" src="https://image.shutterstock.com/image-vector/profile-placeholder-image-gray-silhouette-600w-1190386324.jpg">
                        <p class="NwQs_marking NwQs_userName " ng-bind="user_data.name">mukesh</p>
                    </div>
                    <ul class="ques_ans_details">
                    <?php 
                    $Attempted=0;
                    $NotAttempted=0;
                    $NotVisit=0;
                        foreach($examRuning as $key=> $val){ 


                            if($val['status']==0){
                                $NotVisit++;
                            } else if($val['status']==1){
                                $NotAttempted++;
                            }else{
                                $Attempted++;
                            }
                          

                         } ?>
                        <li><span class="green " ><?=$Attempted?></span>Attempted</li>
                        <li><span ><?=$NotVisit?></span>Not Visited</li>
                        <li><span class="red " ><?=$NotAttempted?></span>Not Answered</li>
                    </ul>
                </div>
                <div class="section_name_class">
                    <b>Section: </b> <span >CBT</span>
                </div>        
                <div class="quest_nobs_details">
                    <ul class="question_nob_list">
                        <?php 
                       
                        foreach($examRuning as $key=> $val){ 

                            if($val['status']==0){
                                echo '<li onclick="return openQuestion('.$val['counter'].','.$val['exam_id'].')"; style="background: white; border: 1px solid rgba(0, 0, 0, 0.51); color: rgb(0, 0, 0);">'.$val['question_no'].'</li>';
                            } else if($val['status']==1){
                                echo '<li onclick="return openQuestion('.$val['counter'].','.$val['exam_id'].')"; style="background: rgb(218, 84, 80); color: rgb(255, 255, 255); border-radius: 15px;">'.$val['question_no'].'</li>';

                            }else{
                                echo '<li onclick="return openQuestion('.$val['counter'].','.$val['exam_id'].')"; style="background: #3c763d; border: 1px solid rgba(0, 0, 0, 0.51); color: rgb(0, 0, 0);">'.$val['question_no'].'</li>';

                            }
                          

                         } ?>
                       
                    </ul>
                    <div class="NwQs_report_submit">

                    <a href="<?=base_url();?>exam/result" style="display: block"> <button type="button" >SUBMIT TEST</button></a>
                    </div>
                </div>
            </div>
        </div>
        </div>
        
    
    
    
    <script type="text/javascript">
function openQuestion(question_no,exam_id){
  // alert(question_no);
 //  alert(exam_id);
    $.ajax({
                type: 'post',
                url: '<?php echo $this->config->base_url() ?>Ajax/open_question/'+question_no+'/'+exam_id,
                success: function (data) {
                    console.log(data);
                   $('.SectionFirst').empty();
                   $('.SectionFirst').append(data);

                }
             });}
$('.save_next_ques').click(function(){
    var answer = $("input[name='rdoAnswers']:checked").val();

    var question_no = $(".question_no").val();
    var exam_id = $(".exam_id").val();

    $.ajax({
                type: 'post',
                url: '<?php echo $this->config->base_url() ?>Ajax/submit_answer/'+answer+'/'+question_no+'/'+exam_id,
                success: function (data) {
                    console.log(data);
                    $('.SectionFirst').empty();
                    $('.SectionFirst').append(data);

                }
             });
})



$('.ResponseBtn').click(function(){

    $("input[name='rdoAnswers']:checked").prop('checked', false);

})




        function closeNav() {
            if (document.getElementById("mySidenav").style.width != '0px') {
                $("#main").animate({
                    'margin-right': '0px'
                }, {
                    queue: false
                });
                document.getElementById("mySidenav").style.width = "0";
                document.getElementsByClassName("mobQS_closebtn")[0].style.backgroundImage = "url('<?=base_url();?>/static/images/slide-arrow1.png')";
                document.getElementsByClassName("mobQS_closebtn")[0].style.right = "-14px";
                document.getElementsByClassName("quest_profile_ans")[0].style.display = "none";
                document.getElementsByClassName("ques_sumbit_input")[0].style.display = "none";
                $(".NwQs_stickyDiv").animate({
                    width: '100%'
                });
            } else {
                $("#main").animate({
                    'margin-right': '300px'
                }, {
                    queue: false
                });
                document.getElementById("mySidenav").style.width = "300px";
                document.getElementsByClassName("mobQS_closebtn")[0].style.backgroundImage = "url('<?=base_url();?>/static/images/slide-arrow2.png')";
                document.getElementsByClassName("mobQS_closebtn")[0].style.right = "285px";
                document.getElementsByClassName("quest_profile_ans")[0].style.display = "block";
                document.getElementsByClassName("ques_sumbit_input")[0].style.display = "block";
                $(".NwQs_stickyDiv").animate({
                    width: '-=300px'
                }, {
                    queue: false
                });
            }
        }
       /* jQuery(document).ready(function(b) {
            b(document).on("contextmenu", function(a) {
                a.preventDefault()
            });
            b(window).on("keydown", function(a) {
                if (123 == a.keyCode || a.ctrlKey && a.shiftKey && 73 == a.keyCode || a.ctrlKey && 73 == a.keyCode || a.ctrlKey && 85 == a.keyCode) return !1
            })
        }); */
        jQuery(document).ready(function($) {
            $("#optional_sec_error_id").show();
            $(".left-NwQs_paddle").click(function() {
                var slideAmount = $(".NwQs_ExamName_scroll")[0].children[0].clientWidth;
                $(".NwQs_ExamName_scroll").animate({
                    scrollLeft: ($(".NwQs_ExamName_scroll").scrollLeft() - slideAmount) + "px"
                });
            });
            $(".right-NwQs_paddle").click(function() {
                var slideAmount = $(".NwQs_ExamName_scroll")[0].children[0].clientWidth;
                $(".NwQs_ExamName_scroll").animate({
                    scrollLeft: ($(".NwQs_ExamName_scroll").scrollLeft() + slideAmount) + "px"
                });
            });
            $(".NwQs_ExamName_scroll").scroll(function() {
                var element = $(".NwQs_ExamName_scroll");
                if (element.scrollLeft() > 0) {
                    $(".left-NwQs_paddle").show();
                } else {
                    $(".left-NwQs_paddle").hide();
                }
                if (element[0].clientWidth + element.scrollLeft() < element[0].scrollWidth) {
                    $(".right-NwQs_paddle").show();
                } else {
                    $(".right-NwQs_paddle").hide();
                }
            });
            // Calculator
            $(".calculator-icon").click(function() {
                $('#loadCalc').toggle();
            });
            $('#closeButton').click(function() {
                $('#loadCalc').hide();
                $('#keyPad_UserInput').val(0);
                $('#keyPad_UserInput1').val('');
            });
            $("#loadCalc").draggable({
                handle: "#helptopDiv"
            });
            $('#scientificText').show();
            $('#closeButton').click(function() {
                $('#loadCalc').hide();
            });
            $('#keyPad_Help').on('click', function() {
                $(this).hide();
                $('#keyPad_Helpback').show();
                $('.text_container').hide();
                $('.left_sec').hide();
                $('#keyPad_UserInput1').hide();
                $('#helpContent').show();
                $('#English').show();
            });
            $('#keyPad_Helpback').on('click', function() {
                $(this).hide();
                $('#keyPad_Help').show();
                $('.text_container').show();
                $('.left_sec').show();
                $('#keyPad_UserInput1').show();
                $('#helpContent').hide();
                $('#English').hide();
                $('#Japanese').hide();
            });
        });
      
    </script>

</body>

</html>