
<content>


<div class="slct_exam_Page" >

<!-- breadcrumb start -->
<div class="TSeries_Breadcrumb">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<ul class="TS_breadcrumb_div">
<li><a target="_self" href="<?=base_url();?>">Home</a></li>
<li><a target="_self" href="#"><?= $category->name; ?></a></li>
</ul>
</div>
</div>
</div>
</div>
<!-- breadcrumb end -->

<div class="TS_head_bg" style="background:url('https://testseries.edugorilla.com/static/images/L0_Images/521.png') 100% 100%">
<div class="container">
<div class="row exam_course_details">
<h1 class="exam_course_heading"><?= $category->name; ?></h1>

</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 Ts_freeTest_link">
<button class="btn btn-primary center-block test_series_signup_btn" ng-click="goToUnlock('TS_exam_testSeries')">Start free test</button>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="getTs_off"></div>
</div>
</div>
<div class="row exam_course_pattern_start" ng-style="{'min-height':main_min_height}" style="min-height: 143px;">
<div class="tspattern_desktop_view">
<div class="col-xs-12 col-sm-4 col-md-3" id="sidebarWrap">
<div class="snaks_sidebar_fixed" id="sidebar">
<div class="quest_paper_tab">
<a class="quest_topictablinks ng-binding ng-hide" ></a>
<a class="quest_topictablinks" >Test Series <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
<ul class="TStopic_sub_links">
<!-- ngRepeat: x in all_test.L3 --><li title="Mock Tests (Prelims)" class="ng-scope active" style=""><a  class="ng-binding"><?= $category->name; ?></a></li><!-- end ngRepeat: x in all_test.L3 -->
</ul>
<!-- ngRepeat: leftS in leftSideBar -->
</div>
</div>
</div>
<div class="col-xs-12 col-sm-8 col-md-9 ">
<!-- ngIf: FirstElement.length > 0 -->
<div class="exam_course_test_section">
<div class="getTs_About_div" id="TS_exam_testSeries">
<div class="tests_div ng-hide" style="">
</div>
<!-- ngIf: !ajaxLoading --><div id="physics" class="quest_topictabcontent ng-scope" style="">
<!-- ngIf: zero_L5 -->
<!-- ngRepeat: sub in sub_data.L4 --><div class="tst_exam_MainExam ng-scope"  style="border: unset;">
<div class="tst_SubExam_list animate-show-hide"  style="border-top: unset;">
<!-- ngRepeat: paper in sub.L5 -->

<?php foreach($exams as $value){

  $statusExam =0;
  $statusExam = $this->db->query("SELECT *  FROM `question` WHERE  `exam_id`=$value->exam_id")->num_rows();

    ?>


<div class="quest_paper_nobtabcontent hidden-content quest_paper_UnlockBg">
<div class="exam_course_test_paper">
<div class="exam_course_test_paper_name">
<h3 title="<?= $value->name ?>"  class="ng-binding"><?= $value->name ?>(<?= $value->lang ?>)</h3>
<p><span class=""><?= $value->question ?></span> Question&nbsp;|&nbsp; <span ><?= $value->marks ?></span> Marks&nbsp;|&nbsp;<span ><?= $value->hour ?> Hour :  <?= $value->minutes ?> Min</span></p>
</div>

<div class="exam_course_test_paper_start test_paper_unlock ng-scope" ng-if="paper[4] != 1">
<p class="lang_style">
</p>
<?php 
$ExamId= $value->exam_id;
 $checkExam = $this->db->query("SELECT * FROM `user_exam_start` WHERE  `user_id`= '$userLogin' and `exam_id` = '$ExamId'")->row();

 if($checkExam==''){
   $examStatus = 0;

 }else {
   if($checkExam->end_date < $cur_time || $checkExam->status == 1){
     $examStatus = 1;
   } else {
    $examStatus = 0;    
   }
}
if($statusExam > 0 ){
if($examStatus==0){ ?>

<a href="<?=base_url();?>home/exam/<?= $value->exam_id ?>"><i class="fa fa-start" aria-hidden="true"></i>&nbsp;&nbsp;Start Exam</a>

<?php } else { ?>
<a href="#"><i class="fa fa-start" aria-hidden="true"></i>&nbsp;&nbsp;Already Taken</a>
<?php }
} else {
  ?>
  <a href="#"><i class="fa fa-start" aria-hidden="true"></i>&nbsp;&nbsp;Comming Soon</a>

  <?php
} 
?>
 </div>
</div>
</div><!-- end ngRepeat: paper in sub.L5 -->

<?php }
 ?>

</div>
</div><!-- end ngRepeat: sub in sub_data.L4 -->
<div >
<span class="text-center font-weight-bold"><h4><b>More Tests are coming soon</b></h4></span>
</div>
<!-- ngIf: (sub_data.L4.length > 10) -->
</div><!-- end ngIf: !ajaxLoading -->
</div>
</div>
<!-- ngIf: leftSideBar.length>0 -->
</div>
</div>
</div>
<div id="footer"></div>
</div>





</content>
