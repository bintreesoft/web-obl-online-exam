
<content>


<div class="content">
<div class="TestSeriesLoginPage" >
<div class="container-fluid">

<div class="row">
    <div class="ts_lg_leftPart">
        <div class="ts_lgSide_image">



        </div>
    </div>
<div class="ts_lg_rightPart ts_resize_form">
<div class="tS_center_form">
<div class="tS_Form_Scroll">
<div class="Ts_loginPage">
<div class="SignUp_Section_tst">
<div class="ts_lgText_section">
<span class="ts_lg_heading">Welcome back!</span>
<span class="ts_lg_text">Please login to your account</span>
<span class="ts_lg_text"><?php

 //print_r($this->session->userdata('data')['msg']);

 if(isset($this->session->userdata('data')['msg'])){ echo $this->session->userdata('data')['msg']; }

$this->session->unset_userdata('data')['msg'];
 ?></span>
</div>
<p class="text-danger" ng-if="otp_data.error" ng-bind="otp_data.error"></p>

<form class="ts_lgForm" name="sign_up_user" action="<?=base_url();?>User/login" method="POST">

<div class="form-group">
<input type="email" name="email" value="" autocomplete="email" class="form-control" placeholder="Email address*">
</div>

<div class="form-group ts_lg_seePass">
<input type="password" pattern=".{6,}" name="password"  value="" autocomplete="password" class="form-control" placeholder="Password*" required>
<!-- <span><i class="fa fa-eye"></i></span> -->
</div>

<button type="submit" class="btn ts_log_btn">Login <span class="rotate_icon" > </span></button>
</form>
<span class="ts_lg_text ts_lgAccText">Already have an account? <a target="_self" href="<?=base_url();?>signup"> Sign Up</a></span>
</div>

<div class="ts_lgSepratorText logPage_seprator">
<span class="ts_lgConnect">or connect with</span>
</div>
<div class="ts_lg_eduSign">
<div class="eduSignBtn" onclick="SSOPopup()" >
<span class="ts_lg_eduLogo"></span>
<span class="ts_lg_eduText"></span>
</div>
</div>


<div class="ts_lg_eduSign">
<div class="eduSignBtn g-signin2" data-width="240" data-height="40" data-longtitle="true" data-theme="dark" data-onsuccess="onSignIn" data-onfailure="onFailure" data-scope="profile"></div>
</div>


<div class="ts_lg_eduSign">
<div class="eduSignBtn">
<fb:login-button scope="public_profile,email" onlogin="fbLogin();" data-width="240" data-height="40" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></fb:login-button>
</div>
</div>


</div>
</div>
</div>
</div>
</div>
<div id="testSModal" class="test_series_modal" ng-show="signup_process == 1">
<div class="test_series_modal-content">
<div class="test_series_modal-header" ng-click="signup_process=0">
<span class="test_series_close">&times;</span>
</div>
<div class="test_series_modal-body">
<p>We've sent you a message on your phone, please enter the OTP to verify yourself. Alternatively, check your email!!</p>
<form ng-submit="verify_otp()">
<div class="form-group">
<input type="text" pattern="[0-9]{6}" class="form-control" placeholder="Enter the OTP Please">
<p class="text-danger" ng-bind="wrong_otp"></p>
</div>
<button ng-disabled="!otp" type="submit" class="btn btn-default">Submit
<span ng-hide="otp_ == 0" class="ro">
<span class="rotate_icon">
<i class="fa fa-spinner" aria-hidden="true"></i>
</span>
</span>
</button>
<button ng-disabled="timer > 0 || r_diss == 1" type="button" class="btn btn-default" ng-click="resend_otp()">Resend OTP
<span ng-hide="rr == 0" class="ro">
<span class="rotate_icon">
<i class="fa fa-spinner" aria-hidden="true"></i>
</span>
</span>
<span ng-show="timer > 0">(
<span ng-bind="timer"></span>)</span>
</button>
<b ng-bind="resend_msg"></b>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
<script src="static/js/select2.min5b5f.js?v=26abb17f4b7260ea8c912313e2c80fef"></script>

</content>
