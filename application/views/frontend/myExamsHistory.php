
<content>


<div class="New_homePage" >







<div class="Nhm_popularExam ng-scope" >
<div class="container">
<div class="row">

<div class="col-xs-12 col-sm-12 col-md-12">

<div class="Nhm-popularDiv ng-scope ">
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>">
<span class="countIcon nobs_icon-1"></span>
<p  class="ng-binding">Home</p>
</a>
</div>


<div class="Nhm-popularDiv ng-scope " >
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/dashboard">
<span class="countIcon nobs_icon-4"></span>
<p  class="ng-binding">All Exams</p>
</a>
</div>


<div class="Nhm-popularDiv ng-scope " style="border:solid 1px green;" >
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/myExams">
<span class="countIcon nobs_icon-2"></span>
<p  class="ng-binding">My Exams</p>
</a>
</div>




<div class="Nhm-popularDiv ng-scope">
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/profile">
<span class="countIcon nobs_icon-3"></span>
<p  class="ng-binding">My Profile</p>
</a>
</div>




<div class="Nhm-popularDiv ng-scope">
<a target="_self"  class="Nw_ExmPopular" href="<?=base_url();?>user/logout">
<span class="countIcon nobs_icon-1"></span>
<p  class="ng-binding">Logout</p>
</a>
</div>




<style>
th.text-center.update-result {
	font-size: 25px;
	color: #0c7cd5;
}
.result_history table, td, th {
    border: 1px solid #c1c1c1;
    font-weight: 500;
    text-align: justify;
}

.result_history table {
    border-collapse: collapse;
}
.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
}
table {
    background-color: transparent;
}
table {
    border-spacing: 0;
    border-collapse: collapse;
}
* {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
table {
    display: table;
    border-collapse: separate;
    white-space: normal;
    line-height: normal;
    font-weight: normal;
    font-size: medium;
    font-style: normal;
    color: -internal-quirk-inherit;
    text-align: start;
    border-spacing: 2px;
    border-color: grey;
    font-variant: normal;
}
body {
    font-family: 'Source Sans Pro', sans-serif;
    font-weight: normal;
    color: rgba(0,0,0,0.6);
    font-size: 16px;
}
</style>

<div class="col-xs-12 col-sm-12 col-md-12">
<h3 class="Nhm_heading-1 Nhm_heading-2">My Exams History</h3>
</div>
<div class="result_history col-md-12">
        <div class="table-responsive">
          <table class="table">
            <thead>
             
              
                 <br>
                  <div class="clearfix"></div>
              
              <tr>
              <th>Sr.</th>
        <th>Exam</th>
        <th>Question</th>
        <th>Right Answer</th>
        <th>Your's Answer</th>
        <th>Status</th>
               
              </tr>
            </thead>
            <tbody>
            <?php if(count($result)>0){ 
               $counter=0;
              // print_r($exams);
               foreach($result as $val){ 
                  $counter++;
                    $qId= $val->question_id;
                  $ans = $this->db->query("SELECT * FROM `question` WHERE is_deleted=0 and question_id = $qId")->row();
                   $orig_ans=$val->orig_ans;
                   $st_ans=$val->st_ans;
        //print_r($val->st_ans);
               ?>
               <tr>
                  <td><?= $counter ?></td>
                  <td><?= $val->exam_title ?></td>
                  <td><?= $val->examquestion ?></td>
                  <td><?= $ans->$orig_ans ?>   </td>
                  <td><?= $ans->$st_ans ?></td>
                  <td><?php if($val->orig_ans==$val->st_ans and $val->st_ans!=''){ ?><i class="fa fa-check right-ans" aria-hidden="true"></i> <?php }else if($x_ary['orig_ans']!=$x_ary['st_ans'] and $x_ary['st_ans']!=''){ ?><i class="fa fa-times wron-ans" aria-hidden="true"></i><?php } else { ?>
		      <i class="fa fa-square"></i>
		      <?php } ?></td>
                
                
               </tr>
               <?php } } else { ?>
                  <tr>
                  <td rowspan=2> <center>No Record Found</center></td>
                  
                
               </tr>
                  <?php } ?>
            
            </tbody>
          </table>
        </div>
      </div>



</div>
</div>



</div>
</div>



</div>
</content>
