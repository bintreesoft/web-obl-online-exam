<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

     function __construct() {
         // Call the Model constructor
         parent::__construct();
         // $this->load->helper('string');
         $this->load->model('UiModel');
				 $this->load->model('ProductModel');
         $this->load->model('HelperModel');

         $session =   $this->session->userdata('data');
         if(isset($session['data']) && $session['data']->user_id!=''){
           $userLogin = $session['data']->user_id;
     
         } else {
           $userLogin = "";
     
         }

         if($userLogin==''){
          
          redirect(base_url().'login');
        
        }

         // $this->load->library('common');
         // $this->load->library('upload');
         // $this->load->helper('date');
         // date_default_timezone_set(TIMEZONE);
     }

	public function submit_answer($answer="", $question_no="",$id="")
	{


    $timezone = new DateTimeZone("Asia/Kolkata");
    $date = new DateTime();
    $date->setTimezone($timezone );
     $cur_time= $date->format('Y-m-d G:i:s'); 
    //echo "SELECT * FROM `exam` WHERE exam_id = $id";
    $resultExam = $this->db->query("SELECT * FROM `exam` WHERE exam_id = $id")->row();
    //print_r($resultExam);die;
     $timeem = $resultExam->minutes;
    $end_date =  date('Y-m-d G:i:s',strtotime('+'.$timeem.' minutes',strtotime($cur_time)));

    $examRecord =   $this->session->userdata('examRecord');
    $end_date_exam = $examRecord['end_time'];
  

    if($answer=='undefined'){
   $status = 1;
    } else {
      $status = 2;

    }
    $examRuning =   $this->session->userdata('exam');
    $counter=0;
    $count=0;
    $_page = [];

   // $this->session->unset_userdata('exam');
    //echo $question_no;die;
    foreach($examRuning as $key=>$val){
      

   
        if($examRuning[$key]['question_no']==$question_no){
          
          $_page[] = array(
            'user_id' => $examRuning[$key]['user_id'],
            'exam_id' => $examRuning[$key]['exam_id'],
            'status' => $status,
            'answer' => $answer,
            'question_id' => $examRuning[$key]['question_id'],
            'question' => $examRuning[$key]['question'],
            'a' => $examRuning[$key]['a'],
            'b' => $examRuning[$key]['b'],
            'c' => $examRuning[$key]['c'],
            'd' => $examRuning[$key]['d'],
            'e' => $examRuning[$key]['e'],

            'image_question' => $examRuning[$key]['image_question'],
            'image_a' => $examRuning[$key]['image_a'],
            'image_b' => $examRuning[$key]['image_b'],
            'image_c' => $examRuning[$key]['image_c'],
            'image_d' => $examRuning[$key]['image_d'],
            'image_e' => $examRuning[$key]['image_e'],
            
            'question_no' => $examRuning[$key]['question_no'],
            'counter' => $examRuning[$key]['counter'],
          
          );
        } else {
          $_page[] = array(
            'user_id' => $examRuning[$key]['user_id'],
            'exam_id' => $examRuning[$key]['exam_id'],
            'status' => $examRuning[$key]['status'],
            'answer' => $examRuning[$key]['answer'],
            'question_id' => $examRuning[$key]['question_id'],
            'question' => $examRuning[$key]['question'],
            'a' => $examRuning[$key]['a'],
            'b' => $examRuning[$key]['b'],
            'c' => $examRuning[$key]['c'],
            'd' => $examRuning[$key]['d'],
            'e' => $examRuning[$key]['e'],

            'image_question' => $examRuning[$key]['image_question'],
            'image_a' => $examRuning[$key]['image_a'],
            'image_b' => $examRuning[$key]['image_b'],
            'image_c' => $examRuning[$key]['image_c'],
            'image_d' => $examRuning[$key]['image_d'],
            'image_e' => $examRuning[$key]['image_e'],
            
            'question_no' => $examRuning[$key]['question_no'],
            'counter' => $examRuning[$key]['counter'],
          
          );
        }

      }

      $data['examRuning'] = $examRuning;

      $data['total_question'] = count($examRuning);

      if($end_date_exam < $cur_time){
        redirect(base_url().'exam/result');

        //$this->UiModel->renderViewExam('frontend/submit_exam',$data );

      } else {
        $examArray = json_encode($_page, TRUE);

        //echo $examArray;
        $examRuning = json_decode($examArray, true);
      // /  print_r($examRuning);die;
      $data['examRuning'] = $examRuning;
      
      $this->session->set_userdata('exam', $examRuning);
      }

    

   //  var_dump(count($examRuning));
   //  die;







if(!empty($examRuning[$question_no]['question'])){
$data['question'] = $examRuning[$question_no]['question'];
$data['a'] = $examRuning[$question_no]['a'];
$data['b'] = $examRuning[$question_no]['b'];
$data['c'] = $examRuning[$question_no]['c'];
$data['d'] = $examRuning[$question_no]['d'];
$data['e'] = $examRuning[$question_no]['e'];

$data['image_question'] = $examRuning[$question_no]['image_question'];
$data['image_a'] = $examRuning[$question_no]['image_a'];
$data['image_b'] = $examRuning[$question_no]['image_b'];
$data['image_c'] = $examRuning[$question_no]['image_c'];
$data['image_d'] = $examRuning[$question_no]['image_d'];
$data['image_e'] = $examRuning[$question_no]['image_e'];


$data['total_question'] = count($examRuning);
$data['question_no'] = $examRuning[$question_no]['question_no'];
$data['exam_id'] = $examRuning[$question_no]['exam_id'];
$data['answer'] = $examRuning[$question_no]['answer'];

$data['NotVisit'] = 0;

$data['NotAttempted'] = 0;

$data['Attempted'] = 0;

  $this->UiModel->renderViewExam('frontend/new_question',$data );

} else {


  $this->UiModel->renderViewExam('frontend/submit_exam',$data );

}

	}








  public function open_question($question_no="",$id="")
	{
  
 
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
 $cur_time= $date->format('Y-m-d G:i:s'); 
//echo "SELECT * FROM `exam` WHERE exam_id = $id";
$resultExam = $this->db->query("SELECT * FROM `exam` WHERE exam_id = $id")->row();
//print_r($resultExam);die;
 $timeem = $resultExam->minutes;
$end_date =  date('Y-m-d G:i:s',strtotime('+'.$timeem.' minutes',strtotime($cur_time)));


$examRuning =   $this->session->userdata('exam');

$examRecord =   $this->session->userdata('examRecord');
$end_date_exam = $examRecord['end_time'];
$data['examRuning'] = $examRuning;
$data['total_question'] = count($examRuning);
if($end_date_exam < $cur_time){
//  $this->UiModel->renderViewExam('frontend/submit_exam',$data );
redirect(base_url().'exam/result');
} 


$data['question'] = $examRuning[$question_no]['question'];
$data['a'] = $examRuning[$question_no]['a'];
$data['b'] = $examRuning[$question_no]['b'];
$data['c'] = $examRuning[$question_no]['c'];
$data['d'] = $examRuning[$question_no]['d'];
$data['e'] = $examRuning[$question_no]['e'];

$data['image_question'] = $examRuning[$question_no]['image_question'];
$data['image_a'] = $examRuning[$question_no]['image_a'];
$data['image_b'] = $examRuning[$question_no]['image_b'];
$data['image_c'] = $examRuning[$question_no]['image_c'];
$data['image_d'] = $examRuning[$question_no]['image_d'];
$data['image_e'] = $examRuning[$question_no]['image_e'];

$data['question_no'] = $examRuning[$question_no]['question_no'];
$data['exam_id'] = $examRuning[$question_no]['exam_id'];
$data['answer'] = $examRuning[$question_no]['answer'];

$data['NotVisit'] = 0;

$data['NotAttempted'] = 0;

$data['Attempted'] = 0;
$this->UiModel->renderViewExam('frontend/new_question',$data );

}
}