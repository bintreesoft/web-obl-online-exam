<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exam extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

     function __construct() {
         // Call the Model constructor
         parent::__construct();
         // $this->load->helper('string');
         $this->load->model('UiModel');
				 $this->load->model('ProductModel');
         $this->load->model('HelperModel');

         $session =   $this->session->userdata('data');
         if(isset($session['data']) && $session['data']->user_id!=''){
           $userLogin = $session['data']->user_id;
     
         } else {
           $userLogin = "";
     
         }

         if($userLogin==''){
          
          redirect(base_url().'login');
        
        }
         // $this->load->library('common');
         // $this->load->library('upload');
         // $this->load->helper('date');
         // date_default_timezone_set(TIMEZONE);
     }



     
	public function exam_start_user($id="")
	{
   // $examRunidng =   $this->session->userdata('exam');

   //// print_r($examRunidng);exit;
  error_reporting(0);
//  print_r($userLogin);die();
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$cur_time= $date->format('Y-m-d G:i:s'); 
$data['cur_time'] = $cur_time;
//echo "SELECT * FROM `exam` WHERE exam_id = $id";
$resultExam = $this->db->query("SELECT * FROM `exam` WHERE exam_id = $id")->row();
$data['resultExam'] = $resultExam;
//print_r($resultExam);die;
 $timeem = $resultExam->minutes;
$end_date =  date('Y-m-d G:i:s',strtotime('+'.$timeem.' minutes',strtotime($cur_time)));
$session =   $this->session->userdata('data');
  $userLogin = $session['data']->user_id;
   $checkExam = $this->db->query("SELECT * FROM `user_exam_start` WHERE is_deleted=0 and `user_id`= '$userLogin' and `exam_id` = '$id'")->num_rows();

  
  // echo "sdfd";exit;




  $this->db->where(array('user_id'=>$userLogin,'status'=>0));
  $this->db->where_not_in('exam_id', $id);
  $this->db->delete('user_exam_start');




   if($checkExam==0){
$this->db->insert('user_exam_start' , array('user_id'=>$userLogin,'exam_id'=>$id,'term'=>1,'start_date'=>$cur_time,'end_date'=>$end_date));

  $questions = $this->db->query("SELECT *  FROM `question` WHERE `question`.`is_deleted`='0' and  `exam_id`='$id'")->result();
  $counter=0;
  $count=0;
$UserExam = array(
  'start_time'=>$cur_time,
  'end_time'=>$end_date,
  'user_id'=>$userLogin,
  'exam_id'=>$id
);

$this->session->set_userdata('examRecord', $UserExam);

  $_page = array();

foreach($questions as $val){

$ans = $val->answer;
$count++;

$_page[] = array(
  'user_id' => $userLogin,
  'exam_id' => $id,
  'status' => 0,
  'answer' => "",
  'question_id' => $val->question_id,
  'question' => $val->question,
  'a' => $val->a,
  'b' => $val->b,
  'c' => $val->c,
  'd' => $val->d,
  'e' => $val->e,

  'image_question' => $val->image_question,
  'image_a' => $val->image_a,
  'image_b' => $val->image_b,
  'image_c' => $val->image_c,
  'image_d' => $val->image_d,
  'image_e' => $val->image_e,
  
  'question_no' => $count,
  'counter' => $counter,

);
$counter++;

  
  $this->db->insert('exam_start' , array('user_id'=>$userLogin,'exam_id'=>$id,'status'=>0,'answer'=>'','question_id'=>$val->question_id,'right_ans'=>$val->$ans,'question_no'=>$count));
     

}

//print_r($examRuning);exit;

}else {
  redirect(base_url().'exam/exam_start/'.$id);

}
$examArray = json_encode($_page, TRUE);

//echo $examArray;
$examRuning = json_decode($examArray, true);
$this->session->set_userdata('exam', $examRuning);

$data['examRuning'] = $examRuning;
$data['question'] = $examRuning[0]['question'];
$data['a'] = $examRuning[0]['a'];
$data['b'] = $examRuning[0]['b'];
$data['c'] = $examRuning[0]['c'];
$data['d'] = $examRuning[0]['d'];
$data['e'] = $examRuning[0]['e'];

$data['image_question'] = $examRuning[0]['image_question'];
$data['image_a'] = $examRuning[0]['image_a'];
$data['image_b'] = $examRuning[0]['image_b'];
$data['image_c'] = $examRuning[0]['image_c'];
$data['image_d'] = $examRuning[0]['image_d'];
$data['image_e'] = $examRuning[0]['image_e'];


$data['total_question'] = count($examRuning);
$data['question_no'] = $examRuning[0]['question_no'];
$data['question_id'] = $examRuning[0]['question_id'];

$data['NotVisit'] = 0;

$data['NotAttempted'] =0;

$data['Attempted'] =0;

//print_r($examRuning);die;

/*
die;
$data['question'] = $this->db->query("SELECT *, pi.answer as  question_answer, exam_start.answer as  exam_start_answer FROM `exam_start`
    left join question as pi on exam_start.question_id = pi.question_id

 WHERE pi.`is_deleted`='0' and  pi.`exam_id`='$id' and  `exam_start`.`user_id` = $userLogin  and  `exam_start`.`question_no` = 1")->row();


$data['question_total'] = $this->db->query("SELECT *, pi.answer as  question_answer, exam_start.answer as  exam_start_answer, exam_start.status as  exam_start_status FROM `exam_start`
    left join question as pi on exam_start.question_id = pi.question_id

 WHERE pi.`is_deleted`='0' and  pi.`exam_id`='$id' and  `exam_start`.`user_id` = $userLogin")->result();
//print_r($data['question'] );die;

$user_exam_start = $this->db->query("SELECT *  FROM `user_exam_start` WHERE `user_exam_start`.`user_id`='$userLogin' and  `exam_id`='$id'")->row();
*/
$data['end_date'] = $user_exam_start->end_date;
redirect(base_url().'exam/exam_start/'.$id);

	//	$this->UiModel->renderViewExam('frontend/exam',$data );

	}




	public function exam_start($id="")
	{

  error_reporting(0);
//  print_r($id);die();
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$cur_time= $date->format('Y-m-d G:i:s'); 
$data['cur_time'] = $cur_time;
//echo "SELECT * FROM `exam` WHERE exam_id = $id";
$resultExam = $this->db->query("SELECT * FROM `exam` WHERE exam_id = $id")->row();
$data['resultExam'] = $resultExam;
//print_r($resultExam);die;
 $timeem = $resultExam->minutes;
$end_date =  date('Y-m-d G:i:s',strtotime('+'.$timeem.' minutes',strtotime($cur_time)));
$session =   $this->session->userdata('data');
  $userLogin = $session['data']->user_id;
 $checkExam = $this->db->query("SELECT * FROM `user_exam_start` WHERE is_deleted=0 and `user_id`= '$userLogin' and `exam_id` = '$id' and `status`=1")->num_rows();
if($checkExam > 0){
  redirect(base_url().'exam/result');

}

   $examRuning =   $this->session->userdata('exam');
   $examRecord =   $this->session->userdata('examRecord');
   $data['cur_time'] = $cur_time;
   $data['end_date'] = $examRecord['end_time'];


$data['examRuning'] = $examRuning;
$data['question'] = $examRuning[0]['question'];
$data['question_id'] = $examRuning[0]['question_id'];

$data['a'] = $examRuning[0]['a'];
$data['b'] = $examRuning[0]['b'];
$data['c'] = $examRuning[0]['c'];
$data['d'] = $examRuning[0]['d'];
$data['e'] = $examRuning[0]['e'];


$data['image_question'] = $examRuning[0]['image_question'];
$data['image_a'] = $examRuning[0]['image_a'];
$data['image_b'] = $examRuning[0]['image_b'];
$data['image_c'] = $examRuning[0]['image_c'];
$data['image_d'] = $examRuning[0]['image_d'];
$data['image_e'] = $examRuning[0]['image_e'];


$data['total_question'] = count($examRuning);
$data['question_no'] = $examRuning[0]['question_no'];
$data['exam_id'] = $examRuning[0]['exam_id'];
$data['answer'] = $examRuning[0]['answer'];

$data['NotVisit'] = 0;

$data['NotAttempted'] = 0;

$data['Attempted'] = 0;

//print_r($examRuning);die;

/*
die;
$data['question'] = $this->db->query("SELECT *, pi.answer as  question_answer, exam_start.answer as  exam_start_answer FROM `exam_start`
    left join question as pi on exam_start.question_id = pi.question_id

 WHERE pi.`is_deleted`='0' and  pi.`exam_id`='$id' and  `exam_start`.`user_id` = $userLogin  and  `exam_start`.`question_no` = 1")->row();


$data['question_total'] = $this->db->query("SELECT *, pi.answer as  question_answer, exam_start.answer as  exam_start_answer, exam_start.status as  exam_start_status FROM `exam_start`
    left join question as pi on exam_start.question_id = pi.question_id

 WHERE pi.`is_deleted`='0' and  pi.`exam_id`='$id' and  `exam_start`.`user_id` = $userLogin")->result();
//print_r($data['question'] );die;

$user_exam_start = $this->db->query("SELECT *  FROM `user_exam_start` WHERE `user_exam_start`.`user_id`='$userLogin' and  `exam_id`='$id'")->row();
*/
//$data['end_date'] = $user_exam_start->end_date;

		$this->UiModel->renderViewExam('frontend/exam',$data );

	}

	public function result()
	{



    //  print_r($userLogin);die();
    $timezone = new DateTimeZone("Asia/Kolkata" );
    $date = new DateTime();
    $date->setTimezone($timezone );
    $cur_time= $date->format('Y-m-d G:i:s'); 
    $session =   $this->session->userdata('data');
    $userLogin = $session['data']->user_id;
    $examRecord =   $this->session->userdata('examRecord');
    $end_date_exam = $examRecord['end_time']; 
    $user_exam_id = $examRecord['exam_id'];
    $start_date_exam = $examRecord['start_time'];

    $examRuning =   $this->session->userdata('exam');

   $this->UiModel->exam_start($examRecord,$examRuning);



    $updateResult = $this->db->query("select * from `user_exam_start` WHERE  `user_id`='$userLogin'  and `status`='0' and `is_deleted`=0")->result();
    if($updateResult!=NULL){
     // print_r($val->exam_id);die;
      $examId = $user_exam_id;
    
     $done=$this->UiModel->get_Ques_done($user_exam_id,$userLogin);
     $notdone=$this->UiModel->get_Ques_wrong($user_exam_id,$userLogin);
    
     $examDetails=$this->db->query("select * from `exam` WHERE  `exam_id`='$examId'")->row();

     $totalMarks = $done- (($notdone*$examDetails->negative)/100);
     $final=100*$totalMarks/$examDetails->question;
     $marks=round($final,2);

     $solveQuestion = $this->UiModel->get_solve_done($val->exam_id,$userLogin);
     if($examDetails->cut_off <= $marks){ $cutst=1; }
	
     if($examDetails->cut_off > $marks){ $cutst=0; }
      $data['exams'] = $this->db->query("SELECT * FROM `exam` where exam.is_deleted=0 and exam.exam_id = '$examId'")->row();


      $check_record_exam = $this->UiModel->getCheckExamRecod($user_exam_id,$userLogin);
      if($check_record_exam == 0 ) {


      $this->db->insert('exam_record' , array('exam_id'=>$examId,'attempted'=>$solveQuestion,'right_ans'=>$done,
      'result'=>$marks,'user_id'=>$userLogin,
      'cutoff_status'=>$cutst,'wrong_ans'=>$notdone,
      'mark_obtained'=>$totalMarks));

      }

    

    $this->db->where(array('exam_id'=>$examId,'user_id'=>$userLogin));
    $this->db->update('user_exam_start', array('status'=>1,'start_date'=>$start_date_exam,'end_date'=>$end_date_exam));  
  }

  redirect(base_url().'user/myExams');
}

}
