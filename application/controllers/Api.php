<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */



     function __construct() {
         // Call the Model constructor
         parent::__construct();
         // $this->load->helper('string');
         $this->load->model('UiModel');
				 $this->load->model('ProductModel');
         $this->load->model('HelperModel');

        header('Access-Control-Allow-Origin: *');

     }

	public function index()
	{

    $session =   $this->session->userdata('data');


    $data['lat_product'] = $this->ProductModel->get_home_latest_product();



    $data['like_product'] = $this->ProductModel->get_home_liked_product();





    $data['banner'] = $this->UiModel->getBanner();
    $data['post'] = $this->UiModel->getPost();


	$this->UiModel->renderApi($data);



	}


	public function products($catId , $subcatId = NULL , $lowcatId = NULL){




  


    $data['offset'] = $this->input->get('offset');
    if($data['offset']==NULL){
      $data['offset'] = 0;
    }

		$data['catId'] = $catId;
		$data['subcatId'] = $subcatId;

    $data['lowcatId'] = $lowcatId;

		if($subcatId==NULL){
			$data['catData'] = $this->UiModel->getCatLoop($catId);
			$data['title'] = $data['catData']->name;

		}else{
			$data['catData'] = $this->UiModel->getCatLoop($catId);
			$data['subcatData'] = $this->UiModel->get_subcat_on_id($subcatId);
			$data['title']  = $data['subcatData']->name;


		}

    if($lowcatId != NULL){
      $data['lowcatData'] = $this->UiModel->get_lowcat_on_id($lowcatId);
    }



		$data['catFilter'] = $this->UiModel->get_subcat($catId);


		if($lowcatId!=NULL){

      $data['product'] = $this->ProductModel->get_product_on_lowcat($lowcatId);
      $data['productCount'] =0;
  }else if($subcatId==NULL){
    $data['product'] = $this->ProductModel->get_product_on_category($catId);
    $data['productCount'] = $this->ProductModel->get_product_on_category_count($catId);
  }else{
  		$data['product'] = $this->ProductModel->get_product_on_subcat($subcatId);
      $data['productCount'] =0;

    }

		$this->UiModel->renderApi( $data);


	}


	public function cart(){


        if(!$session['status']){
          redirect(base_url().'home/login');

        }
		$this->UiModel->renderView('frontend/cart');


	}




	public function item($itemId){



        if(!$session['status']){
          redirect(base_url().'home/login');

        }
		$data['product'] = $this->ProductModel->get_product($itemId);


		$this->UiModel->renderApi($data);


	}



	public function collection(){




        if(!$session['status']){
          redirect(base_url().'home/login');

        }
		$this->UiModel->renderView('frontend/listing-collection');


	}

	public function account(){


        if(!$session['status']){
          redirect(base_url().'home/login');

        }

		$this->UiModel->renderView('frontend/account');

	}



		public function login(){


      $session =   $this->session->userdata('data');

      $data['message'] = $session['msg'];

			$this->UiModel->renderView('frontend/login' , $data);

		}


			public function register(){


        $session =   $this->session->userdata('data');

        if(isset($session['msg']))
        $data['message'] = $session['msg'];
        else
        $data['message'] ='Registration';



				$this->UiModel->renderView('frontend/register' , $data);

			}


      function product($productId){

        $session =   $this->session->userdata('data');



            // if(!$session['status']){
            //   redirect(base_url()."home/login");
            //
            // }

          $data = $this->ProductModel->get_product($productId);

          $data['attribute'] = $this->db->query("SELECT * ,  GROUP_CONCAT(value) as items FROM attribute where product_id=$productId group by `key`")->result();

        $this->UiModel->renderApi( $data);


      }


      function testmail(){


        $hash = bin2hex(random_bytes(24));



      // echo   $checkHash = serialize($hash);

      // echo uniqid();


        // $ip['user_id'] = 1;
        // $ip['status'] = 0;
        //
        // $hashData = $this->db->get_where('user_verify' , array('hash'=>$checkHash))->row();
        //
        // if($hashData!=null){
        //
        // }
        //
        //
        // $data['company'] = $this->db->get_where('settings' , array('key'=>'system_name'))->row()->value;
        // $data['user'] = 'Krishan Mohan';
        // $data['verify'] =base_url();
        //
        //
        //
        // $this->load->view('template/confirmation' , $data);




        // $this->HelperModel->register();
      }


      public function search(){
        $search = $this->input->get('search');

// $query = "select * from product where name LIKE %$search% OR model  LIKE %$search% OR description  LIKE %$search% ";

$data['title'] = $search;

$product =   $this->db->query("select product.* , pi.url , pi.is_main from product
left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
where product.product_id IN (select product_id from product where name LIKE '%$search%' OR model  LIKE '%$search%' OR description  LIKE '%$search%')")->result();




$category =  $this->db->query("select product.* , pi.url , pi.is_main from product
      left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
             where product.product_id IN (select product_id from product_to_category where cat_id IN (select cat_id from category where name LIKE '%$search%'))")->result();



$subcat =  $this->db->query("select product.* , pi.url , pi.is_main from product
     left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
            where product.product_id IN (select product_id from product_to_subcat where subcat_id IN (select subcat_id from subcat where name LIKE '%$search%'))")->result();



$data['product'] = array_merge($product , $category , $subcat);




$this->UiModel->renderApi( $data);

      }






}
