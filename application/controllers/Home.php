<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

     function __construct() {
         // Call the Model constructor
         parent::__construct();
         // $this->load->helper('string');
         $this->load->model('UiModel');
				 $this->load->model('ProductModel');
         $this->load->model('HelperModel');


         // $this->load->library('common');
         // $this->load->library('upload');
         // $this->load->helper('date');
         // date_default_timezone_set(TIMEZONE);
     }

	public function index()
	{

    $session =   $this->session->userdata('data');


   // $data['lat_product'] = $this->ProductModel->get_home_latest_product();



   // $data['like_product'] = $this->ProductModel->get_home_liked_product();

   $data['total_users'] =  $this->db->query("SELECT *  FROM settings where setting_id=7")->row();
     $data['total_questions'] =  $this->db->query("SELECT *  FROM settings where setting_id=8")->row();
     $data['total_exams'] =  $this->db->query("SELECT *  FROM settings where setting_id=9")->row();
     $data['total_test'] =  $this->db->query("SELECT *  FROM settings where setting_id=10")->row();


   $data['exams'] = $this->db->query("SELECT * FROM `category` WHERE is_deleted=0")->result();


    $data['slider'] = $this->db->query("SELECT * FROM `slider` WHERE status=1")->result();
    $data['posts'] = $this->db->query("SELECT * FROM `posts` WHERE status=1")->result();
    $data['about'] = $this->db->query("SELECT * FROM `about_us` WHERE about_us_id=1")->row();

    $data['post'] = $this->UiModel->getPost();
    $data['model'] = "show";
    //echo "<pre>";
    //print_r($data['pralour']);die;

		$this->UiModel->renderView('frontend/home' , $data);

	}

	public function exams($id="")
	{

    $timezone = new DateTimeZone("Asia/Kolkata" );
    $date = new DateTime();
    $date->setTimezone($timezone );
    $cur_time= $date->format('Y-m-d G:i:s'); 
    $session =   $this->session->userdata('data');
    $userLogin = $session['data']->user_id;

    $session =   $this->session->userdata('data');
    if(isset($session['data']) && $session['data']->user_id!=''){
      $userLogin = $session['data']->user_id;

    } else {
      $userLogin = "";

    }

    if($userLogin==''){
     
     redirect(base_url().'login');
   
   }


    $data['userLogin'] = $userLogin;
    $data['cur_time'] = $cur_time;

   $data['exams'] = $this->db->query("SELECT *, exam.name as name, language.name as lang FROM `exam`,`language` WHERE exam.language=language.language_id and exam.is_deleted=0 and exam.category = $id")->result();

   $data['category'] = $this->db->query("SELECT * FROM `category` WHERE is_deleted=0 and cat_id = $id")->row();
//print_r($data['exams']);die;
		$this->UiModel->renderView('frontend/exams', $data);

	}
  public function exam($id=""){

  $session =   $this->session->userdata('data');
  $userLogin = $session['data']->user_id;


  $checkExam = $this->db->query("SELECT * FROM `user_exam_start` WHERE is_deleted=0 and `user_id`= '$userLogin' and `exam_id` = '$id' and `status`=1")->num_rows();
  if($checkExam > 0){
    redirect(base_url().'exam/result');
  
  }



    $data['exams'] = $this->db->query("SELECT * FROM `exam` 
     where  exam.is_deleted=0 and exam.exam_id = $id")->row();
     $cid = $data['exams']->category;
   $data['categoryy'] = $this->db->query("SELECT * FROM `category` WHERE is_deleted=0 and cat_id = $cid")->row();

//print_r($data['exams']);
if($userLogin!=''){
  $this->UiModel->renderView('frontend/exam-start', $data);

} else {
  redirect(base_url().'login');

}
  }

  public function products(){
   // $data['service'] = $this->db->query("select * from services where status=0")->result();
    $this->UiModel->renderView('frontend/product');

  }
  public function parlour(){
    
    $data['pralour'] = $this->db->query("select *, shop.id as shop_idd, pi_banner.image as background_image , pi.image as user_image from shop
   left join shop_images as pi on shop.id = pi.shop_id and pi.is_main = 1
   
   left join shop_images as pi_banner on shop.id = pi_banner.shop_id and pi_banner.is_main = 0 group by shop.id ")->result();
   // echo "<pre>";
   $data['model_location'] = "show";
    //print_r( $data['pralour']);die;
    $this->UiModel->renderView('frontend/parlour', $data);

  }


  function autocompleteData() {
    $returnData = array();
    
    // Get skills data
    $conditions['searchTerm'] = $this->input->get('term');
    $conditions['conditions']['status'] = '1';
    $skillData = getRows($conditions);
    
    // Generate array
    if(!empty($skillData)){
        foreach ($skillData as $row){
            $data['id'] = $row['id'];
            $data['value'] = $row['skill'];
            array_push($returnData, $data);
        }
    }
    
    // Return results as json encoded array
    echo json_encode($returnData);die;
}

function getRows($params = array()){
  $this->db->select("*");
  $this->db->from($this->dbTbl);
  
  //fetch data by conditions
  if(array_key_exists("conditions",$params)){
      foreach ($params['conditions'] as $key => $value) {
          $this->db->where($key,$value);
      }
  }
  
  //search by terms
  if(!empty($params['searchTerm'])){
      $this->db->like('skill', $params['searchTerm']);
  }
  
  $this->db->order_by('skill', 'asc');
  
  if(array_key_exists("id",$params)){
      $this->db->where('id',$params['id']);
      $query = $this->db->get();
      $result = $query->row_array();
  }else{
      $query = $this->db->get();
      $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
  }

  //return fetched data
  return $result;
}






  public function shopDetails($id=NULL)
	{

    $session =   $this->session->userdata('data');
    $data['sliderr'] = $this->UiModel->getBanner();


    $data['shopDetails'] = $this->db->get_where('shop' , array('id'=>$id))->row();
    $data['shopImages'] = $this->db->get_where('shop_images' , array('shop_id'=>$id, 'is_main'=> 1))->row();
    $data['shopImagesSlider'] = $this->db->get_where('shop_images' , array('shop_id'=>$id, 'is_main'=> 0))->result();
    $data['service'] =   $this->db->query("select services_price_shop_wise.* , services_price_shop_wise.id as service_table_id ,pi.services, pi.service_image from services_price_shop_wise 
    left join services as pi on pi.id = services_price_shop_wise.services_id 
    where services_price_shop_wise.services_id IN (select services_id from services_price_shop_wise ) and services_price_shop_wise.shop_id='$id'")->result();
    

    //$shopImages = $this->db->get_where('services_price_shop_wise' , array('shop_id'=>$id))->result();
 
    $this->UiModel->renderView('frontend/shop-default', $data);
    }
    public function products_old($catId , $subcatId = NULL , $lowcatId = NULL){

    // $session =   $this->session->userdata('data');

    // if(!$session['status']){
    //   redirect(base_url().'home/login');
    //
    // }

    $data['offset'] = $this->input->get('offset');
    if($data['offset']==NULL){
      $data['offset'] = 0;
    }

		$data['catId'] = $catId;
		$data['subcatId'] = $subcatId;

    $data['lowcatId'] = $lowcatId;

		if($subcatId==NULL){
			$data['catData'] = $this->UiModel->getCatLoop($catId);
			$data['title'] = $data['catData']->name;

		}else{
			$data['catData'] = $this->UiModel->getCatLoop($catId);
			$data['subcatData'] = $this->UiModel->get_subcat_on_id($subcatId);
			$data['title']  = $data['subcatData']->name;


		}

    if($lowcatId != NULL){
      $data['lowcatData'] = $this->UiModel->get_lowcat_on_id($lowcatId);
    }



		$data['catFilter'] = $this->UiModel->get_subcat($catId);


		if($lowcatId!=NULL){

      $data['product'] = $this->ProductModel->get_product_on_lowcat($lowcatId);
      $data['productCount'] =0;
  }else if($subcatId==NULL){
    $data['product'] = $this->ProductModel->get_product_on_category($catId);
    $data['productCount'] = $this->ProductModel->get_product_on_category_count($catId);
  }else{
  		$data['product'] = $this->ProductModel->get_product_on_subcat($subcatId);
      $data['productCount'] =0;

    }

		$this->UiModel->renderView('frontend/products' , $data);


	}


	public function cart(){


        if(!$session['status']){
          redirect(base_url().'home/login');

        }
		$this->UiModel->renderView('frontend/cart');


	}




	public function item($itemId){



        if(!$session['status']){
          redirect(base_url().'home/login');

        }
		$data['product'] = $this->ProductModel->get_product($itemId);


		$this->UiModel->renderView('frontend/product' , $data);


	}



	public function collection(){




        if(!$session['status']){
          redirect(base_url().'home/login');

        }
		$this->UiModel->renderView('frontend/listing-collection');


	}

	public function account(){


        if(!$session['status']){
          redirect(base_url().'home/login');

        }

		$this->UiModel->renderView('frontend/account');

	}



		public function login(){


      $session =   $this->session->userdata('data');

      $data['message'] = $session['msg'];

			$this->UiModel->renderView('frontend/login' , $data);

		}


			public function register(){


        $session =   $this->session->userdata('data');

        if(isset($session['msg']))
        $data['message'] = $session['msg'];
        else
        $data['message'] ='Registration';



				$this->UiModel->renderView('frontend/register' , $data);

			}


      function product($productId){

        $session =   $this->session->userdata('data');



            // if(!$session['status']){
            //   redirect(base_url()."home/login");
            //
            // }

          $data = $this->ProductModel->get_product($productId);

          $data['attribute'] = $this->db->query("SELECT * ,  GROUP_CONCAT(value) as items FROM attribute where product_id=$productId group by `key`")->result();

        $this->UiModel->renderView('frontend/product' , $data);


      }


      function testmail(){


        $hash = bin2hex(random_bytes(24));



      // echo   $checkHash = serialize($hash);

      // echo uniqid();


        // $ip['user_id'] = 1;
        // $ip['status'] = 0;
        //
        // $hashData = $this->db->get_where('user_verify' , array('hash'=>$checkHash))->row();
        //
        // if($hashData!=null){
        //
        // }
        //
        //
        // $data['company'] = $this->db->get_where('settings' , array('key'=>'system_name'))->row()->value;
        // $data['user'] = 'Krishan Mohan';
        // $data['verify'] =base_url();
        //
        //
        //
        // $this->load->view('template/confirmation' , $data);




        // $this->HelperModel->register();
      }


      public function search(){
        $search = $this->input->get('search');

// $query = "select * from product where name LIKE %$search% OR model  LIKE %$search% OR description  LIKE %$search% ";

$data['title'] = $search;

$product =   $this->db->query("select product.* , pi.url , pi.is_main from product
left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
where product.product_id IN (select product_id from product where name LIKE '%$search%' OR model  LIKE '%$search%' OR description  LIKE '%$search%')")->result();




$category =  $this->db->query("select product.* , pi.url , pi.is_main from product
      left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
             where product.product_id IN (select product_id from product_to_category where cat_id IN (select cat_id from category where name LIKE '%$search%'))")->result();



$subcat =  $this->db->query("select product.* , pi.url , pi.is_main from product
     left join product_img as pi on pi.product_id = product.product_id and pi.is_main = 1
            where product.product_id IN (select product_id from product_to_subcat where subcat_id IN (select subcat_id from subcat where name LIKE '%$search%'))")->result();



$data['product'] = array_merge($product , $category , $subcat);




$this->UiModel->renderView('frontend/search' , $data);

      }


  public function about(){
    $this->UiModel->renderView('frontend/about');

  }



}
