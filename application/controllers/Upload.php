<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		// $data['script_tag'] = 'table';
	}

        

        public function uploadServiceImage(){

                //
                // $session = $this->session->userdata('user');
                // if (empty($session->admin_id)) {
                //     redirect($this->config->base_url());
                // }
              
                $config['upload_path']          = 'myc-admin/uploads/service';
                $config['allowed_types']        = 'jpg|png|jpeg';
              
                $config['encrypt_name']             = true;
                $config['max_width']            = 0;
                $config['max_height']           = 0;
              
                $this->load->library('upload', $config);        
              
                if ( ! $this->upload->do_upload('file'))
                {
                        $error = array('error' => $this->upload->display_errors());
              
                        // $this->load->view('upload_form', $error);
                        // echo "Failed";
                        // print_r($error);
                        echo json_encode(array('status'=>false,'message'=>$error['error']));
              
              
              
              
                }
                else
                {
                        $this->load->library('image_lib');

                        $data =   array('upload_data' => $this->upload->data());
                        $urll = 'myc-admin/uploads/service/'.$data['upload_data']['file_name'];
                        $target_path = 'myc-admin/suploads/service/';
                        $configer =  array(
                          'image_library'   => 'gd2',
                          'source_image'    =>  $urll,
                          'new_image' => $target_path,
                          'maintain_ratio'  =>  FALSE,
                          'width'           =>  500,
                          'height'          =>  300,
                        );
                        $this->image_lib->clear();
                        $this->image_lib->initialize($configer);
                        $this->image_lib->resize();



              
                        $ip['user_id'] = '1';
                        // $ip['user_id'] = 1;
                        $ip['user_type'] = 'admin';
                        $ip['name'] = $data['upload_data']['orig_name'];
                        $ip['url'] = base_url().$config['upload_path'].'/'.$data['upload_data']['file_name'];
                        $ip['type'] = $data['upload_data']['file_type'];
                        $ip['status'] = 1;
                        $ip['sys_path'] = $config['upload_path'].'/'.$data['upload_data']['file_name'];
              
              
                          //  print_r($ip);
                        $this->db->insert('attachment', $ip);
                        $attachId = $this->db->insert_id();
              
              
                        echo json_encode(array('status'=>true,'message'=>$data['upload_data']['file_name'],'attach'=>$attachId ,'url'=>$ip['url']));
              
              
                }
              
              }
            
        public function uploadShopImage(){

                //
                // $session = $this->session->userdata('user');
                // if (empty($session->admin_id)) {
                //     redirect($this->config->base_url());
                // }
              
                $config['upload_path']          = 'uploads/shops/original/';
                $config['allowed_types']        = 'jpg|png|jpeg';
                        
                $config['encrypt_name']             = true;
                $config['max_width']            = 0;
                $config['max_height']           = 0;
              
                $this->load->library('upload', $config);
                        
                if ( ! $this->upload->do_upload('file'))
                {
                        $error = array('error' => $this->upload->display_errors());
              
                        // $this->load->view('upload_form', $error);
                        // echo "Failed";
                        // print_r($error);
                        echo json_encode(array('status'=>false,'message'=>$error['error']));
              
              
              
              
                }
                else
                {

                        $datadfd = array('upload_data' => $this->upload->data());



                       

                        $config['upload_path']          = 'uploads/shops';
                        $config['allowed_types']        = 'jpg|png|jpeg';
                                
                        $config['encrypt_name']             = true;
                        $config['max_width']            = 0;
                        $config['max_height']           = 0;
                      
                        $this->load->library('upload', $config);


                        $this->load->library('image_lib');

                        $data =   array('upload_data' => $this->upload->data());
                        
                       
                       // print_r($data['upload_data']['orig_name']);die;
                        $urll = 'uploads/shops/original/'.$data['upload_data']['file_name'];
                        $target_path = 'uploads/shops/';
                        $configer =  array(
                          'image_library'   => 'gd2',
                          'source_image'    =>  $urll,
                          'new_image' => $target_path,
                          'maintain_ratio'  =>  FALSE,
                          
                          'width'           =>  1040,
                          'height'          =>  400,
                        );
                        $this->image_lib->clear();
                        $this->image_lib->initialize($configer);
                        $this->image_lib->resize();



                        $dataa =   array('upload_data' => $this->upload->data());
                        $urlld = 'uploads/shops/'.$dataa['upload_data']['file_name'];
                        $target_pathd = 'uploads/shops/profile/';
                        $configerr =  array(
                          'image_library'   => 'gd2',
                          'source_image'    =>  $urlld,
                          'new_image' => $target_pathd,
                          'maintain_ratio'  =>  FALSE,
                          'width'           =>  500,
                          'height'          =>  400,
                        );
                        $this->image_lib->clear();
                        $this->image_lib->initialize($configerr);
                        $this->image_lib->resize();



              
                        $ip['user_id'] = '1';
                        // $ip['user_id'] = 1;
                        $ip['user_type'] = 'admin';
                        $ip['name'] = $data['upload_data']['orig_name'];
                        $ip['url'] = base_url().$config['upload_path'].'/'.$data['upload_data']['file_name'];
                        $ip['profile_url'] = $config['upload_path'].'/profile/'.$data['upload_data']['file_name'];
                        $ip['original_url'] = $config['upload_path'].'/original/'.$data['upload_data']['file_name'];
                        $ip['type'] = $data['upload_data']['file_type'];
                        $ip['status'] = 1;
                        $ip['sys_path'] = $config['upload_path'].'/'.$data['upload_data']['file_name'];
              
              
                          //  print_r($ip);
                        $this->db->insert('attachment', $ip);
                        $attachId = $this->db->insert_id();
              
              
                        echo json_encode(array('status'=>true,'message'=>$data['upload_data']['file_name'],'attach'=>$attachId ,'url'=>$ip['url']));
              
              
                }
              
              }
            
  public function uploadProductImage(){

  //
  // $session = $this->session->userdata('user');
  // if (empty($session->admin_id)) {
  //     redirect($this->config->base_url());
  // }





  $config['upload_path']          = 'uploads/slider/original';
  $config['allowed_types']        = 'jpg|png|jpeg';

  $config['encrypt_name']             = true;
  $config['max_width']            = 0;
  $config['max_height']           = 0;

  $this->load->library('upload', $config);
  $this->load->library('image_lib');
  if ( ! $this->upload->do_upload('file'))
  {
          $error = array('error' => $this->upload->display_errors());

          // $this->load->view('upload_form', $error);
          // echo "Failed";
          // print_r($error);
          echo json_encode(array('status'=>false,'message'=>$error['error']));




  }
  else
  {
        $config['upload_path']          = 'uploads/slider';
        $config['allowed_types']        = 'jpg|png|jpeg';
                
        $config['encrypt_name']             = true;
        $config['max_width']            = 0;
        $config['max_height']           = 0;
      
        $this->load->library('upload', $config);


        $this->load->library('image_lib');

        $data =   array('upload_data' => $this->upload->data());

                       
                       // print_r($data['upload_data']['orig_name']);die;
                       $urll = 'uploads/slider/original/'.$data['upload_data']['file_name'];
                       $target_path = 'uploads/slider/';
                       $configer =  array(
                         'image_library'   => 'gd2',
                         'source_image'    =>  $urll,
                         'new_image' => $target_path,
                         'maintain_ratio'  =>  FALSE,
                         
                         'width'           =>  900,
                         'height'          =>  350,
                       );
                       $this->image_lib->clear();
                       $this->image_lib->initialize($configer);
                       $this->image_lib->resize();

          // $this->load->view('upload_success', $data);

          $ip['user_id'] = '1';
          // $ip['user_id'] = 1;
          $ip['user_type'] = 'admin';
          $ip['name'] = $data['upload_data']['orig_name'];
          $ip['url'] = base_url().$config['upload_path'].'/'.$data['upload_data']['file_name'];
          $ip['original_url'] = base_url().$config['upload_path'].'/original/'.$data['upload_data']['file_name'];
          $ip['type'] = $data['upload_data']['file_type'];
          $ip['status'] = 1;
          $ip['sys_path'] = $config['upload_path'].'/'.$data['upload_data']['file_name'];



          $this->db->insert('attachment', $ip);
          $attachId = $this->db->insert_id();


          echo json_encode(array('status'=>true,'message'=>$data['upload_data']['file_name'],'attach'=>$attachId ,'url'=>$ip['url']));


  }

}

}
