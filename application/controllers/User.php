<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        error_reporting(E_ERROR | E_PARSE);

        $this->load->helper('url');
        $this->load->model('HelperModel');
        $this->load->model('UiModel');


    }


public function changePassword($old,$new){
    
    $id=$this->session->userdata('data')['data']->id;
    //print_r($id);exit;
     $oldPass = $this->db->get_where('shop' , array('id'=>$id,'password'=>$old))->row();
     if(!empty($oldPass)){
          $this->db->where('id' , $id);
             $this->db->update('shop', array('password'=>$new));
             echo "1";
     } else {
         echo "0";
     }
     //redirect('user/dashboard', 'refresh');
}


      public function registration(){
        $email = $_POST['email'];
        
        $isExist = $this->db->get_where('user' , array('email'=>$_POST['email']))->row();
       // print_r($isExist);die;
        if($isExist==null){
          
    $this->db->insert('user' , array('name'=>$_POST['full_name'],'mobile'=>$_POST['phone_no'],'email'=>$_POST['email'],'password'=>$_POST['password']));
        $registrationId =   $this->db->insert_id();
          if($registrationId > 0){
            $op['status'] = false;
             $op['msg'] = 'Registration Successful ';
            $op['data'] = $this->db->get_where('user' , array('user_id'=>$registrationId))->row();
            $op['data']->password = '';
            $this->session->set_userdata('data' , $op);
            //$this->HelperModel->register($registrationId);
            //print_r($_POST);die;
            redirect(base_url().'signup ');

          }else{

            $op['status'] = false;
            $op['msg'] = 'Registration Failed';
            $this->session->set_userdata('data' , $op);


          }

        }else{

                $op['status'] = false;
                $op['msg'] = 'Email Already Exist';
        }

        $this->session->set_userdata('data' , $op);

       // print_r($op);        
        redirect(base_url().'signup');

      }

     
      public function login(){


      if($_POST){
        $isExist = $this->db->get_where('user' , array('email'=>$_POST['email']))->row();
       // print_r($isExist);die;
        if($isExist!=null){


        //  $isCustomer = $this->db->get_where('user' , array('mobile'=>$_POST['email']))->row();



         // print_r($isExist);die;
          if($isExist->password != $_POST['password']){
            $op['status'] = false;
            $op['msg'] = 'Invalid Credentials';

            $this->session->set_userdata('data' , $op);

          


          }else{

         //   if($isExist->status == 1){

              $op['status'] = true;
              $op['msg'] = 'Login Successful';
              $op['data'] = $isExist;
             // $op['user_type'] = "student";

              $this->session->set_userdata('data' , $op);
           
              redirect(base_url().'home' );

         //   }else{

           //   $op['status'] = false;
           //   $op['msg'] = 'Your account is not verified yet!';
           //   $op['data'] = null;

           //   $this->session->set_userdata('data' , $op);


          //    redirect(base_url().'login');

         //   }


          }




        }else{


          $isCustomer = $this->db->get_where('user' , array('email'=>$_POST['email']))->row();
          if($isCustomer!=null){


            //  $isCustomer = $this->db->get_where('user' , array('mobile'=>$_POST['email']))->row();
    
    
    
             // print_r($isExist);die;
              if($isCustomer->password != $_POST['password']){
                $op['status'] = false;
                $op['msg'] = 'Invalid Credentials';
    
                $this->session->set_userdata('data' , $op);
    
              
    
    
              }else{
    
                if($isCustomer->status == 1){
    
                  $op['status'] = true;
                  $op['msg'] = 'Login Successful';
                  $op['data'] = $isCustomer;
                  $op['user_type'] = "customer";
    
                  $this->session->set_userdata('data' , $op);
               
                  redirect(base_url().'user/dashboard' );
    
                }else{
    
                  $op['status'] = false;
                  $op['msg'] = 'Your account is not verified yet!';
                  $op['data'] = null;
    
                  $this->session->set_userdata('data' , $op);
    
    
                  redirect(base_url().'login');
    
                }
    
    
              }
    
    
    
    
            } else {
          $op['status'] = false;
          $op['msg'] = 'Email Doesnot Exist';

          $this->session->set_userdata('data' , $op);
            }

        }

        redirect(base_url().'login');



      } else {
        $this->UiModel->renderView('frontend/login');
      }

    }

    public function dashboard(){
      $session =   $this->session->userdata('data');
         if(isset($session['data']) && $session['data']->user_id!=''){
           $userLogin = $session['data']->user_id;
     
         } else {
           $userLogin = "";
     
         }

         if($userLogin==''){
          
          redirect(base_url().'login');
        
        }
      $session =   $this->session->userdata('data');
      $userLogin = $session['data']->user_id;

     // $data['result'] = $this->db->query("SELECT * FROM `exam_record` WHERE `user_id`='$userLogin' AND `is_deleted`=0 ORDER BY `exam_record_id` DESC")->result();
      $data['exams'] = $this->db->query("SELECT * FROM `category` WHERE is_deleted=0")->result();

        $this->UiModel->renderView('frontend/dashboard', $data);
        
    

    }

    public function exams($id=""){
      $session =   $this->session->userdata('data');
         if(isset($session['data']) && $session['data']->user_id!=''){
           $userLogin = $session['data']->user_id;
     
         } else {
           $userLogin = "";
     
         }

         if($userLogin==''){
          
          redirect(base_url().'login');
        
        }
      $timezone = new DateTimeZone("Asia/Kolkata" );
      $date = new DateTime();
      $date->setTimezone($timezone );
      $cur_time= $date->format('Y-m-d G:i:s'); 
      $session =   $this->session->userdata('data');
      $userLogin = $session['data']->user_id;

     // $data['result'] = $this->db->query("SELECT * FROM `exam_record` WHERE `user_id`='$userLogin' AND `is_deleted`=0 ORDER BY `exam_record_id` DESC")->result();
     $data['exams'] = $this->db->query("SELECT * FROM `exam` WHERE is_deleted=0 and category = $id")->result();

     $data['category'] = $this->db->query("SELECT * FROM `category` WHERE is_deleted=0 and cat_id = $id")->row();
     $data['userLogin'] = $userLogin;
     $data['cur_time'] = $cur_time;

        $this->UiModel->renderView('frontend/all_exams', $data);
        
    

    }
    
    public function myExams(){
      $session =   $this->session->userdata('data');
         if(isset($session['data']) && $session['data']->user_id!=''){
           $userLogin = $session['data']->user_id;
     
         } else {
           $userLogin = "";
     
         }

         if($userLogin==''){
          
          redirect(base_url().'login');
        
        }
      $timezone = new DateTimeZone("Asia/Kolkata" );
      $date = new DateTime();
      $date->setTimezone($timezone );
      $cur_time= $date->format('Y-m-d G:i:s'); 
      $session =   $this->session->userdata('data');
      $userLogin = $session['data']->user_id;

      
      $data['exams'] = $this->db->query("SELECT * FROM `exam_record`
    left join exam as pi on exam_record.exam_id = pi.exam_id

       WHERE `exam_record`.`user_id`='$userLogin' AND `exam_record`.`is_deleted`=0 ORDER BY `exam_record`.`exam_record_id` DESC")->result();
   //  print_r( $data['exams']);
      //$data['exams'] = $this->db->query("SELECT * FROM `exam` WHERE is_deleted=0 and category = $id")->result();

     ///$data['category'] = $this->db->query("SELECT * FROM `category` WHERE is_deleted=0 and cat_id = $id")->row();
     $data['userLogin'] = $userLogin;
     $data['cur_time'] = $cur_time;

        $this->UiModel->renderView('frontend/myExams', $data);
        
    

    }

    public function history($id=""){
      $session =   $this->session->userdata('data');
         if(isset($session['data']) && $session['data']->user_id!=''){
           $userLogin = $session['data']->user_id;
     
         } else {
           $userLogin = "";
     
         }

         if($userLogin==''){
          
          redirect(base_url().'login');
        
        }
      $timezone = new DateTimeZone("Asia/Kolkata" );
      $date = new DateTime();
      $date->setTimezone($timezone );
      $cur_time= $date->format('Y-m-d G:i:s'); 
      $session =   $this->session->userdata('data');
      $userLogin = $session['data']->user_id;

      
      $data['result'] = $this->db->query("SELECT `question`.`question_id`,`exam`.`name` as exam_title ,`question`.`question` as examquestion,
      `question`.`answer` as `orig_ans`,`exam_start`.`answer` as `st_ans`
       FROM `question`,`exam_start`,`exam` 
      WHERE `question`.`question_id`=`exam_start`.`question_id` and 
      `exam`.`exam_id`=`question`.`exam_id` and `question`.`is_deleted`=0
       and `exam_start`.`exam_id`='$id' and `exam_start`.`user_id`='$userLogin'")->result();


    // print_r( $data['result']);
      //$data['exams'] = $this->db->query("SELECT * FROM `exam` WHERE is_deleted=0 and category = $id")->result();

     ///$data['category'] = $this->db->query("SELECT * FROM `category` WHERE is_deleted=0 and cat_id = $id")->row();
     $data['userLogin'] = $userLogin;
     $data['cur_time'] = $cur_time;

        $this->UiModel->renderView('frontend/myExamsHistory', $data);
        
    

    }

    public function profile($method = NULL){
     // $this->session->unset_userdata('data')['msg'];
     $session =   $this->session->userdata('data');
     if(isset($session['data']) && $session['data']->user_id!=''){
       $userLogin = $session['data']->user_id;
 
     } else {
       $userLogin = "";
 
     }

     if($userLogin==''){
      
      redirect(base_url().'login');
    
    }
      $timezone = new DateTimeZone("Asia/Kolkata" );
      $date = new DateTime();
      $date->setTimezone($timezone );
      $cur_time= $date->format('Y-m-d G:i:s'); 
      $session =   $this->session->userdata('data');
      $userLogin = $session['data']->user_id;
      $data['userDetails'] =  $this->db->query("SELECT *  FROM user where user_id=$userLogin")->row();
      $data['msg'] = "";
      $this->UiModel->renderView('frontend/profile', $data);
      
    }

    public function updateProfile($method = NULL){
      $session =   $this->session->userdata('data');
      if(isset($session['data']) && $session['data']->user_id!=''){
        $userLogin = $session['data']->user_id;
  
      } else {
        $userLogin = "";
  
      }

      if($userLogin==''){
       
       redirect(base_url().'login');
     
     }
      $timezone = new DateTimeZone("Asia/Kolkata" );
      $date = new DateTime();
      $date->setTimezone($timezone );
      $cur_time= $date->format('Y-m-d G:i:s'); 
      $session =   $this->session->userdata('data');
      $userLogin = $session['data']->user_id;
      $email = $_POST['email'];
      $password = $_POST['password'];
      $isExist = $this->db->query("SELECT *  FROM user where `user_id` !=$userLogin and email='$email' ")->row();
     // print_r($isExist);die;
      if($isExist==null){
        
    $this->db->where(array('user_id'=>$userLogin));
    $this->db->update('user', array('name'=>$_POST['full_name'],'mobile'=>$_POST['phone_no'],'email'=>$_POST['email']));  
    
    if($password!=''){
        
    $this->db->where(array('user_id'=>$userLogin));
    $this->db->update('user', array('password'=>$_POST['password']));  
    
    }
          $op['status'] = false;
           $op['msg'] = 'Profile Updated Successful ';
       

 

      }else{

              $op['status'] = false;
              $op['msg'] = 'Email Already Exist';
      }

      //$this->session->set_userdata('data' , $op);

      redirect(base_url().'user/profile', $op);
      
    }
    public function authOtp(){

      $mobile = $this->input->post('mobile');
      $otp = $this->input->post('otp');


      $this->db->where('otp' , $otp);
      $this->db->where('mobile' , $mobile);
      $this->db->where('status' , 1);

      $isUserVerified = $this->db->get('otp')->row();


      if($isUserVerified!=null){

        $output['status'] = true;

        $user = $this->db->get_where('user' , array('mobile'=>$mobile,'status'=>1))->row();

        if($user!=null){
          $output['user'] = $user;
          $output['msg'] = "Login Success";
          $output['key'] = 'login';

        }else{


          $this->db->insert('user' , array('mobile'=>$mobile , 'status'=>1));
          $user = $this->db->get_where('user' , array('mobile'=>$mobile,'status'=>1))->row();

          $output['user'] = $user;
          $output['msg'] = "Register Success";
          $output['key'] = 'register';

        }




      }else{

        $output['status'] = false;
        $output['user'] = null;
        $output['msg'] = "Invalid OTP";
        $output['key'] = 'error_otp';

      }


      echo json_encode($output);






    }


    public function updateProfilee(){


      $userId = $this->input->post('user_id');

      unset($_POST['mobile']);
      if($userId!=null){
      $this->db->where('user_id' , $userId);
      $this->db->update('user' , $_POST);

      $user = $this->db->get_where('user' , array('user_id'=>$userId,'status'=>1))->row();


              $output['status'] = true;
              $output['user'] = $user;
              $output['msg'] = "User Updated Successfully";
              $output['key'] = 'user_updated';


      }else{


                $output['status'] = false;
                $output['user'] = null;
                $output['msg'] = "Invalid Request";
                $output['key'] = 'error_user_id';

      }

      echo json_encode($output);





    }


    public function profilee($method = NULL){




          $session =   $this->session->userdata('data');

          if($session['status'] ==false){
            redirect(base_url().'home/login');
          }
          $userId = $session['data']->user_id;


          if($userId==NULL){
            redirect(base_url().'home/login');
          }

      if($method =='update'){


        unset($_POST['email']);

        $this->db->where('user_id' , $userId);
        $this->db->update('user' , $this->input->post());


        $op = $this->session->userdata('data');
        $op['data']->name = $this->input->post('name');
        $op['data']->mobile = $this->input->post('mobile');
        $op['data']->password = $this->input->post('password');

        $this->session->set_userdata('data' , $op);

        redirect(base_url().'home/account');


      }else{
        redirect(base_url().'home/login');

      }

    }



    public function getProfile(){

      $mobile = $this->input->post('mobile');






        $output['status'] = true;

        $user = $this->db->get_where('user' , array('mobile'=>$mobile,'status'=>1))->row();

        if($user!=null){
          $output['user'] = $user;
          $output['msg'] = "Login Success";
          $output['key'] = 'login';

        }else{

          $output['status'] = false;

          $this->db->insert('user' , array('mobile'=>$mobile , 'status'=>1));
          $user = $this->db->get_where('user' , array('mobile'=>$mobile,'status'=>1))->row();

          $output['user'] = null;
          $output['msg'] = "No User Found";
          $output['key'] = 'invalid_user';

        }







      echo json_encode($output);






    }


    public function logout(){


      $op['status'] = false;


      $this->session->set_userdata('data' , $op);


      redirect(base_url().'home');

    }


    public function verify($hash){

    $hashData =   $this->db->get_where('user_verify', array('hash'=>$hash,'status'=>0))->row();

    if($hashData!=null){



      $this->db->where('hash' , $hash);
      $this->db->update('user_verify' , array('status'=>1));
      //
      //
      //
      // $this->db->where('user_id',$hashData->user_id);
      // $this->db->update('user' , array('status'=>1));
      //
      // $op['status'] = true;
      // $op['msg'] = 'Verify Successful';
      // $op['data'] =   $this->db->get_where('user' , array('user_id'=>$hashData->user_id))->row();
      //
      // $this->session->set_userdata('data' , $op);


      redirect(base_url().'home/login');


    }else{

      redirect(base_url().'home');
    }

    }


    public function forgotpass($method=NULL){
      if($method=='gen'){
        $email = $this->input->post('email');
      $userData =   $this->db->get_where('user' , array('email'=>$email))->row();
      if($userData!=null){
        $this->HelperModel->forgotpass($userData->user_id);
        $this->session->set_userdata('msg' , "Link Sent. Please check your email");
        redirect(base_url().'user/forgotpass');


      }else{
        $this->session->set_userdata('msg' , "Email Doesnot exist");

        redirect(base_url().'user/forgotpass');


      }

    }else if($method == 'update'){


    $pass =   $this->input->post('pass');
    $userId = $this->session->userdata('forgot_user');

    $this->db->where('user_id', $userId);
    $this->db->update('user' , array('password'=>$pass));


    $op['status'] = true;
    $op['msg'] = 'Login Successful';
    $op['data'] = $this->db->get_where('user' , array('user_id'=>$userId))->row();

    $this->session->set_userdata('data' , $op);


    redirect(base_url().'home');




    }else{
        $data['message'] = $this->session->userdata('msg');
        $this->UiModel->renderView('frontend/forgotpass' ,$data);

      }
    }



        public function updatepass($link){

          if($link==''){
            redirect(base_url().'home');

          }

// echo $link;

          $hashData = $this->db->get_where('password_reset' , array('hash'=>$link,'status'=>0))->row();

          // print_r($hashData);
          // die();

          if($hashData!=null){

            $this->db->where('verify_id', $hashData->verify_id);
            $this->db->update('password_reset' , array('status'=>1));
            $this->session->set_userdata('forgot_user' , $hashData->user_id);

            $this->UiModel->renderView('frontend/updatepassword' ,$data);


          }else{
            redirect(base_url().'home');

          }

        }




}
