<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exams extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

     function __construct() {
         // Call the Model constructor
         parent::__construct();
         // $this->load->helper('string');
         $this->load->model('UiModel');
				 $this->load->model('ProductModel');
         $this->load->model('HelperModel');

         $session =   $this->session->userdata('data');
         if(isset($session['data']) && $session['data']->user_id!=''){
           $userLogin = $session['data']->user_id;
     
         } else {
           $userLogin = "";
     
         }

         if($userLogin==''){
          
     //     redirect(base_url().'login');
        
        }
         // $this->load->library('common');
         // $this->load->library('upload');
         // $this->load->helper('date');
         // date_default_timezone_set(TIMEZONE);
     }
     public function mainExam()
     {

      $data['result'] = $this->db->query("SELECT *  FROM `category` WHERE `category`.`is_deleted`='0'")->result();

      $this->UiModel->renderView('frontend/category_exams', $data );

     }   

     public function subExam($id="")
     {

      $data['result'] = $this->db->query("SELECT *  FROM `subcat` WHERE `subcat`.`status`='1' and `cat_id`=$id")->result();
      $data['category'] = $this->db->query("SELECT *  FROM `category` WHERE `category`.`status`='1' and `cat_id`=$id")->row();
     // print_r($data['result']);die;

      $this->UiModel->renderView('frontend/subcategory_exams', $data );

     }

     public function exams($id="")
     {

      $data['result'] = $this->db->query("SELECT *  FROM `lowcat` WHERE `lowcat`.`status`='1' and `subcat_id`=$id")->result();
      $data['category'] = $this->db->query("SELECT *  FROM `subcat` WHERE `subcat`.`status`='1' and `subcat_id`=$id")->row();
//print_r($data['result']);die;

      $this->UiModel->renderView('frontend/lowcategory_exams', $data );

     }
   //  left join exam as pi on exam_start.question_id = pi.question_id
  // $data['result'] = $this->db->query("SELECT *, lowcat.name as NameLowcat, count(*) as totalExams  FROM `lowcat` 
   //left join exam as pi on lowcat.lowcat_id = pi.lowcat_id
   //WHERE `lowcat`.`status`='1' and `lowcat`.`lowcat_id`=$id")->result();
  // $data['category'] = $this->db->query("SELECT *  FROM `subcat` WHERE `subcat`.`status`='1' and `subcat_id`=$id")->row();
     public function mockTest($id="")
     {
        
     
    $timezone = new DateTimeZone("Asia/Kolkata" );
    $date = new DateTime();
    $date->setTimezone($timezone );
    $cur_time= $date->format('Y-m-d G:i:s'); 
    $session =   $this->session->userdata('data');
    $userLogin = $session['data']->user_id;

    $session =   $this->session->userdata('data');
    if(isset($session['data']) && $session['data']->user_id!=''){
      $userLogin = $session['data']->user_id;

    } else {
      $userLogin = "";

    }

    if($userLogin==''){
     
     redirect(base_url().'login');
   
   }


    $data['userLogin'] = $userLogin;
    $data['cur_time'] = $cur_time;

   $data['exams'] = $this->db->query("SELECT *, exam.name as name, language.name as lang FROM `exam`,`language` WHERE exam.language=language.language_id and exam.is_deleted=0 and exam.lowcat_id = $id")->result();

   $data['category'] = $this->db->query("SELECT * FROM `lowcat` WHERE  lowcat_id = $id")->row();
//print_r($data['category']);die;
		$this->UiModel->renderView('frontend/exams', $data);

     }

	public function exam_start($id="")
	{

  
//  print_r($userLogin);die();
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$cur_time= $date->format('Y-m-d G:i:s'); 
$data['cur_time'] = $cur_time;
//echo "SELECT * FROM `exam` WHERE exam_id = $id";
$resultExam = $this->db->query("SELECT * FROM `exam` WHERE exam_id = $id")->row();
$data['resultExam'] = $resultExam;
//print_r($resultExam);die;
 $timeem = $resultExam->minutes;
$end_date =  date('Y-m-d G:i:s',strtotime('+'.$timeem.' minutes',strtotime($cur_time)));
$session =   $this->session->userdata('data');
  $userLogin = $session['data']->user_id;
   $checkExam = $this->db->query("SELECT * FROM `user_exam_start` WHERE is_deleted=0 and `user_id`= '$userLogin' and `exam_id` = '$id'")->row();

   if($checkExam==NULL){
  
 $this->db->insert('user_exam_start' , array('user_id'=>$userLogin,'exam_id'=>$id,'term'=>1,'start_date'=>$cur_time,'end_date'=>$end_date));

  $questions = $this->db->query("SELECT *  FROM `question` WHERE `question`.`is_deleted`='0' and  `exam_id`='$id'")->result();
  $counter=0;
foreach($questions as $val){
$counter++;
$ans = $val->answer;

  $this->db->insert('exam_start' , array('user_id'=>$userLogin,'exam_id'=>$id,'status'=>0,'answer'=>'','question_id'=>$val->question_id,'right_ans'=>$val->$ans,'question_no'=>$counter));

}

}else {
    if($checkExam->end_date > $cur_time || $checkExam->status == 1){
      redirect(base_url().'exam/result');
    }
}
$data['question'] = $this->db->query("SELECT *, pi.answer as  question_answer, exam_start.answer as  exam_start_answer FROM `exam_start`
    left join question as pi on exam_start.question_id = pi.question_id

 WHERE pi.`is_deleted`='0' and  pi.`exam_id`='$id' and  `exam_start`.`user_id` = $userLogin  and  `exam_start`.`question_no` = 1")->row();


$data['question_total'] = $this->db->query("SELECT *, pi.answer as  question_answer, exam_start.answer as  exam_start_answer, exam_start.status as  exam_start_status FROM `exam_start`
    left join question as pi on exam_start.question_id = pi.question_id

 WHERE pi.`is_deleted`='0' and  pi.`exam_id`='$id' and  `exam_start`.`user_id` = $userLogin")->result();
//print_r($data['question'] );die;

$user_exam_start = $this->db->query("SELECT *  FROM `user_exam_start` WHERE `user_exam_start`.`user_id`='$userLogin' and  `exam_id`='$id'")->row();
$data['end_date'] = $user_exam_start->end_date;

		$this->UiModel->renderViewExam('frontend/exam',$data );

	}

	public function result()
	{

    //  print_r($userLogin);die();
    $timezone = new DateTimeZone("Asia/Kolkata" );
    $date = new DateTime();
    $date->setTimezone($timezone );
    $cur_time= $date->format('Y-m-d G:i:s'); 
    $session =   $this->session->userdata('data');
    $userLogin = $session['data']->user_id;
    $updateResult = $this->db->query("select * from `user_exam_start` WHERE  `user_id`='$userLogin'  and `status`='0' and `is_deleted`=0")->result();
    if($updateResult!=NULL){
    foreach($updateResult as $val){
     // print_r($val->exam_id);die;
      $examId = $val->exam_id;
    
     $done=$this->UiModel->get_Ques_done($val->exam_id,$userLogin);
     $notdone=$this->UiModel->get_Ques_wrong($val->exam_id,$userLogin);
    
     $examDetails=$this->db->query("select * from `exam` WHERE  `exam_id`='$examId'")->row();

     $totalMarks = $done- (($notdone*$examDetails->negative)/100);
     $final=100*$totalMarks/$examDetails->question;
     $marks=round($final,2);

     $solveQuestion = $this->UiModel->get_solve_done($val->exam_id,$userLogin);
     if($examDetails->cut_off <= $marks){ $cutst=1; }
	
     if($examDetails->cut_off > $marks){ $cutst=0; }
      $data['exams'] = $this->db->query("SELECT * FROM `exam` where exam.is_deleted=0 and exam.exam_id = '$examId'")->row();


      $check_record_exam = $this->UiModel->getCheckExamRecod($val->exam_id,$userLogin);
      if($check_record_exam == 0 ) {


      $this->db->insert('exam_record' , array('exam_id'=>$examId,'attempted'=>$solveQuestion,'right_ans'=>$done,
      'result'=>$marks,'user_id'=>$userLogin,
      'cutoff_status'=>$cutst,'wrong_ans'=>$notdone,
      'mark_obtained'=>$totalMarks));

      }

    }

    $this->db->where(array('exam_id'=>$examId,'user_id'=>$userLogin));
    $this->db->update('user_exam_start', array('status'=>1));  
  }

  redirect(base_url().'user/myExams');
}

}
