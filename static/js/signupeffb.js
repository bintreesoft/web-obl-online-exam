(function() {
	
	TestSeries.controller('signupCtrl', ['$scope', '$http', '$interval', '$window', '$timeout', '$location', function($scope, $http, $interval, $window, $timeout, $location){
		$scope.signup_process = 0;
		$scope.rr = $scope.r_diss = $scope.otp_ = $scope.signup_ = $scope.signupfinal_ = 0;
		$scope.timer = 60;
		$scope.otp_data = {};
		$scope.verified_data = {};
		var phone_no = "";
		$scope.wrong_otp = $scope.resend_msg = "";
		$scope.showCities = $scope.allStates = $scope.allCities = $scope.showCOI = [];
		$scope.hostName = $location.host();

		$scope.load = function(){
			$scope.get_states_cities('states');
		}
		$scope.get_states_cities = function (arg) {
			var req = {method:"GET",url:"/static/json/" + ((arg == 'states') ? "states": "cities") + '.json',headers:{'Content-Type':'application/json'}};
			$http(req).then(function(response) {
				if (arg == "states") {
					$scope.allStates = JSON.parse(JSON.stringify(response.data));
					$scope.get_states_cities('cities');
				}
				else {
					$scope.allCities = JSON.parse(JSON.stringify(response.data));
					$scope.get_COI();	
				}
			},
            function(error) {
                console.log("Something went wrong! Please try again later!");
            });
		}
		var set_timer = function(net_time) {
			var interval_count = $interval(function() {
				if (net_time < 1) {
					$interval.cancel(interval_count);
					$scope.r_diss = 0;
				}
				$scope.timer = net_time;
				net_time --;
			}, 1000);
		}
		$scope.signup = function () {
			$scope.otp_data.error = "";
			$scope.signup_ = 1;
			phone_no = $scope.phone_no;
			var signup_data = {'full_name':$scope.full_name,'phone':$scope.phone_no,'email':$scope.email,'password':$scope.password,'conf_pass':$scope.confirm_password};
			if ($scope.hostName == 'recruitment.edugorilla.com') {
				signup_data.clg_name = $scope.clg_name ? $scope.clg_name : '';
				signup_data.job_profile = $scope.job_profile ? $scope.job_profile : '';
				signup_data.emp_type = $scope.emp_type ? $scope.emp_type : '';
			}
			if ($scope.password == $scope.confirm_password) {
				var req = {method: 'POST',url: "/api/v2/auth/signup/st1",data:signup_data,headers:{'Content-Type':'application/x-www-form-urlencoded'}};
				$http(req).then(function(response) {
					$scope.signup_ = 0;
					if (response.data.status) {
						$scope.otp_data = response.data.result;
						if ($scope.otp_data.data == 1) {
							var req = {method:'POST',url:"/api/v1/auth/login", data:{"username": phone_no,"password": $scope.password}, headers:{'Content-Type': 'application/x-www-form-urlencoded'}};
							$http(req).then(function(response) {
								if (response.data.status) {
									if (location.host == 'recruitment.edugorilla.com') {
										$window.location.href = '/tests/576/round-1';
									}
									else {
										$window.location.href = getUrlArgument("next");
									}
								}
							},
							function(error) {
								console.log("Something went wrong in auto login after signup! Please try again later!");
							});
						}
					}
					else {
						$scope.otp_data.error = response.data.msg;
					}
				},
	            function(error) {
	                console.log("Something went wrong! Please try again later!");
	            });
			}
		}

		$scope.verify_otp = function() {
			$scope.otp_ = 1;
			var req = {method: 'POST',url: "/api/v1/auth/signup/st2",data: {"otp": $scope.otp,"phone": phone_no},headers: {'Content-Type': 'application/x-www-form-urlencoded'}};
			$http(req).then(function(response){
				$scope.otp_ = 0;
				if (response.data.status) {
					if(location.host == 'testseries.edugorilla.com') {
						fbq('track', 'CompleteRegistration');
					}
					// $scope.signup_process = 2;
					var req = {method:'POST',url:"/api/v1/auth/login", data:{"username": phone_no,"password": $scope.password}, headers:{'Content-Type': 'application/x-www-form-urlencoded'}};
					$http(req).then(function(response) {
						if (response.data.status) {
							$window.location.href = getUrlArgument("next");
						}
					},
					function(error) {
						console.log("Something went wrong in auto login after signup! Please try again later!");
					});
				}
				else {
					$scope.wrong_otp = "Invalid OTP!";
				}
			},
            function(error) {
                console.log("Something went wrong! Please try again later!");
            });
		}

		$scope.resend_otp = function () {
			$scope.rr = $scope.r_diss = 1;
			$scope.resend_msg = "";
			var req = {method:'POST',url:"/api/v1/auth/signup/resendotp",data:{"phone":phone_no},headers:{'Content-Type':'application/x-www-form-urlencoded'}};
			$http(req).then(function(response){
				$scope.rr = 0;
				if (response.data.status) {
					$scope.resend_msg = "Success.";
					set_timer(60);
				}
				else {
					$scope.resend_msg = "Something went wrong!";
				}
			},
            function(error) {
                console.log("Something went wrong! Please try again later!");
            });
		}
		// function to decode and get query argument in url based on key, if present
		function getUrlArgument(arg) {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            if (arg in vars) {
                return decodeURIComponent(vars[arg]);
            }
            return "";
        }

		$scope.loadCities = function(state){
			$scope.showCities = [];
			for (var i = 0; i < $scope.allCities.length; i++) {
				if (state.id == $scope.allCities[i].state_id) {
					$scope.showCities.push($scope.allCities[i]);
				}
			}
		}

		$scope.get_COI = function () {
			$http({method:'GET',url:'/api/v1/auth/signup/getL1',headers:{'Content-Type':'application/json'}}).then(function(response){
				if (response.data.status) {
					$scope.showCOI = response.data.result.data;
				}
			},
			function(error) {
				console.log("Something went wrong! Please try again later!");
			});
		}
		// In case if user is in between the non-login payment state, and wants to go-back to signin, and continue with non-login signin cycle
		$scope.createLogin = function () {
            var url = "/login"
            next = getUrlArgument("next");
            if (next != "") {
                url = "/login" + "?next=" + encodeURIComponent(next)
            }
            return url;
		}
	}]);
})(angular);