(function() {
    // In HTML5 mode, the $location service getters and setters interact with the browser URL address through the HTML5 history API.
    TestSeries.config(['$locationProvider',function($locationProvider) {
        $locationProvider.html5Mode(true);
    }]);

    // Global functions can be used in multiple controller
    TestSeries.run(['$rootScope', '$sce', function($rootScope, $sce) {
        // To make HTML content trusted.
        $rootScope.toTrustedHTML = function(html) {
            return $sce.trustAsHtml(html);
        };

        // Convert seconds to minutes
        $rootScope.min_converter = function(sec_) {
            var min = Math.floor(sec_/60);
            var sec = Math.floor(sec_ - (min*60));
            return min + ' Min ' + ((sec>0) ? sec + ' Sec': '');
        };
    }]);

    // Homepage controller
    TestSeries.controller('NewHomePageController', ['$scope', '$rootScope', '$http', '$timeout', function ($scope, $rootScope, $http, $timeout){
        
        // Initialized variables inside '$scope'
        $scope.offerData = {};
        $scope.analyticsData = {};
        $scope.popularExamsData = [];
        $scope.allExamsData = [];
        $scope.videoSliderData = [];
        $scope.featureSlidersData = [];
        $scope.currentL0Data = [];
        $scope.currentL1Data = [];
        $scope.allPacks = [];
        $scope.allVerticalData = {};
        $scope.currentL0Index = 0;
        $scope.showL2 = false;
        var currentL1Index = 0;
        $scope.l0AjaxOngoing = false;
        $scope.l1AjaxOngoing = false;
        $scope.currentL1OngoingIndex = null;

        //############################ EXTERNAL FUNCTIONS ############################

        // Auto invoked after page load
        $scope.initialLoading = function() {
            getOffersData();
            getPopularExamsData();
            getAllExamsData();
            // getVideoSliderData();
            if(location.host == 'testseries.javatpoint.com') {
                getVerticalData();
            }
        };

        // Check if the Object is empty or not
        $scope.isNotEmptyObj = function(obj) {
            return Object.keys(obj).length > 0;
        };

        // Fetch single L0 exam data
        $scope.fetchSingleL0ExamData = function(l0_index_id) {
            $scope.showL2 = false;
            if ($scope.allExamsData[l0_index_id].L1.length > 0) {
                $scope.currentL0Data = $scope.allExamsData[l0_index_id];
            }
            else {
                $scope.l0AjaxOngoing = true;
                let url = $scope.allExamsData[l0_index_id].url;
                // Do Ajax GET request to fetch L1 data of that L0
                $http.get(url).then(
                    function(response) {
                        if (response.status) {
                            $scope.allExamsData[l0_index_id].L1 = response.data.result.data;
                            $scope.currentL0Data = $scope.allExamsData[l0_index_id];
                        }
                        $scope.l0AjaxOngoing = false;
                    },
                    function(error) {
                        console.log(error);
                    }
                );
            }
            $scope.currentL0Index = l0_index_id;
        };

        // Fetch single L1 exam data
        $scope.fetchSingleL1ExamData = function(l1_index_id) {
            $scope.currentL1OngoingIndex = l1_index_id;
            if ($scope.allExamsData[$scope.currentL0Index].L1[l1_index_id].L2.length > 0) {
                $scope.currentL1Data[l1_index_id] = $scope.allExamsData[$scope.currentL0Index].L1[l1_index_id];
                $scope.showL2 = true;
                modifyAtagForIframe();
            }
            else {
                $scope.l1AjaxOngoing = true;
                let url = '/api/v1/ecatalog/L2/'+$scope.allExamsData[$scope.currentL0Index].L1[l1_index_id].id;
                // Do Ajax GET request to fetch L1 data of that L0
                $http.get(url).then(
                    function(response) {
                        if (response.status) {
                            $scope.allExamsData[$scope.currentL0Index].L1[l1_index_id].L2 = response.data.result.data;
                            $scope.currentL1Data[l1_index_id] = $scope.allExamsData[$scope.currentL0Index].L1[l1_index_id];
                            $scope.showL2 = true;
                        }
                        $scope.l1AjaxOngoing = false;
                        modifyAtagForIframe();
                    },
                    function(error) {
                        console.log(error);
                    }
                );
            }
        };

        $scope.generateOfferUrl = function(pack_id, coupon_code) {
            var url = "/cart/exam_package/" + pack_id;
            if(coupon_code) {
                url += "?q=" + btoa(coupon_code)
            }
            return url;
        };

        // Get parsed offer data
        $scope.parseOfferData = function (data, key) {
            var jsonData = JSON.parse(data);
            return jsonData[key];
        };

        //############################################################################


        //############################ INTERNAL FUNCTIONS ############################

        // Get offers data
        function getOffersData(){
            let url = '/api/v2/offers';
            
            // Do Ajax GET request
            $http.get(url).then(
                function(response) {        // Success response
                    if (response.status) {
                        $rootScope.root_user_data = response.data.result.user;
                        $scope.offerData = response.data.result.data;
                        $timeout(function(){
                            if ($scope.offerData.length > 1) {
                                $("#NwBanner-Slider-Offer").lightSlider({
                                    loop:true, keyPress:true, item:1, controls:true, auto:true, speed:1500, pause:5000, pauseOnHover: true, pager: true
                                });
                            }
                            else {
                                $("#NwBanner-Slider-Offer").lightSlider({
                                    loop:false, keyPress:false, item:1, controls: false, auto:false, speed:2000, enableTouch:false, pager: false, enableDrag: false
                                });
                            }
                        });
                        $(".update_loading").fadeOut(1000);
                    }
                },
                function(error) {          // Error response
                    console.log(error);
                }
            );
        }

        // Get popular exams data
        function getPopularExamsData() {
            let url = '/api/v2/live_stats?p=homepage_popular_exams';
            // Do Ajax GET request
            $http.get(url).then(
                function(response) {        // Success response
                    if (response.status) {
                        $scope.popularExamsData = JSON.parse(response.data.result.data.trending_l2s);
                    }
                    modifyAtagForIframe();
                },
                function(error) {           // Error response
                    console.log(error);
                }
            );
        }

        // Get all exams data
        function getAllExamsData() {
            let url = '/api/v1/ecatalog/L0';

            // Do Ajax GET request
            $http.get(url).then(
                function(response) {        // Success response
                    if (response.status) {
                        let data = response.data.result.data;
                        $scope.analyticsData = {
                            'exams': convertToIndianNumberSystem(data.exams_covered),
                            'questions_attempted': convertToIndianNumberSystem(data.questions_attempted),
                            'tests': convertToIndianNumberSystem(data.tests_available),
                            'users': convertToIndianNumberSystem(data.users_month)
                        };
                        $scope.allExamsData = data.L0;
                        $scope.fetchSingleL0ExamData(0);
                    }
                },
                function(error) {          // Error response
                    console.log(error);
                }
            );
        }

        // Convert count into indian number system format
        function convertToIndianNumberSystem(number) {
            if (number > 10000000) {
                number = Math.floor(number/10000000) + ' Crore+'
            }
            else if (number > 100000) {
                number = Math.floor(number/100000) + ' Lakh+'
            }
            else if (number) {
                number = number.toLocaleString('en-IN');
            }
            return number;
        }
        
        // For javaTpoint
        function getVerticalData() {
            $http.get('static/json/homepage_vertical_design/89.json').then(function(response) {
                $scope.allVerticalData = response.data;
            });
        }

        // Set '_top' in a tag when the site is accessed from iframe
        function modifyAtagForIframe() {
            function inIframe() {
                try {
                    return window.self !== window.top;
                } catch (e) {
                    return true;
                }
            }
            setTimeout(function(){
                if (inIframe()) {
                    jQuery("a").attr("target","_top");
                }
            }, 500);
        }
    }]);
})(angular);