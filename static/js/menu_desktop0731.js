var TestSeries = angular.module('TestSeries', ['ngAnimate']);
TestSeries.filter('ceil', function() {
    return function(input) {
        return Math.ceil(input);
    };
});
TestSeries.run(['$rootScope', '$timeout', function($rootScope, $timeout) {
    $rootScope.root_user_data;
    $rootScope.setAlertBoxTimeout = function (title, msg) {
        $rootScope.boughtPackageExpired = true;
        $rootScope.errorTitle = title;
        $rootScope.errorIMsg = msg;
        $timeout(function(){
            $rootScope.boughtPackageExpired = false;
        }, 3000);
    }
    $rootScope.setAlertBoxTimeoutClose = function () {
        $rootScope.boughtPackageExpired = false;
    }
    $rootScope.adjustStudentCount = function (no) {
        if (no > 10000000) {
            no = Math.floor(no/10000000) + ' Crore+'
        }
        else if (no > 10000000) {
            no = Math.floor(no/100000) + ' Lakh+'
        }
        else if (no) {
            no = no.toLocaleString('en-IN') + '+';
        }
        return no;
    }
}]);
(function () {
    jQuery(document).ready(function($) {
        function getCookie(key) {
            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
            return keyValue ? keyValue[2] : null;
        }
        if (getCookie('eg_user') !== null) {
            $(".myaccountbtn").show();
            $(".loginbtn").hide();
            $(".signupbtn").hide();
            $("#signup_big_btn").hide();
            $("#join_now").css("visibility", "hidden");
        }
        else {
            $(".myaccountbtn").hide();
            $(".loginbtn").show();
            $(".signupbtn").show();
            $("#signup_big_btn").show();
            $("#join_now").show();
        }
        $(".p_year").html(new Date().getFullYear());
        $('.eg_footer_menu_heading').click( function() {
            $(this).find('i.fa').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
            $(this).siblings('ul.sub_menu').slideToggle();
        });
        $(".mobQS_mobbars").click(function () {
            $("#mobQ_mobSidenav").css('width', '276px');
            $(".kill_menu_close").css('right', '300px');
            $('.lazyloading').css('filter','blur(5px)');
        });
        $(".mobQS_closebtn").click(function () {
            $("#mobQ_mobSidenav").css('width', '0');
            $(".kill_menu_close").css('right', '-50px');
            $('.lazyloading').css('filter','');
        });
        $("#exams_tab").click(function() {
            $(this).find('i.fa').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
            $(".mob_ts_exam_name").slideToggle();
        });
        $(".l0_name").click(function() {
            $(this).find('i.fa').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
            $(this).siblings('ul.ts_mob_examlist_one').slideToggle();
        });
        // Get the modal
        var modal = ""
        if(document.getElementById('more_examModal')) {
            modal = document.getElementById('more_examModal');
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        }

        // Get the button that opens the modal
        var btn = "";
        if(document.getElementById("more_exam_click")){
        btn = document.getElementById("more_exam_click");
        btn.onclick = function() {
                modal.style.display = "block";
            }
        }

        // Get the <span> element that closes the modal
        var span = "";
        if(document.getElementsByClassName("exam_popupclose")[0]){
            span = document.getElementsByClassName("exam_popupclose")[0];
            span.onclick = function() {
                    modal.style.display = "none";
                }
        }        
        $(".Ts_more_sublist").hide();
        $(".Ts_more_exm_name").show();
        
        $('.Ts_more_exm_name').click(function(){
            $( this ).find(".Ts_more_sublist").slideToggle();
            $( this ).children('a').toggleClass('active');
        });
        $('.TS_PromoMmodal').css('display', 'block');
    });

    TestSeries.controller('MenuCtrl', ['$scope', '$http', '$q', '$timeout', '$rootScope', function($scope, $http, $q, $timeout, $rootScope){

        $scope.rmodel = false;
        $scope.rmodel_sub = 1;
        $scope.couponResponse = [];
        $scope.errorMsg = "";
        $scope.packageId = "";
        $scope.successCoupon = "";
        $scope.btnStatus = true;
        $scope.selectedPack = [];
        $scope.showCouponData = [];

        $scope.examSearchStatusHead = false;
        $scope.searchExamResultHead = [];
        var ajaxSerchOngoing = false;

        $scope.go_fb_login = 1;
		$scope.go_fb_login_error = "";
		$scope.phoneVerifyProgress = false;
		$scope.go_fb_login_error_otp = "";
		$scope.resent_fb_go = false;
		$scope.resend_fb_go_msg = "";
        $scope.fb_go_otp_verified = false;
        $scope.currentSearchTitle = "";
        $scope.currentSearchHref = "";

        var canceller = $q.defer();
        $scope.examSearchLoadingHead = false;

        function getCookie(key) {
            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
            return keyValue ? keyValue[2] : null;
        }
        $scope.isLoggedIn = function () {
            return getCookie('eg_user') ? true : false;
        }
        $scope.red_link = function (pid) {
            window.location.href='/?pid='+pid;
        }
        $scope.checkCouponClick = function (event) {
            if (event.target.className == 'TS_PromoMmodal') {
                $scope.close_rmodel_sub();
            }
        }
        $scope.open_redeem_promo = function () {
            $scope.rmodel = true;
            $scope.rmodel_sub = 1;
        }
        $scope.close_rmodel_sub = function () {
            $scope.rmodel = false;
            $scope.rmodel_sub = 1;
            $scope.headerCouponCode = null;
            $scope.couponResponse = [];
            $scope.errorMsg = "";
            $scope.packageId = "";
            $scope.successCoupon = "";
            $scope.btnStatus = true;
            $scope.selectedPack = [];
            $scope.showCouponData = [];
        }
        $scope.fetchRedeemPacks = function () {
            var couponCode = $scope.headerCouponCode;
            var req = {method:'POST',url:'/api/v1/packageActivationCodeAjax',data:{'coupon':couponCode.toLowerCase()},headers:{'Content-Type':'application/json'}};
                $http(req).then(function(response){
                    var data = response.data;
                    if (data.status) {
                        var req = {
                            method: 'GET',
                            url: data.result.data,
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        };
                        $http(req).then(function(response){
                            var data_j = response.data;
                            $scope.couponResponse = data_j;
                            $scope.rmodel_sub = 2;
                            $scope.successCoupon = couponCode;
                        },
                        function(error) {
                            console.log("Something went wrong! Please try again later!");
                        });
                    }
                    else {
                        $scope.errorMsg = response.data.msg;
                    }
                },
                function(error) {
                    console.log("Something went wrong! Please try again later!");
                });
        }
        $scope.getPackCid = function(coupon_radio) {
            if (!coupon_radio) {
                return;
            }
            $scope.packageId = coupon_radio;
            if ($scope.packageId) {
                $scope.btnStatus = false;
                var index_id = $scope.couponResponse.findIndex(a_key => a_key[1] === $scope.packageId);
                if (index_id > -1) {
                    $scope.showCouponData = $scope.couponResponse[index_id][2];
                }
                else {
                    $scope.showCouponData = [];
                }
            }
            $scope.selectPack(coupon_radio);
        }
        $scope.unlockCouponCode = function () {
            $scope.rmodel_sub = 3;
        }
        $scope.sanL2 = function (L2Data) {
            return L2Data.join(', ');
        }
        $scope.checkInArray = function (name) {
            if (name == 'Civil Services (Prelims)' || name == 'Engineering All India' || name == 'UG Medical Entrance (NEET, AIIMS, JIPMER)') {
                return true;
            }
            else {
                return false;
            }
        }
        $scope.selectPack = function (data) {
            var hId = 'radioH'+data;
            // var pId = 'radioP'+data;
            $scope.selectedPack[0] = document.getElementById(hId).innerText;
            // $scope.selectedPack[1] = document.getElementById(pId).innerText;
        }
        $scope.examAjaxSearchHead = function (arg, e) {
            if (!arg) {
                $('.TsH_searchView').removeClass('searchViewFocus');
                $scope.examSearchStatusHead = false;
                return;
            }
            if (e.key == "Enter") {
                if ($scope.currentSearchHref) {
                    window.location.href = $scope.currentSearchHref;
                }
                return;
            }
            if (e.key == "ArrowDown" || e.key == "ArrowUp") {
                var all_li = $(".list_full li");
                var active_li = $(".li_active");
                if (active_li.length == 0) {
                    all_li.eq(0).addClass("li_active");
                }
                else {
                    if(e.key == "ArrowDown"){
                        if(all_li.length > active_li.index() + 1) {
                            active_li.removeClass("li_active");
                            active_li.next().addClass("li_active");
                        }
                    }
                    else {
                        if(active_li.index() > 0) {
                            active_li.removeClass("li_active");
                            active_li.prev().addClass("li_active");
                        }
                    }
                }
                $scope.currentSearchTitle = $(".li_active a").text();
                $scope.currentSearchHref = $(".li_active a").attr('href');
            }
            else {
                if (arg.length < 3) {
                    $('.TsH_searchView').removeClass('searchViewFocus');
                    $scope.examSearchStatusHead = false;
                    return;
                }
                if (ajaxSerchOngoing) {
                    canceller.resolve();
                    canceller = $q.defer();
                    ajaxSerchOngoing = false;
                }
                $scope.examSearchLoadingHead = true;
                var req = {
                    method: 'POST',
                    url: '/api/v1/ecatalog/examajaxsearch',
                    timeout: canceller.promise,
                    data: {
                        'keyword': arg
                    }
                }
                ajaxSerchOngoing = true;
                $http(req).then(function(response){
                    if(response.data.status) {
                        $scope.examSearchStatusHead = true;
                        ajaxSerchOngoing = false;
                        var data = response.data.result.data;
                        $scope.searchExamResultHead = [];
                        for (var i = 0; i < data.length; i++) {
                            $scope.searchExamResultHead.push({
                                name: data[i].name,
                                url: data[i].url
                            });
                        }
                        $scope.examSearchLoadingHead = false;
                        $('.TsH_searchView').addClass('searchViewFocus');
                    }
                },
                function (error) {
                    console.log("Something went wrong! Please try again later!");
                });
            }
        }

        $scope.verify_go_fb_phone = function (phone_no) {
			var token = document.getElementById('temp').value;
			if(phone_no && token) {
				$scope.go_fb_login_error = "";
				$scope.go_fb_login_error_otp = "";
				$scope.phoneVerifyProgress = true;
				var req = {
					method: 'POST',
					url: '/api/v1/auth/loginsignup/fb/go/st2',
					data: {
						'phone_no': phone_no,
						'token': token
					},
					headers: {
						'Content-Type':'application/json'
					}
				};
				$http(req).then(function (response){
                    if (response.data.status) {
                        $scope.go_fb_login = 2;
                        $scope.token_re = token;
                        $scope.fb_go_phone_no_re = phone_no;
                    }
                    else {
                        $scope.go_fb_login_error = response.data.msg;
                    }
                    $scope.phoneVerifyProgress = false;
				},
				function(error) {
					console.log("Something went wrong! Please try again later!");
				});
			}
		}

		$scope.otp_verify_fb_go_user = function (otp, phone, token) {
			if (otp && phone && token) {
				$scope.go_fb_login_error_otp = "";
				$scope.fb_go_otp_verified = true;
				$scope.resend_fb_go_msg = "";
				var req = {
					method: 'POST',
					url: '/api/v1/auth/loginsignup/fb/go/st3',
					data: {
						'otp': otp,
						'phone_no': phone,
						'token': token
					},
					headers: {
						'Content-Type':'application/json'
					}
				};
				$http(req).then(function(response){
                    if(response.data.status) {
                        if(location.host == 'testseries.edugorilla.com') {
                            fbq('track', 'CompleteRegistration');
                        }
                        $scope.go_fb_login = 3;
                        $timeout(function(){
                            next = getUrlArgument("next");
                            if (next) {
                                window.location.href = getUrlArgument("next");
                            }
                            else {
                                location.reload();
                            }
                        }, 2000);
                    }
                    else {
                        $scope.go_fb_login_error_otp = "Invalid OTP!";
                    }
                    $scope.fb_go_otp_verified = false;
				},
				function(error) {
					console.log("Something went wrong! Please try again later!");
				});
			}
		}

		$scope.resend_otp_go_fb = function (phone_no, token) {
			if(phone_no && token) {
				$scope.resent_fb_go = true;
				$scope.resend_fb_go_msg = "";
				$scope.go_fb_login_error_otp = "";
				var req = {
					method: 'POST',
					url: '/api/v1/auth/loginsignup/fb/go/st2',
					data: {
						'phone_no': phone_no,
						'token': token
					},
					headers: {
						'Content-Type':'application/json'
					}
				};
				$http(req).then(function (response){
                    if(response.data.status) {
                        $scope.resent_fb_go = false;
                        $scope.resend_fb_go_msg = "success";
                    }
				},
				function(error) {
					console.log("Something went wrong! Please try again later!");
				});
			}
        }
        // function to decode and get query argument in url based on key, if present
		function getUrlArgument(arg) {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            if (arg in vars) {
                return decodeURIComponent(vars[arg]);
            }
            return "";
        }
        $scope.createLogin = function () {
            var url = "/login"
            next = getUrlArgument("next");
            if (next != "") {
                url = "/login" + "?next=" + encodeURIComponent(next)
            }
            return url;
        }
        $scope.createSignup = function () {
            var url = "/signup"
            next = getUrlArgument("next");
            if (next != "") {
                url = "/signup" + "?next=" + encodeURIComponent(next)
            }
            return url;
        }
        $scope.sendCouponConf = function() {
            cou = document.getElementById('coupon_code_header').value;
            pac = document.getElementById('pack_id_header').value;
            var req = {
                method: 'POST',
                url: '/payment',
                data: {
                    'coupon_code': cou,
                    'pack_id': pac,
                    'gateway_type': 'razor'
                },
                headers :{
                    'Content-Type': 'application/json'
                }
            }
            $http(req).then(function(response){
                if(response.data.status) {
                    var paymentResponseData = response.data.result.data;
                    var payment_html_code = `
                        <form method="POST" action="${paymentResponseData.surl}" name="activation_code_success_form">
                            <input type="hidden" name="txnid" value="${paymentResponseData.txnid}">
                            <input type="hidden" name="status" value="success">
                            <input type="hidden" name="amount" value="${paymentResponseData.amount}">
                        </form>
                    `;
                    $("#activation_code_form_div").html(payment_html_code);
                    document.activation_code_success_form.submit();
                }
            },
            function(){
                console.log("Something went wrong! Please try again later!");
            });
        }
    }]);
})();