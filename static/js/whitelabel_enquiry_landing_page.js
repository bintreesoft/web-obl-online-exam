(function(){
    TestSeries.run(['$rootScope', '$sce', function ($rootScope, $sce) {
        $rootScope.toTrustedHTML = function (html) {
            return $sce.trustAsHtml(html);
        };
    }]);

    TestSeries.controller('whitelabelLandingPg_cntrl',['$scope', '$rootScope', '$http', function($scope, $rootScope, $http){
        $scope.msg = "";
        $scope.errorMsg = "";
        $scope.query_response = "";
        $scope.ajaxLoading = false;
        $scope.load = function(){
            var req = {
                method: 'GET',
                url: '/api/v1/ecatalog/L0',
                headers: {
                    'Content-Type': 'application/json'
                }
            };
            $http(req).then(function(response){
                if(response.status){
                    $rootScope.root_user_data = response.data.result.user;
                }
            }, function(error){});
        };

        $scope.whitelabel_enquiry = function () {
        	jQuery('#submitBtn').button('loading');
        	$scope.query_response = "";
            if(!$scope.name ||  !$scope.email || !$scope.phone || !$scope.ins_name)
                $scope.errorMsg = "Please fill out the mandatory fields";

            var msg_ = `
                <b><u>Message:</u></b>
                <br>
                ${$scope.msg}
                <br>
                <b><u>Enquiry Details:</u></b>
                <br>
                <ul>
                    <li>Full Name: ${$scope.name}</li>
                    <li>Phone: ${$scope.phone}</li>
                    <li>Email: ${$scope.email}</li>
                    <li>Institute Name: ${$scope.ins_name}</li>
                    ${$scope.stu_no ? '<li>No. of Students: ' + $scope.stu_no : ''}</li>
                    ${$scope.ins_website ? '<li>Website: ' + $scope.ins_website : ''}</li>
                </ul>
            `;
            var postData = {
                name: $scope.name,
                email: $scope.email,
                phone: ""+$scope.phone,
                ins_name: $scope.ins_name,
                msg: msg_
            };
            $http({method:'POST',url:'/api/v1/sendwhitelabelenquiry',data:postData,headers:{ 'Content-Type': 'application/json' }}).then(function (response) {
				if (response.data.status) {
                    $scope.query_response = "Thanks for contacting us. We will get back to you shortly.";
				}
				else {
                    $scope.errorMsg = "Something went wrong! Please try again later!";
                }
                $('#e_form').trigger('reset');
                jQuery('#submitBtn').button('reset');
            },
            function(error) {
                console.log("Something went wrong! Please try again later!");
            });
        };
    }]);
})(angular);