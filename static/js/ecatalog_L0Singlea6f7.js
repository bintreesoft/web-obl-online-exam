(function() {

TestSeries.controller('SingleCtrl', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope){
	$scope.allData = [];
	$scope.showData = {};
	$scope.currentIndex = 0;
	$scope.ajaxLoading = true;

	$scope.load = function () {
		$scope.getData();
	}
	$scope.getData = function () {
		var req = {
			method: 'GET',
			url: '/api/v1/ecatalog/L1/'+location.pathname.split('/')[3],
			headers: {'Content-Type' : 'application/json'}
		};
		$http(req).then(function (response) {
			data = response.data;
			if (data.status) {
				$scope.allData = data.result.data;
				$rootScope.root_user_data = data.result.user;
				$scope.ajaxLoading = false;				
			}
		},
		function(error) {
            console.log("Something went wrong! Please try again later!");
        });
	}
	$scope.changeData = function (indexId) {
		$scope.showData = $scope.allData[indexId];
		$scope.currentIndex = indexId;
	}
}]);
})(angular);